#!/bin/bash

if [ -f cppshell/setup ]
then
	cppshell/setup env
else
	echo -e '\033[31m'
	echo -e 'please compile cppweb project'
	echo -e '\033[0m'
	exit -1
fi

OSTYPE=`uname`
EXCLUDE='--exclude=makefile --exclude=.bin --exclude=log --exclude=obj --exclude=*.java --exclude=*.cpp --exclude=*.yml --exclude=*.db --exclude=*.o --exclude=*.c'

function clone
{
	mkdir -p $path/$1
	cp -r $1/* $path/$1
}

function setup
{
	if [ -f $path/makeconf ]
	then
		rm -f $path/makeconf
		if [ $? != 0 ]
		then
			echo -e '\033[31m'
			echo -e 'please install with root user'
			echo -e '\033[0m'
			exit -1
		fi
	fi
	
	mkdir -p $path
	if [ $? != 0 ]
	then
		echo -e '\033[31m'
		echo -e 'please install with root user'
		echo -e '\033[0m'
		exit -1
	fi

	clone product/res
	clone webapp/app/compile/cpp
	clone webapp/app/compile/pub

	echo 'install cppweb'
	echo '--------------------------------'
	echo '1.packaging files'
	tar cvf $path/product.tar $EXCLUDE library webapp > /dev/null
}

if [ "$OSTYPE" == "Linux" ]
then
	path=/opt/cppweb

	setup

	clone product/lib
	clone product/dll
	clone product/bin
else
	path=/c/cppweb

	setup

	clone product/win
fi

cp makeconf $path/makeconf
cp .webprofile $path/webprofile

echo '2.unpacking files'
cd $path && tar xvf product.tar > /dev/null && rm product.tar
echo '3.installing service'
echo '--------------------------------'
echo '>>> install cppweb success'

if [ "$OSTYPE" == "Linux" ]
then
	su - `whoami`
else
	echo '>>> you need login again'
	echo ''
fi