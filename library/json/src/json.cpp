#ifndef XG_JSON_CPP
#define XG_JSON_CPP
////////////////////////////////////////////////////////////////////
#include "../json.h"

static void cJSON_freeValue(cJSON* elem)
{
	assert(elem);

	if (elem->valuestring)
	{
		cJSON_free(elem->valuestring);
		elem->valuestring = NULL;
	}

	if (elem->child)
	{
		cJSON_Delete(elem->child);
		elem->child = NULL;
	}
}
static char* cJSON_strdup(const char* str)
{
	int len = str ? strlen(str) : 0;
    char* dest = (char*)cJSON_malloc(len + 1);

    if (len > 0) memcpy(dest, str, len);

	dest[len] = 0;

    return dest;
}
static void cJSON_swapValue(cJSON* dest, cJSON* src)
{
	assert(src && dest);

	std::swap(src->type, dest->type);
	std::swap(src->child, dest->child);
	std::swap(src->valueint, dest->valueint);
	std::swap(src->valuestring, dest->valuestring);
	std::swap(src->valuedouble, dest->valuedouble);
}
static bool cJSON_load(cJSON* item, const char* str)
{
	CHECK_FALSE_RETURN(str);

	cJSON* tmp = cJSON_Parse(str);

	if (tmp == NULL)
	{
		item->valuestring = cJSON_strdup(str);
		item->type = cJSON_String;

		return true;
	}

	cJSON_swapValue(item, tmp);
	cJSON_Delete(tmp);

	return true;
}

JsonElement::iterator::iterator(cJSON* item, const JsonElement* parent)
{
	idx = -1;
	len = -1;

	if (elem = item)
	{
		if (elem->type == cJSON_Array)
		{
			if ((len = cJSON_GetArraySize(elem)) > 0) idx = 0;
		}
		else
		{
			if (elem = elem->child) idx = 0;
		}
	}

	if (parent) holder = parent->holder;
}

JsonElement::iterator& JsonElement::iterator::operator ++ ()
{
	++idx;

	if (len < 0)
	{
		if ((elem = elem->next) == NULL) idx = -1;
	}
	else
	{
		if (idx >= len) idx = -1;
	}

	return *this;
}
JsonElement JsonElement::iterator::operator * ()
{
	cJSON* item = elem;

	if (len >= 0) item = cJSON_GetArrayItem(elem, idx);

	JsonElement res(item, NULL);

	res.holder = holder;

	return res;
}
bool JsonElement::iterator::operator != (const iterator& obj) const
{
	return idx != obj.idx;
}

bool JsonElement::InitElement(cJSON* item)
{
	CHECK_FALSE_RETURN(item);

	assert(item->child == NULL);

	cJSON_freeValue(item);

	return true;
}
bool JsonElement::InitElement(cJSON* item, int val)
{
	CHECK_FALSE_RETURN(InitElement(item));

	item->type = cJSON_Number;
	item->valuedouble = val;
	item->valueint = val;

	return true;
}
bool JsonElement::InitElement(cJSON* item, bool val)
{
	CHECK_FALSE_RETURN(InitElement(item));

	item->type = val ? cJSON_True : cJSON_False;

	return true;
}
bool JsonElement::InitElement(cJSON* item, double val)
{
	CHECK_FALSE_RETURN(InitElement(item));

	item->type = cJSON_Number;
	item->valueint = (int)(val);
	item->valuedouble = val;

	return true;
}
bool JsonElement::InitElement(cJSON* item, long long val)
{
	return val < INT_MIN || val > INT_MAX ? InitElement(item, stdx::str(val).c_str()) : InitElement(item, (int)(val));
}
bool JsonElement::InitElement(cJSON* item, const char* val)
{
	CHECK_FALSE_RETURN(InitElement(item));
	
	item->type = cJSON_String;
	item->valuestring = cJSON_strdup(val);
	
	return item->valuestring ? true : false;
}
bool JsonElement::InitElement(cJSON* item, unsigned long long val)
{
	return val > INT_MAX ? InitElement(item, stdx::str(val).c_str()) : InitElement(item, (int)(val));
}
string JsonElement::toString() const
{
	if (elem == NULL) return stdx::EmptyString();

	char* tmp = cJSON_PrintUnformatted(elem);

	if (tmp == NULL) return stdx::EmptyString();

	string res = tmp;

	cJSON_free(tmp);

	return std::move(res);
}
string JsonElement::getVariable() const
{
	if (elem == NULL) return stdx::EmptyString();

	if (elem->type == cJSON_True) return "true";

	if (elem->type == cJSON_False) return "false";

	if (elem->type == cJSON_Number)
	{
		char* msg = cJSON_Print(elem);

		if (msg == NULL) return stdx::EmptyString();

		string res = msg;

		cJSON_free(msg);

		return std::move(res);
	}

	return elem->valuestring ? elem->valuestring : stdx::EmptyString();
}
bool JsonElement::init(const string& msg, function<cJSON*(void)> creator)
{
	if (elem) return load(msg);

	elem = msg.empty() ? creator() : cJSON_Parse(msg.c_str());

	if (elem) holder = sp<cJSON>(elem, cJSON_Delete);

	return true;
}
JsonElement JsonElement::add()
{
	if (elem == NULL) init(stdx::EmptyString(), cJSON_CreateArray);

	cJSON* item = cJSON_CreateObject();

	if (item) cJSON_AddItemToArray(elem, item);

	return pack(item);
}
void JsonElement::remove(int idx)
{
	cJSON_DeleteItemFromArray(elem, idx);
}
bool JsonElement::load(const string& msg)
{
	if (elem)
	{
		cJSON_freeValue(elem);

		if (msg.empty())
		{
			elem->type = cJSON_NULL;
			holder = NULL;
			elem = NULL;

			return true;
		}
	}
	else
	{
		if (msg.empty()) return true;

		CHECK_FALSE_RETURN(init());
	}

	return cJSON_load(elem, msg.c_str());
}
void JsonElement::remove(const string& name)
{
	cJSON_DeleteItemFromObject(elem, name.c_str());
}
JsonElement JsonElement::get(int idx) const
{
	if (elem == NULL) return pack(NULL);

	if (idx < 0 || idx >= cJSON_GetArraySize(elem)) return pack(NULL);

	return pack(cJSON_GetArrayItem(elem, idx));
}
JsonElement JsonElement::addObject(const string& name, function<cJSON*(void)> creator)
{
	if (elem == NULL)
	{
		init();
	}
	else
	{
		JsonElement tmp = get(name);

		if (tmp.elem) return tmp;
	}

	cJSON* item = creator();

	if (item) cJSON_AddItemToObject(elem, name.c_str(), item);

	return pack(item);
}
JsonElement JsonElement::get(const string& name) const
{
	if (elem == NULL) return pack(NULL);

	return pack(cJSON_GetObjectItem(elem, name.c_str()));
}
int JsonElement::size() const
{
	if (isArray()) return cJSON_GetArraySize(elem);
	
	if (elem->child)
	{
		cJSON* child = elem->child;

		if (child)
		{
			int cnt = 1;

			while (child = child->prev) cnt++;

			child = elem->child;

			while (child = child->next) cnt++;

			return cnt;
		}
	}

	return 0;
}
JsonElement JsonElement::operator [] (int idx)
{
	JsonElement item = get(idx);

	if (item.elem) return item;

	if (elem == NULL) init();

	int sz = size();
	
	if (sz == 0)
	{
		assert(isArray() || elem->child == NULL);
	
		if (elem->valuestring)
		{
			cJSON_free(elem->valuestring);
			elem->valuestring = NULL;
		}

		elem->type = cJSON_Array;
	}
	
	if (isArray())
	{
		while (sz++ <= idx) item = add();
	}
	
	return item;
}
JsonElement JsonElement::operator [] (const string& name)
{
	JsonElement item = get(name);
	
	return item.elem ? item : addNull(name);
}
JsonElement& JsonElement::operator = (const JsonElement& val)
{
	if (holder)
	{
		if (holder == val.holder)
		{
			elem = val.elem;
		}
		else
		{
			if (elem == NULL) init();

			if (val.isNull())
			{
				cJSON_freeValue(elem);
				elem->type = cJSON_NULL;
			}
			else
			{
				cJSON* tmp = cJSON_Duplicate(val.elem, true);
				cJSON_swapValue(elem, tmp);
				cJSON_Delete(tmp);
			}
		}
	}
	else
	{
		holder = val.holder;
		elem = val.elem;
	}
	return *this;
}

bool JsonReflect::isArray() const
{
	return false;
}
string JsonReflect::toString() const
{
	JsonElement dest;

	toJsonElement(dest);

	return dest.toString();
}
string JsonReflect::toDocString() const
{
	string doc;
	vector<ReflectItem> vec = ReflectHelper::GetAttrList(this);

	for (auto& item : vec)
	{
		const char* type = item.type;
		const char* remark = item.getRemark();
		const char* extdata = item.getExtdata();

		if (item.isObject())
		{
			JsonReflect* obj = (JsonReflect*)((char*)(this) + item.offset);

			type = strstr(obj->getClassName(), "JsonReflectList") ? "array" : "object";
	
			doc += stdx::format("\n<tr><td><span>%s</span></td><td>%s</td><td>%s</td><td>%s</td></tr>", item.name, type, extdata, remark);
			doc += stdx::replace(obj->toDocString(), "\n<tr><td>", "\n<tr><td>&nbsp;&nbsp;<span>&gt;</span>");
		}
		else
		{
			doc += stdx::format("\n<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>", item.name, type, extdata, remark);
		}
	}

	while (true)
	{
		string tmp = stdx::replace(doc, "<span>&gt;</span>&nbsp;&nbsp;", "<span>&nbsp;</span>&nbsp;&nbsp;");

		if (tmp.length() == doc.length()) return doc;

		std::swap(tmp, doc);
	}

	return doc;
}
void JsonReflect::toJsonElement(JsonElement& dest) const
{
	toJsonElementAttrs(dest, ReflectHelper::GetAttrList(this));
}
bool JsonReflect::fromJsonElement(const JsonElement& obj, bool inited)
{
	return fromJsonElementAttrs(obj, inited, ReflectHelper::GetAttrList(this));
}
void JsonReflect::toJsonElementAttrs(JsonElement& dest, const vector<ReflectItem>& attrs) const
{
	for (auto& item : attrs)
	{
		if (item.isObject())
		{
			JsonReflect* obj = (JsonReflect*)((char*)(this) + item.offset);
			JsonElement data = obj->isArray() ? dest.addArray(item.name) : dest.addObject(item.name);
			obj->toJsonElement(data);
		}
		else
		{
			string val = item.get(this);

			if (item.isString())
			{
				dest[item.name] = val;
			}
			else
			{
				if (val.empty()) continue;

				if (item.isInt())
				{
					dest[item.name] = stdx::atoi(val.c_str());
				}
				else if (item.isLong())
				{
					dest[item.name] = (long)stdx::atol(val.c_str());
				}
				else if (item.isBool())
				{
					dest[item.name] = strcasecmp(val.c_str(), "true") == 0;
				}
				else
				{
					dest[item.name] = stdx::atof(val.c_str());
				}
			}
		}
	}
}
bool JsonReflect::fromJsonElementAttrs(const JsonElement& obj, bool inited, const vector<ReflectItem>& attrs)
{
	CHECK_FALSE_RETURN(obj.isObject() || obj.isArray());

	if (obj.isNull())
	{
		if (inited)
		{
			for (auto& item : attrs) item.check(stdx::EmptyString());
		}

		return true;
	}

	for (auto& item : attrs)
	{
		JsonElement src = obj.get(item.name);

		if (src.isNull())
		{
			if (inited) item.check(stdx::EmptyString());

			continue;
		}

		if (item.isObject())
		{
			JsonReflect* dest = (JsonReflect*)((char*)(this) + item.offset);

			CHECK_FALSE_RETURN(dest->fromJsonElement(src));
		}
		else
		{
			string val = src.getVariable();

			item.check(val);

			if (item.isString())
			{
				CHECK_FALSE_RETURN(item.set(this, val));
			}
			else
			{
				if (val.length() > 0) CHECK_FALSE_RETURN(item.set(this, val));
			}
		}
	}

	return true;
}
bool JsonReflect::fromObject(const JsonReflect& obj)
{
	const char* type;
	const char* name;
	vector<ReflectItem> vec = ReflectHelper::GetAttrList(this);
	vector<ReflectItem> attrs = ReflectHelper::GetAttrList(&obj);

	for (auto& item : vec)
	{
		type = item.type;
		name = item.name;

		if (item.isObject())
		{
			JsonReflect* dest = dynamic_cast<JsonReflect*>((Object*)((char*)(this) + item.offset));

			if (dest)
			{
				for (auto& item : attrs)
				{
					if (strcmp(name, item.name) == 0 && strcmp(type, item.type) == 0)
					{
						JsonReflect* src = dynamic_cast<JsonReflect*>((Object*)((char*)(&obj) + item.offset));

						if (src) CHECK_FALSE_RETURN(dest->fromObject(*src));

						break;
					}
				}
			}
		}
		else
		{
			for (auto& item : attrs)
			{
				if (strcmp(name, item.name) == 0)
				{
					string val = item.get(&obj);

					if (item.isString())
					{
						CHECK_FALSE_RETURN(item.set(this, val));
					}
					else
					{
						if (val.length() > 0) CHECK_FALSE_RETURN(item.set(this, val));
					}

					break;
				}
			}
		}
	}

	return true;
}
////////////////////////////////////////////////////////////////////
#endif