#ifndef XG_HTTPBASE_CPP
#define XG_HTTPBASE_CPP
////////////////////////////////////////////////////////
#include "../HttpBase.h"

const CgiMapData& CgiMapData::NullObject()
{
	static CgiMapData nullobj;

	return nullobj;
}
string CgiMapData::GetKey(const string& url)
{
	const string& key = stdx::trim(url, HTTP_SPACELIST);

	return key.empty() ? "/" : stdx::tolower(key); 
}

bool HttpSession::clear()
{
	datamap.clear();

	return true;
}
bool HttpSession::disable()
{
	datamap.clear();
	etime = 0;

	return true;
}
int HttpSession::size() const
{
	return datamap.size();
}
long HttpSession::getTimeout() const
{
	time_t now = time(NULL);

	return etime > now ? etime - now : XG_NOTFOUND;
}
bool HttpSession::setTimeout(long second)
{
	etime = time(NULL) + second;

	return true;
}
bool HttpSession::remove(const string& key)
{
	datamap.remove(key);

	return true;
}
bool HttpSession::set(const map<string, string>& attrmap)
{
	datamap.set(attrmap);

	return true;
}
bool HttpSession::set(const string& key, const string& val)
{
	datamap.set(key, val);

	return true;
}
bool HttpSession::get(const string& key, string& val) const
{
	return datamap.get(key, val);
}

string HttpHeadNode::toString() const
{
	string str;

	if (vec.empty()) return str;

	for (const string& key : vec)
	{
		if (stdx::tolower(key).find("set-cookie:") == 0)
		{
			str += endspliter + "Set-Cookie" + keyspliter + content.find(key)->second;
		}
		else
		{
			str += endspliter + key + keyspliter + content.find(key)->second;
		}
	}

	return str.c_str() + endspliter.length();
}
bool HttpHeadNode::parse(const string& msg, bool inited)
{
	size_t idx = 0;
	size_t pos = 0;
	vector<string> tmp;

	if (inited) clear();

	CHECK_FALSE_RETURN(stdx::split(tmp, msg, endspliter) > 0);

	for (const string& item : tmp)
	{
		if ((pos = item.find(keyspliter)) == string::npos) continue;

		string key = item.substr(0, pos);
		string val = item.substr(pos + keyspliter.length());

		if (stdx::tolower(item.substr(0, pos + 1)).find("set-cookie:") == 0) key = "Set-Cookie:" + stdx::str(idx++);

		CHECK_FALSE_RETURN(setValue(key, val, true));
	}

	return vec.size() > 0;
}

void HttpProcessBase::trace(int level, const string& msg) const
{
	if (ctx)
	{
		ctx->trace(level, msg);
	}
	else
	{
		LogTrace(level, msg);
	}
}
void HttpProcessBase::trace(int level, const char* fmt, ...) const
{
	int len;
	string msg;
	va_list args;

	va_start(args, fmt);
	len = stdx::vformat(msg, fmt, args);
	va_end(args);

	trace(level, msg);
}
int HttpProcessBase::doWork(HttpRequest* request, HttpResponse* response)
{
	return 0;
}
////////////////////////////////////////////////////////
#endif
