#ifndef XG_HTTPHELPER_H
#define XG_HTTPHELPER_H
//////////////////////////////////////////////////////////
#include <http/HttpResponse.h>

class HttpHelper
{
public:
	static int GetLastStatus();
	static void SetLastStatus(int code);
	static sp<Socket> Connect(const string& ip, int port, bool crypted);
	static int GetLinkSet(set<string>& uset, const string& html, const string& host = "");
	static string GetLink(const string& path, const string& host, int port = 80, bool crypted = false);

	static SmartBuffer GetResult(const string& url, bool simplify = false);
	static SmartBuffer GetResult(const string& url, const string& data, bool simplify = false);
	static SmartBuffer GetResult(const string& url, const map<string, string>& head, bool simplify = false);
	static SmartBuffer GetResult(const string& url, const string& data, const map<string, string>& head, bool simplify = false);
};
//////////////////////////////////////////////////////////
#endif