#ifndef XG_HTTPRESPONSE_H
#define XG_HTTPRESPONSE_H
////////////////////////////////////////////////////////
#include "HttpRequest.h"

class HttpResponse : public IHttpResponse
{
	friend class HttpStream;
	friend class RouterItem;
	friend class HttpRequest;
	friend class HttpRequestItem;

	string contype;
	string sockhost;
	sp<Socket> sock;
	HttpHeadNode node;
	SmartBuffer result;
	HttpRequest* request;

	bool parseHeader(const char* msg);
	bool parseHeader(const char* msg, int len);
	int init(sp<Socket> sock, bool decode, int timeout);
	bool init(HttpRequest* request, sp<Socket> sock, const string& addr);

	int forward(sp<Context> ctx, const string& url, const string& addr, function<int(const void*, int, const string&)> response);
	int process(sp<Context> ctx, const string& url, const CgiMapData& item, const string& addr, function<int(const void*, int, const string&)> response);

public:
	void close();
	SmartBuffer getRefusedContent(bool withead = true);
	SmartBuffer getNotFoundContent(bool withead = true);
	void addCookie(const string& key, const string& val, int timeout = 0);

	bool isKeepAlive() const;
	string getSocketHost() const;
	string getClientHost() const;
	sp<Socket> getSocket() const;
	string getHeadString() const;
	string getContentType() const;
	SmartBuffer getResult() const;
	bool setContentType(const string& val);
	string getHeadValue(const string& key) const;
	bool setHeadValue(const string& key, const string& val);
};
////////////////////////////////////////////////////////
#endif