#ifndef XG_HTTPFRAME_H
#define XG_HTTPFRAME_H
//////////////////////////////////////////////////////////
#include "HttpCoder.h"


class HttpFrame : public Object
{
	friend class HttpStream;
	friend class HttpRequest;

	u_int32 id = 0;
	u_int32 rid = 0;
	HttpHeadVector& vec;
	mutable u_char type = 0;
	mutable u_char flag = 0;
	mutable u_char weight = 0;
	mutable u_int32 hdrsz = 0;
	mutable u_int32 datsz = 0;
	mutable u_int32 maxsz = 0;
	mutable u_int32 maxcnt = 0;

	mutable string path;
	mutable string param;
	mutable SmartBuffer data;
	mutable HttpHeadNode head;

public:
	int decode() const;
	E_HTTP_METHOD getMethod() const;
	int init(const void* msg, int len);
	int add(const HttpFrame& obj) const;

	static SmartBuffer Encode(u_int32 id, HttpHeadNode& head, SmartBuffer data, HttpHeadVector& vec);

	HttpFrame(HttpHeadVector& tab) : vec(tab)
	{
	}
	bool operator < (const HttpFrame& obj) const
	{
		return id < obj.id;
	}
	bool operator == (const HttpFrame& obj) const
	{
		return id == obj.id;
	}

	int size() const
	{
		return data.size();
	}
	u_int32 getId() const
	{
		return id;
	}
	u_char getType() const
	{
		return type;
	}
	u_char getFlag() const
	{
		return flag;
	}
	bool isAddFrame() const
	{
		return type == HTTP2_CONTINUATION_FRAME;
	}
	bool isRstFrame() const
	{
		return type == HTTP2_RSTSTREAM_FRAME;
	}
	bool isDataFrame() const
	{
		return type == HTTP2_DATA_FRAME;
	}
	bool isHeadFrame() const
	{
		return type == HTTP2_HEADER_FRAME;
	}
	bool isEndHead() const
	{
		return (flag & HTTP2_END_HEADER) > 0;
	}
	bool isEndStream() const
	{
		return (flag & HTTP2_END_STREAM) > 0;
	}
	bool hasPadding() const
	{
		return (flag & HTTP2_HAS_PADDING) > 0;
	}
	bool hasPriority() const
	{
		return (flag & HTTP2_HAS_PRIORITY) > 0;
	}
	SmartBuffer getData() const
	{
		return data;
	}
	const HttpHeadNode& getHead() const
	{
		return head;
	}
};

//////////////////////////////////////////////////////////
#endif