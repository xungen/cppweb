#ifndef XG_HTTPSTREAM_H
#define XG_HTTPSTREAM_H
//////////////////////////////////////////////////////////
#include "HttpFrame.h"
#include "HttpServer.h"

class HttpStream : public WorkItem
{
	friend class HttpServer;

	struct FileItem
	{
		u_int32 id;
		int readed;
		int filelen;
		sp<IFile> file;

		FileItem(sp<IFile> file, int filelen, u_int32 id)
		{
			this->filelen = filelen;
			this->file = file;
			this->readed = 0;
			this->id = id;
		}
	};

	int index = 0;
	int inited = 0;
	int readed = 0;
	int writed = 0;
	int rsplen = 0;
	int errcode = 0;
	int timeout = 0;
	time_t utime = 0;
	u_int32 maxid = 0;

	string addr;
	sp<Socket> sock;
	set<HttpFrame> fs;
	SmartBuffer rspdata;
	HttpHeadVector vec[2];
	list<FileItem> filelist;
	char buffer[HTTP2_HEAD_SIZE + HTTP2_FRAME_MAXSIZE];

	const HttpFrame& get(const HttpFrame& item) const;

public:
	int read();
	void run();
	bool send();
	~HttpStream();
	bool runnable();
	int process(const HttpFrame& frame);
	int response(const void* msg, int len);
	int update(HttpFrameData* hdr, int len);
	HttpStream(sp<Socket> _sock, const string& _addr) : sock(_sock), addr(_addr), timeout(HttpServer::Instance()->getTimeout()){}
};

//////////////////////////////////////////////////////////
#endif