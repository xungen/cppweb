#ifndef XG_DATETIME_H
#define XG_DATETIME_H
//////////////////////////////////////////////////////////////////
#include "std.h"


class Calendar : public Object
{
protected:
	int day;
	int year;
	int month;

public:
	static const int MIN_YEAR = 0;
	static const int MAX_YEAR = 10000;
	static const int BASE_YEAR = 2000;
	static const int BASE_WEEKDAY = 6;

	bool canUse() const
	{
		return getCheckCode() ? false : true;
	}
	Calendar() : year(0), month(0), day(0)
	{
	}
	Calendar(int _year, int _month, int _day) : year(_year), month(_month), day(_day)
	{
	}
	Calendar(int _date) : year(_date / 10000), month(_date / 100 % 100), day(_date % 100)
	{
	}

	bool addYear();
	bool subYear();
	bool addDate();
	bool subDate();
	bool addMonth();
	bool subMonth();
	int getDate() const;
	int getYear() const;
	int getMonth() const;
	int getWeekday() const;
	bool setDate(int day);
	bool setYear(int year);
	bool setMonth(int month);

	bool isLeapYear() const;
	string toString() const;
	int getMonthDayCount() const;
	int getMonthWeekCount() const;
	bool init(int year, int month, int day);

	/* return:
	 *	0: ok
	 *	1: year lower
	 *	2: year upper
	 * 	3: month lower
	 *	4: month upper
	 * 	5: date lower
	 *	6: date upper
	 */
	int getCheckCode() const;
	int getMonthWeeknum() const;
	string getMonthWeekdays() const;
	int getMonthLastWeekday() const;
	int getMonthFirstWeekday() const;
	
public:
	static bool IsLeapYear(int year);
	static int GetYearDayCount(int year);
	static int GetDayCount(int a, int b);
	static int GetYearFirstWeekday(int year);
	static int GetWeekday(const Calendar& calendar);
	static int GetYearDay(const Calendar& calendar);
	static int GetMonthWeekCount(int year, int month);
	static int GetMonthLastWeekday(int year, int month);
	static int GetMonthFirstWeekday(int year, int month);
	static int GetMonthDayCount(int month, bool leapyear);
	static int GetDayCount(const Calendar& a, const Calendar& b);
	static int GetFirstWeekdayDateFromWeeknum(int year, int month, int weeknum);
	static int GetDateFromWeekday(int year, int month, int weeknum, int weekday);
};

class DateTime : public Object, public stDateTime
{
public:
	DateTime();
	DateTime& update();
	DateTime(time_t tm);
	bool canUse() const;
	time_t getTime() const;
	string toString() const;
	DateTime& update(time_t tm);
	Calendar toCalendar() const;
	string getGmtString() const;
	string getTimeString() const;
	string getDateString() const;

	static string ToString();
	static string GetBizId();
	static DateTime FromString(const string& str);
	static DateTime FromCalendar(const Calendar& cal);
	static bool SetSystemDateTime(const DateTime& dt);
	static long long GetSecondsAfterYearFirstDay(const DateTime& dt);
	static long long GetSecondsAfterDayFirstSecond(const DateTime& dt);
};
//////////////////////////////////////////////////////////////////
#endif

