#ifndef XG_APP_H
#define XG_APP_H
///////////////////////////////////////////////////////////////////
#include "sync.h"

#ifdef XG_LINUX

#include <dlfcn.h>

int GetLastError();

#else

#define	RTLD_NOW 0

typedef BOOL (WINAPI *QueryFullProcessPathFunc)(HANDLE, DWORD, LPSTR, PDWORD);

extern QueryFullProcessPathFunc QueryFullProcessPath;

#endif

string GetErrorString();
string GetErrorString(int errcode);

#ifndef XG_PROCESS_GLOBALPTR_ENVNAME_HDR
#define XG_PROCESS_GLOBALPTR_ENVNAME_HDR	"XG_GPTR_ENV_"
#endif

#define START_APP(APPCLASS)						\
int main(int argc, char* argv[])				\
{												\
	Process::Instance(argc, argv);				\
	sp<APPCLASS> app = newsp<APPCLASS>();		\
	Process::GetApplication(app.get());			\
	return app->main() ? 0 : XG_ERROR;			\
}

#define XG_DEFINE_GLOBAL_VARIABLE(TYPE)								\
static TYPE* ptr = NULL; if (ptr) return ptr;						\
{Locker lk(Process::Instance()->getMutex());						\
string name = stdx::format("GPTR_%s_%s_%s",							\
stdx::GetFileNameFromPath(__FILE__).c_str(), #TYPE, __FUNCTION__);	\
if (ptr = (TYPE*)(Process::GetObject(name))) return ptr;			\
Process::SetObject(name, ptr = new TYPE());							\
return ptr;}

class Application;

typedef int PROCESS_T;

struct ProcessData
{
	string name;
	string path;
	PROCESS_T id;
	PROCESS_T pid;
};

class Process : public Object
{
	Mutex mutex;
	char** argv;
	string appname;
	time_t startime;
	vector<string> vec;
	mutable SpinMutex mtx;
	map<string, string> paramap;
	map<string, const void*> objmap;

	Process(const Process&);
	int init(int argc, char** argv);
	Process operator = (const Process&);
	
#ifndef XG_LINUX
	static HANDLE GetProcessHandle(PROCESS_T id);
	static bool EnableDebugPrivilege(HANDLE handle);
#endif

public:
	Process();
	Mutex& getMutex();
	int getParamCount() const;
	int getObjectCount() const;
	char** getCmdParam() const;
	int getCmdParamCount() const;
	const char* getAppname() const;
	const char* getCmdParam(int index) const;
	string getParam(const string& key) const;
	const void* getObject(const string& key) const;
	const char* getCmdParam(const string& key) const;
	void setObject(const string& key, const void* obj);
	void setParam(const string& key, const string& val);

	static Process* Instance();
	static Process* Instance(int argc, char** argv);
	static Application* GetApplication(Application* obj = NULL);

	static bool InitDaemon();
	static void CheckSingle();
	static bool IsStartedByExplorer();
	static bool SetCommonExitSignal();
	static bool Wait(PROCESS_T handle);
	static string GetCurrentDirectory();
	static PROCESS_T GetCurrentProcess();
	static void CommonExitFunc(int signum);
	static string GetEnv(const string& key);
	static bool GetProcessExePath(string& path);
	static bool SetDaemonCommand(const string& cmd);
	static bool Kill(PROCESS_T handle, int flag = 0);
	static PROCESS_T GetParentProcess(PROCESS_T handle);
	static bool SetCurrentDirectory(const string& path);
	static bool SetEnv(const string& key, const string& val);
	static bool RegisterLibraryPath(const string& path = "");
	static bool RegisterExecutablePath(const string& path = "");
	static bool GetProcessExePath(PROCESS_T handle, string& path);
	static bool RegisterVariable(const string& key, const string& val);

	static int GetSystemProcessList(vector<ProcessData>& vec);
	static int GetSystemProcessListByName(vector<ProcessData>& vec, const string& name);
	static int GetSystemProcessListByExeName(vector<ProcessData>& vec, const string& path);

	static string GetGlobalVariableKey(const string& name)
	{
		static const string key = XG_PROCESS_GLOBALPTR_ENVNAME_HDR;

		return std::move(key + name + to_string(GetCurrentProcess()));
	}
	static bool SetGlobalVariable(const string& name, const void* ptr)
	{
		return SetEnv(GetGlobalVariableKey(name), to_string((long long)(ptr)));
	}

	template <class DATA_TYPE>
	static bool GetGlobalVariable(const string& name, DATA_TYPE* &ptr)
	{
		string val = GetEnv(GetGlobalVariableKey(name));

		if (val.empty()) return false;

		ptr = (DATA_TYPE*)((long long)(atol(val.c_str())));

		return true;
	}

	template <class DATA_TYPE>
	static bool GetGlobalVariable(const string& name, DATA_TYPE* &ptr, bool created)
	{
		string val = GetEnv(GetGlobalVariableKey(name));

		if (val.empty())
		{
			if (created)
			{
				if (SetGlobalVariable(name, ptr = new DATA_TYPE())) return true;
				
				delete ptr;
				ptr = NULL;
			}
			
			return false;
		}

		ptr = (DATA_TYPE*)((long long)(atol(val.c_str())));

		return true;
	}

	static int GetParamCount()
	{
		return Instance()->getParamCount();
	}
	static char** GetCmdParam()
	{
		return Instance()->getCmdParam();
	}
	static time_t GetStartTime()
	{
		return Instance()->startime;
	}
	static int GetCmdParamCount()
	{
		return Instance()->getCmdParamCount();
	}
	static const char* GetAppname()
	{
		return Instance()->getAppname();
	}
	static const char* GetCmdParam(int index)
	{
		return Instance()->getCmdParam(index);
	}
	static string GetParam(const string& key)
	{
		return Instance()->getParam(key);
	}
	static const char* GetCmdParam(const string& key)
	{
		return Instance()->getCmdParam(key);
	}
	static void SetParam(const string& key, const string& val)
	{
		Instance()->setParam(key, val);
	}

	static int GetObjectCount()
	{
		return Instance()->getObjectCount();
	}
	static const void* GetObject(const string& key)
	{
		return Instance()->getObject(key);
	}
	static void SetObject(const string& key, const void* obj)
	{
		Instance()->setObject(key, obj);
	}
};

class Application : public Object
{
public:
	virtual bool main();
	virtual void clean();
	static void puts(const string& msg);
	static void printf(const char* fmt, ...);

	static int GetParamCount()
	{
		return Process::GetParamCount();
	}
	static char** GetCmdParam()
	{
		return Process::GetCmdParam();
	}
	static int GetCmdParamCount()
	{
		return Process::GetCmdParamCount();
	}
	static const char* GetAppname()
	{
		return Process::GetAppname();
	}
	static const char* GetCmdParam(int index)
	{
		return Process::GetCmdParam(index);
	}
	static string GetParam(const string& key)
	{
		return Process::GetParam(key);
	}
	static const char* GetCmdParam(const string& key)
	{
		return Process::GetCmdParam(key);
	}
	static void SetParam(const string& key, const string& val)
	{
		Process::SetParam(key, val);
	}
	
	static int GetObjectCount()
	{
		return Process::GetObjectCount();
	}
	static const void* GetObject(const string& key)
	{
		return Process::GetObject(key);
	}
	static void SetObject(const string& key, const void* obj)
	{
		Process::SetObject(key, obj);
	}
};

class DllFile : public Object
{
	typedef map<string, sp<DllFile>> ObjectMap;

	string path;
	time_t ctime = 0;
	DLLFILE_T handle = NULL;

	static SpinMutex* GetMutex();
	static ObjectMap* GetObjectMap();

public:
	~DllFile();
	void close();
	bool open(const string& path);
	void* getAddress(const string& name) const;

	static string GetLastPath();
	static string GetErrorString();
	static bool Remove(sp<DllFile> dll);
	static bool Remove(const string& path);
	static sp<DllFile> Get(const string& path, bool created = true, bool saved = true);

	time_t getLoadTime() const
	{
		return ctime;
	}
	const string& getPath() const
	{
		return path;
	}
	template <typename FUNC_PTR> bool read(FUNC_PTR& func, const char* name) const
	{
		return (func = (FUNC_PTR)(getAddress(name))) ? true : false;
	}
};

namespace stdx
{
	const char* GetProcessExePath();
};

#endif