#ifndef XG_STD_H
#define XG_STD_H
///////////////////////////////////////////////////////////////////
#include "app.h"

enum E_PARAM_TYPE
{
	eINT = 0,
	eBOOL = 1,
	eNULL = 2,
	eDOUBLE = 3,
	eSTRING = 4,
	eBUFFER = 5
};

class BitSequence : public Object
{
	u_char* data;

public:
	void set(int idx)
	{
		BIT_SET(data[idx / 8], idx % 8, 1);
	}
	void clear(int idx)
	{
		BIT_SET(data[idx / 8], idx % 8, 0);
	}
	u_char at(int idx) const
	{
		return BIT_AT(data[idx / 8], idx % 8);
	}
	template<int idx> void set()
	{
		BIT_SET(data[idx / 8], idx % 8, 1);
	}
	template<int idx> void clear()
	{
		BIT_SET(data[idx / 8], idx % 8, 0);
	}
	template<int idx> u_char at() const
	{
		return BIT_AT(data[idx / 8], idx % 8);
	}
	BitSequence(void* _data) : data((u_char*)(_data))
	{
	}
};

class Buffer
{
	friend class SmartBuffer;

	int sz;
	int maxsz;
	u_char* buf;

public:
	void free();
	u_char* malloc(int sz);
	u_char* truncate(int sz);

	~Buffer()
	{
		free();
	}
	u_char& operator [] (int index)
	{
		return buf[index];
	}
	Buffer(int sz = 0) : sz(0), maxsz(0), buf(NULL)
	{
		if (sz > 0) malloc(sz);
	}

	int size() const
	{
		return sz;
	}
	char* str() const
	{
		return buf ? (char*)(buf) : NULL;
	}
	u_char* ptr() const
	{
		return buf ? buf : NULL;
	}
	bool isNull() const
	{
		return buf == NULL;
	}
	int capacity() const
	{
		return maxsz;
	}
};

class SmartBuffer : public Object
{
	sp<Buffer> buffer;

public:
	SmartBuffer clone();
	u_char* malloc(int sz);
	string toString() const;
	SmartBuffer& append(const string& str);
	SmartBuffer& append(const SmartBuffer& obj);
	SmartBuffer& operator = (const string& val);

	void free()
	{
		buffer = NULL; 
	}
	int size() const
	{
		return buffer ? buffer->size() : 0;
	}
	int capacity() const
	{
		return buffer ? buffer->capacity() : 0;
	}
	char* str() const
	{
		return buffer ? buffer->str() : NULL;
	}
	u_char* ptr() const
	{
		return buffer ? buffer->ptr() : NULL;
	}
	bool isNull() const
	{
		return buffer.get() == NULL || buffer->isNull();
	}
	SmartBuffer(int sz = 0)
	{
		if (sz > 0) malloc(sz);
	}
	SmartBuffer(const string& str)
	{
		*this = str;
	}
	u_char* truncate(int sz)
	{
		return buffer->truncate(sz);
	}
	u_char& operator [] (int index)
	{
		return buffer->buf[index];
	}
};

class Exception : public Object, public exception
{
protected:
	int errcode;
	string errmsg;
	map<string, string> fields;

public:
	string toString() const;
	const char* what() const noexcept;

	int getErrorCode() const
	{
		return errcode;
	}
	const char* getErrorString() const
	{
		return errmsg.c_str();
	}
	static const char* GetErrorString(int code)
	{
        switch (code)
		{
            case XG_FAIL: return "process failed";
            case XG_SYSBUSY: return "system busy";
            case XG_DATAERR: return "invalid data";
            case XG_TIMEOUT: return "resource timeout";
            case XG_PARAMERR: return "parameter error";
            case XG_AUTHFAIL: return "permission denied";
			case XG_DAYLIMIT: return "frequent operation";
            case XG_NOTFOUND: return "resource not found";
            case XG_DUPLICATE: return "resource duplicate";
			case XG_NOTCHANGED: return "resource not changed";
            default: return "system error";
        }
    }
	const map<string, string>& getFields() const
	{
		return fields;
	}
	Exception& extra(const string& name, const string& value)
	{
		fields[name] = value;
		return *this;
	}
	Exception(int code = 0, const string& msg = "") : errcode(code), errmsg(msg.empty() ? GetErrorString(code) : msg)
	{
	}
};

namespace stdx
{
	static string& tolower(string& str)
	{
		std::transform(str.begin(), str.end(), str.begin(), ::tolower);
		return str;
	}
	static string& toupper(string& str)
	{
		std::transform(str.begin(), str.end(), str.begin(), ::toupper);
		return str;
	}
	static string tolower(const string& str)
	{
		string res = str;
		return std::move(tolower(res));
	}
	static string toupper(const string& str)
	{
		string res = str;
		return std::move(toupper(res));
	}
	static int random(int begin = 0, int end = -1)
	{
		int div = end - begin;
		int val = abs((int)(rand()));

		if (div <= 0) return begin + val;

		return begin + val % div;
	}

	template<class DATA_TYPE> static DATA_TYPE sum(DATA_TYPE val)
	{
		return val;
	}
	template<class DATA_TYPE> static DATA_TYPE minval(DATA_TYPE val)
	{
		return val;
	}
	template<class DATA_TYPE> static DATA_TYPE maxval(DATA_TYPE val)
	{
		return val;
	}
	template<class DATA_TYPE, class ...ARGS> static DATA_TYPE sum(DATA_TYPE val, ARGS ...args)
	{
		return val + sum(args...);
	}
	template<class DATA_TYPE, class ...ARGS> static DATA_TYPE avg(DATA_TYPE val, ARGS ...args)
	{
		return sum(val, args...) / (sizeof...(args) + 1);
	}
	template<class DATA_TYPE, class ...ARGS> static DATA_TYPE minval(DATA_TYPE val, ARGS ...args)
	{
		return val < minval(args...) ? val : minval(args...);
	}
	template<class DATA_TYPE, class ...ARGS> static DATA_TYPE maxval(DATA_TYPE val, ARGS ...args)
	{
		return val > maxval(args...) ? val : maxval(args...);
	}

	static string str(bool val)
	{
		return val ? "true" : "false";
	}
	static string str(char* str)
	{
		return str ? str : "";
	}
	static string str(void* ptr)
	{
		return to_string((long long)(ptr));
	}
	static string str(const char* str)
	{
		return str ? str : "";
	}
	static string str(const void* ptr)
	{
		return to_string((long long)(ptr));
	}
	static string str(const string& str)
	{
		return str;
	}
	static bool endwith(const string& str, const string& tag)
	{
		return str.length() >= tag.length() && memcmp(str.c_str() + str.length() - tag.length(), tag.c_str(), tag.length()) == 0;
	}
	static bool startwith(const string& str, const string& tag)
	{
		return str.length() >= tag.length() && memcmp(str.c_str(), tag.c_str(), tag.length()) == 0;
	}
	template<class DATA_TYPE> static string str(const DATA_TYPE& val)
	{
		return to_string(val);
	}
	template<class DATA_TYPE, char BEGIN = '"', char END = 0> static string quote(const DATA_TYPE& val)
	{
		return END ? BEGIN + str(val) + END : BEGIN + str(val) + BEGIN;
	}

	int atoi(const char* str);
	double atof(const char* str);
	long long atol(const char* str);
	string format(const char* fmt, ...);
	int append(string& str, const string& msg);
	int format(string& str, const char* fmt, ...);
	int append(string& str, const char* fmt, ...);
	int vformat(string& str, const char* fmt, va_list args);

	string syscode(const string& str);
	string gbkcode(const string& str);
	string utfcode(const string& str);
	string translate(const string& str);
	string fill(const string& str, int len, bool left, char ch);
	string trim(const string& str, const string& space = " \r\n\t");
	string replace(const string& str, const string& src, const string& dest);

	vector<string> split(const string& str, const string& space);
	int split(vector<string>& vec, const string& str, const string& space);

	const string& EmptyString();
	string DecodeURL(const string& msg);
	string EncodeURL(const string& msg);
	SmartBuffer DecodeHex(const char* src, int len);
	SmartBuffer EncodeHex(const void* src, int len, bool upper = true);

	string GetParentPath(const string& path);
	string GetExtNameFromPath(const string& path);
	string GetFileNameFromPath(const string& path);
};

class ParamVector : public Object
{
	vector<string> datas;
	vector<E_PARAM_TYPE> types;

public:
	ParamVector()
	{
	}
	template<class DATA_TYPE> ParamVector(const DATA_TYPE& val)
	{
		add(val);
	}
	template<class DATA_TYPE, class ...ARGS> ParamVector(const DATA_TYPE& val, ARGS ...args)
	{
		add(val);
		add(args...);
	}

	void clear()
	{
		datas.clear();
		types.clear();
	}
	int size() const
	{
		return datas.size();
	}
	int asInt(int idx) const
	{
		return (int)asLong(idx);
	}
	long asLong(int idx) const
	{
		return *(long*)(datas[idx].c_str());
	}
	bool asBool(int idx) const
	{
		return stdx::atoi(asString(idx).c_str());
	}
	float asFloat(int idx) const
	{
		return (float)asDouble(idx);
	}
	double asDouble(int idx) const
	{
		return *(double*)(datas[idx].c_str());
	}
	string asString(int idx) const
	{
		switch(types[idx])
		{
			case eINT: return stdx::str(asLong(idx));
			case eDOUBLE: return stdx::str(asDouble(idx));
		}

		return datas[idx];
	}
	E_PARAM_TYPE type(int idx) const
	{
		return types[idx];
	}
	const string& data(int idx) const
	{
		return datas[idx];
	}
	string join(const string& spliter = ",")
	{
		return join(spliter, NULL);
	}
	string join(const string& spliter, function<string(string)> filter)
	{
		string res;
		size_t len = datas.size();

		if (len == 0) return res;

		if (filter)
		{
			for (size_t i = 0; i < len; i++)
			{
				res += spliter + filter(asString(i));
			}
		}
		else
		{
			for (size_t i = 0; i < len; i++)
			{
				res += spliter + asString(i);
			}
		}

		return res.substr(spliter.length());
	}
	string placeholders(const string& tag = "?", const string& spliter = ",") const
	{
		string res;
		size_t len = datas.size();

		if (len == 0) return res;

		while (len-- > 0) res += spliter + tag;

		return res.substr(spliter.length());
	}

	void add()
	{
		types.push_back(eNULL);
		datas.push_back(stdx::EmptyString());
	}
	void add(int val)
	{
		add((long)(val));
	}
	void add(long val)
	{
		types.push_back(eINT);
		datas.push_back(string((char*)(&val), (char*)(&val) + sizeof(long)));
	}
	void add(bool val)
	{
		types.push_back(eBOOL);
		datas.push_back(val ? "1" : "0");
	}
	void add(size_t val)
	{
		long tmp = val;
		types.push_back(eINT);
		datas.push_back(string((char*)(&tmp), (char*)(&tmp) + sizeof(long)));
	}
	void add(double val)
	{
		types.push_back(eDOUBLE);
		datas.push_back(string((char*)(&val), (char*)(&val) + sizeof(double)));
	}
	void add(char* val)
	{
		add((const char*)(val));
	}
	void add(const char* val)
	{
		types.push_back(val ? eSTRING : eNULL);
		datas.push_back(val ? val : "");
	}
	void add(const string& val)
	{
		types.push_back(eSTRING);
		datas.push_back(val);
	}
	void add(SmartBuffer buffer)
	{
		add((const void*)(buffer.str()), buffer.size());
	}
	void add(const void* data, int len)
	{
		types.push_back(data ? eBUFFER : eNULL);
		datas.push_back(data ? string((char*)(data), (char*)(data) + len) : stdx::EmptyString());
	}
	template<class DATA_TYPE> void add(const set<DATA_TYPE>& list)
	{
		for (const DATA_TYPE& val : list) add(val);
	}
	template<class DATA_TYPE> void add(const list<DATA_TYPE>& list)
	{
		for (const DATA_TYPE& val : list) add(val);
	}
	template<class DATA_TYPE> void add(const vector<DATA_TYPE>& list)
	{
		for (const DATA_TYPE& val : list) add(val);
	}
	template<class DATA_TYPE, class ...ARGS> void add(const DATA_TYPE& val, ARGS ...args)
	{
		add(val);
		add(args...);
	}
};

class StringCreator : public Object
{
	string content;
	string linespliter;

public:
	StringCreator() : linespliter("\r\n")
	{
	}
	StringCreator(const char* str) : content(str), linespliter("\r\n")
	{
	}
	StringCreator(const string& str) : content(str), linespliter("\r\n")
	{
	}

	void clear()
	{
		content.clear();
	}
	bool empty() const
	{
		return content.empty();
	}
	size_t size() const
	{
		return content.length();
	}
	size_t length() const
	{
		return content.length();
	}
	const char* str() const
	{
		return content.c_str();
	}
	string toString() const
	{
		return content;
	}
	const string& getContent() const
	{
		return content;
	}
	void setContent(const string& msg)
	{
		content = msg;
	}
	const string& getLineSpliter() const
	{
		return linespliter;
	}
	void setLineSpliter(const string& spliter)
	{
		linespliter = spliter;
	}

	StringCreator& tr()
	{
		content += linespliter;
		return *this;
	}
	StringCreator& puts(const char* str)
	{
		content += str;
		return tr();
	}
	StringCreator& puts(const string& str)
	{
		content += str;
		return tr();
	}
	StringCreator& printf(const char* fmt, ...)
	{
		va_list args;

		va_start(args, fmt);
		stdx::vformat(content, fmt, args);
		va_end(args);

		return *this;
	}

	StringCreator& operator << (char ch)
	{
		content.push_back(ch);
		return *this;
	}
	StringCreator& operator << (char* str)
	{
		content += str;
		return *this;
	}
	StringCreator& operator << (string& str)
	{
		content += str;
		return *this;
	}
	StringCreator& operator << (const char* str)
	{
		content += str;
		return *this;
	}
	StringCreator& operator << (const string& str)
	{
		content += str;
		return *this;
	}
	StringCreator& operator << (const Object& obj)
	{
		content += obj.toString();
		return *this;
	}
	template<class NUMBER_TYPE> StringCreator& operator << (NUMBER_TYPE val)
	{
		content += stdx::str(val);
		return *this;
	}
};

static string& operator += (string& str, const Object& obj)
{
	return str += obj.toString();
}

static std::ostream& operator << (std::ostream& out, const Object& obj)
{
	return out << obj.toString();
}

static StringCreator& operator << (StringCreator& out, const Object& obj)
{
	return out << obj.toString();
}

static string operator + (const string& str, const Object& obj)
{
	return str + obj.toString();
}
class ContentNode : public Object
{
protected:
	string valspliter;
	string keyspliter;
	string endspliter;
	vector<string> vec;
	map<string, string> content;

public:
	void clear();
	ContentNode();
	string toString() const;
	string& operator [] (const string& key);
	bool setOrder(const string& key, int idx);
	const string& getValue(const string& key) const;
	bool parse(const string& msg, bool inited = true);
	bool setValue(const string& key, const string& val, bool append);

	bool empty() const
	{
		return content.empty();
	}
	size_t size() const
	{
		return content.size();
	}
	const string& getKeySpliter() const
	{
		return keyspliter;
	}
	const string& getEndSpliter() const
	{
		return endspliter;
	}
	const string& getValueSpliter() const
	{
		return valspliter;
	}
	const vector<string>& getKeys() const
	{
		return vec;
	}
	void setKeySpliter(const string& spliter)
	{
		keyspliter = spliter;
	}
	void setEndSpliter(const string& spliter)
	{
		endspliter = spliter;
	}
	void setValueSpliter(const string& spliter)
	{
		valspliter = spliter;
	}
	const map<string, string>& getDataMap() const
	{
		return content;
	}
	void sort(std::function<bool(const string&, const string&)> func)
	{
		std::sort(vec.begin(), vec.end(), func);
	}
	template<class DATA_TYPE> bool setValue(const string& key, const DATA_TYPE& val)
	{
		return setValue(key, stdx::str(val), false);
	}
};
///////////////////////////////////////////////////////////////////
#endif