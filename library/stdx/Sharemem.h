#ifndef XG_SHAREMEM_H
#define XG_SHAREMEM_H
////////////////////////////////////////////////////////
#include "std.h"

class Sharemem : public Object
{
	struct Data
	{
		u_char* data = NULL;

		~Data();
		void close();
		u_char* get() const;
		bool init(HANDLE handle);
	};

	sp<Data> data;
	bool created = false;
	HANDLE handle = INVALID_HANDLE_VALUE;

public:
	CONSTRUCTOR_FORBID_COPY(Sharemem)

	~Sharemem();
	void close();
	int size() const;
	bool canUse() const;
	u_char* get() const;
	u_char* open(HANDLE handle);
	u_char* open(const string& name);
	u_char* create(const string& name, int memsz = 4096);
};
////////////////////////////////////////////////////////
#endif