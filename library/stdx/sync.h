#ifndef XG_SYNC_H
#define XG_SYNC_H
///////////////////////////////////////////////////////////////////
#include <set>
#include <map>
#include <list>
#include <cmath>
#include <queue>
#include <mutex>
#include <vector>
#include <string>
#include <memory>
#include <cctype>
#include <atomic>
#include <thread>
#include <iostream>
#include <algorithm>
#include <functional>

using namespace std;

#include "../clib/code.h"
#include "../clib/netx.h"

#define sp		shared_ptr
#define newsp	make_shared
#define extends virtual public

#define CONSTRUCTOR_FORBID_COPY(CLASS)	\
CLASS(){}								\
CLASS(const CLASS&) = delete;			\
CLASS& operator = (const CLASS&) = delete;

class Object
{
public:
	virtual ~Object();
	virtual string toString() const;
	virtual const char* getClassName() const;
	virtual string attrs(const string& exclude = "") const;

	template<class TYPE> static const char* GetClassName()
	{
#ifdef _MSC_VER
		return typeid(TYPE).name() + 6;
#else
		return SkipStartString(typeid(TYPE).name(), "0123456789");
#endif
	}
};

class Mutex : public Object
{
protected:
	mutex mtx;
	volatile u_int locked = 0;

public:
	CONSTRUCTOR_FORBID_COPY(Mutex)

	void lock()
	{
		locked = eWRITE;
		mtx.lock();
	}
	void unlock()
	{
		mtx.unlock();
		locked = 0;
	}
	bool isLocked() const
	{
		return locked > 0;
	}
};

class SpinMutex : public Object
{
	atomic_flag flag = ATOMIC_FLAG_INIT;

public:
	CONSTRUCTOR_FORBID_COPY(SpinMutex)

	void lock()
	{
		while (flag.test_and_set(memory_order_acquire));
	}
	void unlock()
	{
		flag.clear(std::memory_order_release);
	}
};

class ShareMutex : public Mutex
{
	Mutex mtx;
	volatile long num = 0;

public:
	CONSTRUCTOR_FORBID_COPY(ShareMutex)

	void lock()
	{
		while (locked == eWRITE) std::this_thread::yield();

		mtx.lock();

		if (num++ == 0)
		{
			Mutex::lock();
			locked = eREAD;
		}

		mtx.unlock();
	}
	void unlock()
	{
		mtx.lock();

		if (--num == 0) Mutex::unlock();

		mtx.unlock();
	}
};

typedef lock_guard<Mutex> Locker;
typedef lock_guard<SpinMutex> SpinLocker;
typedef lock_guard<ShareMutex> ShareLocker;


class Thread : public Object
{
public:
	bool start();
	virtual void run() = 0;
};

class WorkItem : public Object
{
	friend class TaskQueue;

	volatile int done = 0;

public:
	bool isDone() const
	{
		return done != 0;
	}
	bool isErrorDone() const
	{
		return done == XG_ERROR;
	}

	virtual void run();
	virtual bool runnable();

	void wait() const;
	bool wait(int timeout) const;
};

template<typename T> class TSQueue : public Object
{
	size_t maxsz;
	queue<T> dataqueue;
	mutable SpinMutex mtx;

public:
	bool pop(T& val)
	{
		SpinLocker lk(mtx);
		CHECK_FALSE_RETURN(dataqueue.size() > 0);
		val = dataqueue.front();
		dataqueue.pop();
		return true;
	}
	bool empty() const
	{
		SpinLocker lk(mtx);
		return dataqueue.empty();
	}
	size_t size() const
	{
		SpinLocker lk(mtx);
		return dataqueue.size();
	}
	bool push(const T& val)
	{
		SpinLocker lk(mtx);
		if (maxsz > 0 && dataqueue.size() >= maxsz) return false;
		dataqueue.push(val);
		return true;
	}
	TSQueue(size_t maxsz = 0)
	{
		this->maxsz = maxsz;
	}
};

template<typename KEY, typename VAL> class TSMap : public Object
{
	map<KEY, VAL> datamap;
	mutable SpinMutex mtx;

public:
	void clear()
	{
		SpinLocker lk(mtx);
		datamap.clear();
	}
	bool empty() const
	{
		SpinLocker lk(mtx);
		return datamap.empty();
	}
	size_t size() const
	{
		SpinLocker lk(mtx);
		return datamap.size();
	}
	bool remove(const KEY& key)
	{
		SpinLocker lk(mtx);
		auto it = datamap.find(key);
		if (it == datamap.end()) return false;
		datamap.erase(it);
		return true;
	}
	void clear(function<bool(VAL&)> func)
	{
		SpinLocker lk(mtx);
		auto it = datamap.begin();
		while (it != datamap.end())
		{
			if (func(it->second))
			{
				datamap.erase(it++);
			}
			else
			{
				++it;
			}
		}
	}
	int get(map<KEY, VAL>& resmap) const
	{
		SpinLocker lk(mtx);
		for (const auto& item : datamap) resmap[item.first] = item.second;
		return resmap.size();
	}
	bool get(const KEY& key, VAL& val) const
	{
		SpinLocker lk(mtx);
		auto it = datamap.find(key);
		if (it == datamap.end()) return false;
		val = it->second;
		return true;
	}
	void set(const map<KEY, VAL>& attrmap)
	{
		SpinLocker lk(mtx);
		for (const auto& item : attrmap) datamap[item.first] = item.second;
	}
	void set(const KEY& key, const VAL& val)
	{
		SpinLocker lk(mtx);
		datamap[key] = val;
	}
	bool update(const KEY& key, function<void(VAL&)> func)
	{
		SpinLocker lk(mtx);
		auto it = datamap.find(key);
		if (it == datamap.end()) return false;
		func(it->second);
		return true;
	}
	size_t get(const vector<KEY> keylist, map<KEY, VAL>& resmap)
	{
		SpinLocker lk(mtx);
		for (const KEY& key : keylist)
		{
			auto it = datamap.find(key);
			if (it == datamap.end()) continue;
			resmap[key] = it->second;
		}
		return resmap.size();
	}
};

template <typename KEY, typename VAL> class CacheMap
{
	class Head
	{
	public:
		KEY key;
		long weight = 0;

		bool operator < (const Head& obj) const
		{
			return weight < obj.weight;
		}
	};

	class Node
	{
	public:
		VAL data;
		Head head;

		Node()
		{
		}
		Node(const KEY& key, const VAL& val, int weight) : data(val)
		{
			head.key = key;
			head.weight = weight;
		}
	};

	size_t maxsz;
	mutable SpinMutex mtx;
	map<KEY, Node> datamap;
	function<void(long&)> func;

	int check()
	{
		long avg = 0;
		auto it = datamap.begin();
		priority_queue<Head> queue;
		const int maxlen = datamap.size() / 8;

		for (int i = 0; i < maxlen; i++)
		{
			avg += it->second.head.weight;
			queue.push(it->second.head);
			++it;
		}

		while (it != datamap.end())
		{
			Head& data = it->second.head;

			if (data.weight < queue.top().weight)
			{
				queue.pop();
				queue.push(data);
			}

			avg += data.weight;
			++it;
		}

		avg /= datamap.size();

		for (int i = 0; i < maxlen; i++)
		{
			datamap.erase(queue.top().key);
			queue.pop();
		}

		return avg;
	}

public:
	void clear()
	{
		SpinLocker lk(mtx);
		datamap.clear();
	}
	bool empty() const
	{
		SpinLocker lk(mtx);
		return datamap.empty();
	}
	size_t size() const
	{
		SpinLocker lk(mtx);
		return datamap.size();
	}
	bool remove(const KEY& key)
	{
		SpinLocker lk(mtx);
		auto it = datamap.find(key);
		if (it == datamap.end()) return false;
		datamap.erase(it);
		return true;
	}
	CacheMap(size_t maxsz = 10000)
	{
		this->func = [](long& val){
			++val;
		};
		this->init(maxsz);
	}
	void init(size_t maxsz = 10000)
	{
		this->maxsz = maxsz > 8 ? maxsz : 8;
	}
	void clear(function<bool(VAL&)> func)
	{
		vector<KEY> vec;
		SpinLocker lk(mtx);
		for (auto& item : datamap)
		{
			if (func(item.second.data)) vec.push_back(item.first);
		}
		for (auto& item : vec) datamap.erase(item);
	}
	bool get(const KEY& key, VAL& val)
	{
		SpinLocker lk(mtx);
		auto it = datamap.find(key);
		if (it == datamap.end()) return false;
		val = it->second.data;
		func(it->second.head.weight);
		return true;
	}
	void weight(function<void(long&)> func)
	{
		this->func = func;
	}
	void set(const KEY& key, const VAL& val)
	{
		SpinLocker lk(mtx);
		auto it = datamap.find(key);
		if (it == datamap.end())
		{
			int weight = datamap.size() >= maxsz ? check() : 0;
			datamap[key] = Node(key, val, weight);
		}
		else
		{
			it->second.data = val;
		}
	}
	bool update(const KEY& key, function<void(VAL&)> func)
	{
		SpinLocker lk(mtx);
		auto it = datamap.find(key);
		if (it == datamap.end()) return false;
		func(it->second.data);
		return true;
	}
	size_t get(const vector<KEY> keylist, map<KEY, VAL>& resmap)
	{
		SpinLocker lk(mtx);
		for (const KEY& key : keylist)
		{
			auto it = datamap.find(key);
			if (it == datamap.end()) continue;
			resmap[key] = it->second.data;
			func(it->second.head.weight);
		}
		return resmap.size();
	}
};

class TaskQueue : public Object
{
	class Queue
	{
		size_t str = 0;
		size_t end = 0;
		size_t maxsz = 0;
		sp<WorkItem>* arr = NULL;

	public:
		~Queue()
		{
			init(0);
		}
		bool empty() const
		{
			return str == end;
		}
		size_t size() const
		{
			return end < str ? maxsz + end - str + 1 : end - str;
		}
		size_t capacity() const
		{
			return maxsz;
		}
		void init(size_t maxsz)
		{
			if (arr) delete[] arr;
			if (maxsz > 0) arr = new sp<WorkItem>[maxsz + 1];
			this->maxsz = maxsz;
			this->str = 0;
			this->end = 0;
		}
		size_t pop(sp<WorkItem>& val)
		{
			size_t len = size();
			if (len == 0) return 0;
			std::swap(val, arr[str++]);
			if (str > maxsz) str = 0;
			return len;
		}
		size_t push(sp<WorkItem>& val)
		{
			size_t len = size();
			if (len++ == maxsz) return 0;
			std::swap(val, arr[end++]);
			if (end > maxsz) end = 0;
			return len;
		}
	};

	Queue queue;
	size_t threads = 0;
	mutable SpinMutex mtx;
	mutable SpinMutex desmtx;
	atomic_int curdepth = {0};
	atomic_int threadcount = {0};
	map<long, function<void()>> destroymap;

	size_t pop(sp<WorkItem>& item)
	{
		SpinLocker lk(mtx);
		return queue.pop(item);
	}

public:
	void run();
	bool push(sp<WorkItem> item);
	bool start(size_t threads = 4, size_t maxsz = 1000000);

	void cancelDestroyFunc();
	bool removeDestroyFunc();
	bool setDestroyFunc(function<void()> func);

	void stop()
	{
		threads = 0;
	}
	void wait()
	{
		while (curdepth > 0) Sleep(1);
	}
	bool empty() const
	{
		SpinLocker lk(mtx);
		return queue.empty();
	}
	size_t size() const
	{
		SpinLocker lk(mtx);
		return queue.size();
	}
	size_t getThreads() const
	{
		return threads;
	}

	static TaskQueue* Instance();
};

class TimerTaskQueue : public Object
{
	struct Task
	{
		int delay = -1;
		int times = -1;
		int maxtimes = -1;
		long long rtime = -1;
		sp<WorkItem> task = NULL;

		bool update(long long now);

		bool operator < (const Task& task) const
		{
			return rtime > task.rtime;
		}
		bool init(int delay, int maxtimes, const sp<WorkItem>& task, long long now = 0)
		{
			if (now <= 0) CHECK_FALSE_RETURN((now = GetTime() / 1000) > 0);

			if (task) this->task = task;
			this->maxtimes = maxtimes;
			this->delay = delay;
			this->times = -1;

			return update(now);
		}
	};

	SpinMutex mtx;
	int started = 0;
	priority_queue<Task> queue;

	bool get(Task& task);

public:
	void run();
	void check(function<bool(sp<WorkItem>)> filter);
	bool push(sp<WorkItem> task, int delay, int maxtimes = 0);
	bool daily(sp<WorkItem> task, const string& time, int maxtimes = 0);
	bool update(int delay, int maxtimes, function<bool(sp<WorkItem>)> filter);

	static TimerTaskQueue* Instance();
};

namespace stdx
{
	sp<WorkItem> async(sp<WorkItem> item);
	sp<WorkItem> async(function<void()> func);
	bool parallel(vector<function<void()>> items);

	static bool parallel(function<void()> a)
	{
		return parallel({a});
	}
	static bool parallel(function<void()> a, function<void()> b)
	{
		return parallel({a, b});
	}
	static bool parallel(function<void()> a, function<void()> b, function<void()> c)
	{
		return parallel({a, b, c});
	}
	static bool parallel(function<void()> a, function<void()> b, function<void()> c, function<void()> d)
	{
		return parallel({a, b, c, d});
	}
	static bool parallel(function<void()> a, function<void()> b, function<void()> c, function<void()> d, function<void()> e)
	{
		return parallel({a, b, c, d, e});
	}
	static bool parallel(function<void()> a, function<void()> b, function<void()> c, function<void()> d, function<void()> e, function<void()> f)
	{
		return parallel({a, b, c, d, e, f});
	}
	static bool parallel(function<void()> a, function<void()> b, function<void()> c, function<void()> d, function<void()> e, function<void()> f, function<void()> g)
	{
		return parallel({a, b, c, d, e, f, g});
	}
	static bool parallel(function<void()> a, function<void()> b, function<void()> c, function<void()> d, function<void()> e, function<void()> f, function<void()> g, function<void()> h)
	{
		return parallel({a, b, c, d, e, f, g, h});
	}
	
	void timer(int delay, sp<WorkItem> item);
	void timer(int delay, function<void()> func);
	void delay(int delay, sp<WorkItem> item, int maxtimes = 1);
	void delay(int delay, function<void()> func, int maxtimes = 1);
};
///////////////////////////////////////////////////////////////////
#endif