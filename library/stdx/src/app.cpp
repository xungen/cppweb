#ifndef XG_APP_CPP
#define XG_APP_CPP
///////////////////////////////////////////////////////////////////
#include "../cmd.h"
#include "../File.h"

#ifdef XG_LINUX

#include <dirent.h>
#include <sys/stat.h>
#include <sys/wait.h>

int GetLastError()
{
	return errno;
}
string GetErrorString(int errcode)
{
	return strerror(errcode);
}

#else

QueryFullProcessPathFunc QueryFullProcessPath = NULL;

string GetErrorString(int errcode)
{
	string msg;
	HLOCAL LocalAddress = NULL;

	FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM, NULL, errcode, 0, (LPSTR)(&LocalAddress), 0, NULL);
	if (LocalAddress == NULL) return msg;
	msg = (LPSTR)(LocalAddress);
	LocalFree(LocalAddress);

	return stdx::trim(stdx::utfcode(msg));
}

#endif

string GetErrorString()
{
	return GetErrorString(GetLastError());
}

Process::Process()
{
	srand(time(&startime) + clock());
	argv = NULL;
}
int Process::init(int argc, char** argv)
{
	this->vec.clear();
	this->argv = argv;

	const char* p = strrchr(argv[0], '/');

	if (p == NULL) p = strrchr(argv[0], '\\');

	appname = p ? p + 1 : argv[0];

	size_t pos = appname.rfind('.');

	if (pos != string::npos && pos > 0) appname = appname.substr(0, pos);

	for (int i = 0; i < argc; i++)
	{
		const char* val = argv[i][0] == '\\' ? argv[i] + 1 : argv[i];
#ifdef XG_LINUX
		vec.push_back(val);
#else
		vec.push_back(stdx::utfcode(val));
#endif
	}

	return vec.size();
}
const char* Process::getAppname() const
{
	return appname.c_str();
}
int Process::getCmdParamCount() const
{
	return vec.size();
}
const char* Process::getCmdParam(int index) const
{
	return index < vec.size() ? vec[index].c_str() : NULL;
}
const char* Process::getCmdParam(const string& key) const
{
	if (vec.empty()) return NULL;

	for (size_t i = 0; i < vec.size(); i++)
	{
		if (key == vec[i])
		{
			if (i + 1 >= vec.size()) return "";
			if (vec[i + 1].empty()) return "";

			return vec[i + 1].c_str();
		}
	}

	return NULL;
}
Mutex& Process::getMutex()
{
	return mutex;
}
char** Process::getCmdParam() const
{
	return argv;
}
int Process::getParamCount() const
{
	SpinLocker lk(mtx);
	return paramap.size();
}
void Process::setParam(const string& key, const string& val)
{
	SpinLocker lk(mtx);
	paramap[key] = val;
}
string Process::getParam(const string& key) const
{
	string val;
	SpinLocker lk(mtx);
	auto it = paramap.find(key);

	if (it == paramap.end()) return stdx::EmptyString();

	val = it->second;

	return std::move(val);
}
int Process::getObjectCount() const
{
	SpinLocker lk(mtx);
	return objmap.size();
}
void Process::setObject(const string& key, const void* obj)
{
	SpinLocker lk(mtx);
	objmap[key] = obj;
}
const void* Process::getObject(const string& key) const
{
	SpinLocker lk(mtx);
	auto it = objmap.find(key);

	if (it == objmap.end()) return NULL;

	return it->second;
}
Process* Process::Instance()
{
	static Process* ptr = NULL;

	if (ptr) return ptr;

	Process::GetGlobalVariable("PROCESS_INSTANCE", ptr, true);

	if (ptr == NULL) ErrorExit(XG_SYSERR);

	if (CheckSystemSizeof())
	{
#ifndef XG_LINUX
		HMODULE handle;

		if (handle = LoadLibraryA("KERNEL32.dll"))
		{
			if ((QueryFullProcessPath = (QueryFullProcessPathFunc)GetProcAddress(handle, "QueryFullProcessImageNameA")) == NULL)
			{
				FreeLibrary(handle);
			}
		}
#endif
	}
	else
	{
		ErrorExit(XG_SYSERR);
	}

	return ptr;
}
Process* Process::Instance(int argc, char** argv)
{
	GetLocaleCharset();

	Instance()->init(argc, argv);

	if (stdx::GetProcessExePath() == NULL) ErrorExit(XG_SYSERR);

	return Instance();
}
Application* Process::GetApplication(Application* obj)
{
	static Application* app = (Application*)GetObject("{APPLICATION}");

	if (app == NULL) SetObject("{APPLICATION}", app = obj);

	return app;
}

void Process::CheckSingle()
{
	string exepath;
	vector<ProcessData> vec;

	if (Process::GetProcessExePath(exepath) && Process::GetSystemProcessList(vec) > 0)
	{
		PROCESS_T pid = Process::GetCurrentProcess();
		
		for (ProcessData& item : vec)
		{
			if (exepath == item.path && pid != item.id)
			{
#ifdef XG_LINUX
				ColorPrint(eYELLOW, "there is a running instance[%ld]\n", (long)(item.id));
#else
				MessageBoxW(NULL, L"当前应用已有一个正在运行的实例", L"正在运行", MB_OK | MB_SYSTEMMODAL | MB_ICONSTOP);
#endif
				ErrorExit(XG_OK);
			}
		}
	}
	else
	{
		ErrorExit(XG_SYSERR);
	}
}
bool Process::IsStartedByExplorer()
{
	static int flag = -1;

	if (flag >= 0) return flag > 0;

#ifdef XG_LINUX
	flag = 0;
#else
	string exepath;
	PROCESS_T handle = Process::GetParentProcess(Process::GetCurrentProcess());

	CHECK_FALSE_RETURN(handle > 0);
	CHECK_FALSE_RETURN(GetProcessExePath(handle, exepath));

	if (string::npos == stdx::tolower(exepath).find("explorer.exe"))
	{
		flag = 0;
	}
	else
	{
		flag = 1;
	}
#endif

	return flag > 0;
}
bool Process::SetCommonExitSignal()
{
#ifdef XG_LINUX
	signal(SIGINT, CommonExitFunc);
	signal(SIGHUP, CommonExitFunc);
	signal(SIGQUIT, CommonExitFunc);
	signal(SIGTERM, CommonExitFunc);
#endif

	return true;
}
void Process::CommonExitFunc(int signum)
{
#ifdef XG_LINUX
	if (signum == SIGHUP)
	{
		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);
		signal(signum, SIG_IGN);

		return;
	}
#endif

	if (GetApplication()) GetApplication()->clean();

#ifdef XG_LINUX
	sync();
	sleep(1);
	_exit(signum);
#else
	TerminateProcess(::GetCurrentProcess(), signum);
#endif
}
bool Process::SetCurrentDirectory(const string& path)
{
#ifdef XG_LINUX
	return chdir(path.c_str()) >= 0;
#else
	string tmp = stdx::syscode(path);

	if (tmp.back() == ':') tmp.push_back('/');

	return SetCurrentDirectoryA(tmp.c_str()) ? true : false;
#endif
}
bool Process::SetEnv(const string& key, const string& val)
{
#ifdef XG_LINUX
	return setenv(key.c_str(), val.c_str(), 1) == 0;
#else
	return SetEnvironmentVariableA(key.c_str(), stdx::syscode(val).c_str()) ? true : false;
#endif
}
string Process::GetEnv(const string& key)
{
#ifdef XG_LINUX
	const char* env = getenv(key.c_str());

	if (env == NULL) return stdx::EmptyString();

	return env;
#else
	char env[1024] = {0};

	if (GetEnvironmentVariableA(key.c_str(), env, sizeof(env) - 1) > 0) return stdx::utfcode(env);

	return stdx::EmptyString();
#endif
}
bool Process::RegisterLibraryPath(const string& path)
{
#ifdef XG_LINUX
	string key = "LD_LIBRARY_PATH";
#else
	string key = "PATH";
#endif
	if (path.empty())
	{
		return RegisterVariable(key, path::parent(proc::exepath()));
	}
	else
	{
		return RegisterVariable(key, path);
	}
}
bool Process::RegisterExecutablePath(const string& path)
{
	if (path.empty())
	{
		return RegisterVariable("PATH", path::parent(proc::exepath()));
	}
	else
	{
		return RegisterVariable("PATH", path);
	}
}
bool Process::RegisterVariable(const string& key, const string& val)
{
#ifdef XG_LINUX
	string gap = ":";
	string str = val;
#else
	string gap = ";";
	string str = stdx::replace(val, "/", "\\");
#endif
	string tmp = stdx::trim(GetEnv(key), gap);

	if (tmp.empty())
	{
		tmp = str;
	}
	else if (tmp.find(str) == string::npos)
	{
		tmp += gap + str;
	}

	return SetEnv(key, tmp);
}
PROCESS_T Process::GetParentProcess(PROCESS_T handle)
{
	vector<ProcessData> vec;

	if (GetSystemProcessList(vec) <= 0) return XG_ERROR;

	for (auto& item : vec)
	{
		if (handle == item.id) return item.pid;
	}

	return XG_ERROR;
}
int Process::GetSystemProcessListByName(vector<ProcessData>& vec, const string& name)
{
	int res = 0;
	vector<ProcessData> tmp;

	if (GetSystemProcessList(tmp) < 0) return XG_ERROR;

	for (auto& item : tmp)
	{
		if (name == item.name)
		{
			vec.push_back(item);
			res++;
		}
	}

	return res;
}
int Process::GetSystemProcessListByExeName(vector<ProcessData>& vec, const string& path)
{
	int res = 0;
	vector<ProcessData> tmp;

	GetSystemProcessList(tmp);

	for (auto& item : tmp)
	{
		if (path == item.path)
		{
			vec.push_back(item);
			res++;
		}
	}

	return res;
}
bool Process::GetProcessExePath(string& path)
{
	path = stdx::GetProcessExePath();

	return path.length() > 0;
}
bool Process::SetDaemonCommand(const string& cmd)
{
	char* data;
	Sharemem shm;

	CHECK_FALSE_RETURN(data = (char*)shm.open(GetEnv(DAEMON_ENVNAME)));

	strncpy(data, cmd.c_str(), 1000);

	return true;
}

#ifdef XG_LINUX

#include <signal.h>

PROCESS_T Process::GetCurrentProcess()
{
	static PROCESS_T pid = (PROCESS_T)(getpid());

	return pid;
}
string Process::GetCurrentDirectory()
{
	char buffer[MAX_PATH];

	if (getcwd(buffer, sizeof(buffer)) == NULL) return stdx::EmptyString();

	return buffer;
}
bool Process::Wait(PROCESS_T handle)
{
	return waitpid(handle, NULL, 0) > 0;
}
bool Process::Kill(PROCESS_T handle, int flag)
{
	return kill(handle, flag > 0 ? flag : SIGKILL) == 0;
}
bool Process::InitDaemon()
{
	PROCESS_T handle = fork();

	if (handle == 0)
	{
		setsid();
	}
	else if (handle > 0)
	{
		ErrorExit(0);
	}

	return handle >= 0;
}
bool Process::GetProcessExePath(PROCESS_T handle, string& path)
{
	char buffer[MAX_PATH] = {0};

	sprintf(buffer, "/proc/%d/exe", (int)(handle));

	if (readlink(buffer, buffer, sizeof(buffer)) > 0)
	{
		size_t pos = (path = buffer).find(" (deleted)");

		if (pos != string::npos && pos + 10 == path.length()) path = path.substr(0, pos);
	}
	else
	{
		path.clear();
	}
	
	return path.length() > 0;
}
int Process::GetSystemProcessList(vector<ProcessData>& vec)
{
	FILE* fp;
	int res = 0;
	ProcessData data;
	vector<string> v;
	char buffer[MAX_PATH];

	if (stdx::GetFolderContent(v, "/proc", ePATH) < 0) return XG_ERROR;

	for (string& item : v)
	{
		data.id = (PROCESS_T)(atoi(item.c_str()));

		sprintf(buffer, "/proc/%s/status", item.c_str());

		fp = fopen(buffer, "r");

		if (fp == NULL) continue;

		int num = 0;

		while (fgets(buffer, sizeof(buffer), fp))
		{
			if (memcmp(buffer, "Name:", 5) == 0)
			{
				++num;

				data.name = stdx::trim(buffer + 5);
			}
			else if (memcmp(buffer, "PPid:", 5) == 0)
			{
				++num;

				data.pid = (PROCESS_T)(atoi(stdx::trim(buffer + 5).c_str()));
			}

			if (num >= 2) break;
		}

		fclose(fp);

		if (num < 2) continue;

		data.path = stdx::EmptyString();

		GetProcessExePath(data.id, data.path);

		vec.push_back(data);
		res++;
	}

	return res;
}

#else

#include <tlhelp32.h>

#ifdef _MSC_VER
#pragma comment(lib,"advapi32.lib")
#endif

HANDLE Process::GetProcessHandle(PROCESS_T id)
{
	return OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ | PROCESS_TERMINATE, FALSE, id);
}
bool Process::EnableDebugPrivilege(HANDLE handle)
{
	HANDLE token = NULL;
	TOKEN_PRIVILEGES tkp;

	tkp.PrivilegeCount = 1;

	auto task = [&](){
		CHECK_FALSE_RETURN(OpenProcessToken(handle, TOKEN_ADJUST_PRIVILEGES, &token));
		CHECK_FALSE_RETURN(LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &tkp.Privileges[0].Luid));

		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

		CHECK_FALSE_RETURN(AdjustTokenPrivileges(token, FALSE, &tkp, sizeof(tkp), NULL, NULL));

		return true;
	};

	bool res = task();

	if (HandleCanUse(token)) Close(token);

	return res;
}
PROCESS_T Process::GetCurrentProcess()
{
	static PROCESS_T pid = GetCurrentProcessId();

	return pid;
}
string Process::GetCurrentDirectory()
{
	char buffer[MAX_PATH];
	DWORD len = GetCurrentDirectoryA(sizeof(buffer), buffer);

	if (len == 0 || len >= sizeof(buffer)) return stdx::EmptyString();

	return stdx::replace(stdx::utfcode(buffer), "\\", "/");
}
bool Process::Wait(PROCESS_T handle)
{
	bool res = true;
	HANDLE tmp = GetProcessHandle(handle);

	CHECK_FALSE_RETURN(HandleCanUse(tmp));

	if (WaitForSingleObject(tmp, INFINITE) == 0xFFFFFFFF) res = false;

	CloseHandle(tmp);

	return res;
}
bool Process::Kill(PROCESS_T handle, int flag)
{
	bool res = true;
	HANDLE tmp = GetProcessHandle(handle);
	
	CHECK_FALSE_RETURN(HandleCanUse(tmp));

	res = (TerminateProcess(tmp, flag) == TRUE);

	CloseHandle(tmp);

	return res;
}
bool Process::InitDaemon()
{
	fclose(stdout);
	
	return true;
}
bool Process::GetProcessExePath(PROCESS_T handle, string& path)
{
	char buffer[MAX_PATH];
	DWORD dw = sizeof(buffer);
	HANDLE tmp = GetProcessHandle(handle);

	CHECK_FALSE_RETURN(HandleCanUse(tmp));

	if (QueryFullProcessPath)
	{
		dw = QueryFullProcessPath(tmp, 0, buffer, &dw);
	}
	else
	{
		dw = GetModuleFileNameExA(tmp, NULL, buffer, dw);
	}

	CloseHandle(tmp);

	CHECK_FALSE_RETURN(dw > 0);

	path = stdx::replace(stdx::utfcode(buffer), "\\", "/");

	return true;
}
int Process::GetSystemProcessList(vector<ProcessData>& vec)
{
	int res = 0;
	HANDLE handle;
	ProcessData data;
	PROCESSENTRY32W pe;
	char buffer[MAX_PATH];

	pe.dwSize = sizeof(pe);
	handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (handle == INVALID_HANDLE_VALUE) return XG_ERROR;

	BOOL flag = Process32FirstW(handle, &pe);

	while (flag)
	{
		if (pe.szExeFile[0] == 0)
		{
			buffer[0] = 0;
		}
		else
		{
			if (WideCharToMultiByte(CP_ACP, 0, pe.szExeFile, -1, buffer, (int)(wcslen(pe.szExeFile) * 2), NULL, NULL) == 0)
			{
				CloseHandle(handle);

				return XG_ERROR;
			}
		}

		data.name = buffer;
		data.id = pe.th32ProcessID;
		data.path = stdx::EmptyString();

		GetProcessExePath(data.id, data.path);

		data.pid = pe.th32ParentProcessID;
		vec.push_back(data);
		res++;

		flag = Process32Next(handle, &pe);
	}

	CloseHandle(handle);

	return res;
}

#endif

bool Application::main()
{
	return true;
}
void Application::clean()
{
}
void Application::puts(const string& msg)
{
	::puts(stdx::syscode(msg).c_str());
}
void Application::printf(const char* fmt, ...)
{
	string str;
	va_list args;

	va_start(args, fmt);
	stdx::vformat(str, fmt, args);
	va_end(args);

	str = stdx::syscode(str);
	fwrite(str.c_str(), 1, str.length(), stdout);
}

DllFile::~DllFile()
{
	close();
}
void DllFile::close()
{
	if (handle) DllFileClose(handle);

	handle = NULL;
	ctime = 0;
	path.clear();
}
bool DllFile::open(const string& path)
{
	close();

	CHECK_FALSE_RETURN(Process::SetEnv("{LAST_OPEN_DLLPATH}", path));

	if (handle = DllFileOpen(path.c_str()))
	{
		this->ctime = time(NULL);
		this->path = path;

		return true;
	}
	
	string msg = DllFile::GetErrorString();

	if (msg.empty())
	{
		LogTrace(eERR, "load dynamic library[%s] failed", path.c_str());
	}
	else
	{
		LogTrace(eERR, "load dynamic library[%s] failed[%s]", path.c_str(), msg.c_str());
	}
	
	return false;
}
void* DllFile::getAddress(const string& name) const
{
	void* ptr = DllFileGetAddress(handle, name.c_str());

	if (ptr) return ptr;

	string msg = DllFile::GetErrorString();

	if (msg.empty())
	{
		LogTrace(eERR, "read dynamic link[%s][%s] failed", path.c_str(), name.c_str());
	}
	else
	{
		LogTrace(eERR, "read dynamic link[%s][%s] failed[%s]", path.c_str(), name.c_str(), msg.c_str());
	}
	
	return ptr;
}

SpinMutex* DllFile::GetMutex()
{
	XG_DEFINE_GLOBAL_VARIABLE(SpinMutex)
}
string DllFile::GetLastPath()
{
	return Process::GetEnv("{LAST_OPEN_DLLPATH}");
}
string DllFile::GetErrorString()
{
#ifdef XG_LINUX
	const char* msg = dlerror();
		
	return msg ? msg : stdx::EmptyString();
#else
	return ::GetErrorString();
#endif
}
bool DllFile::Remove(sp<DllFile> dll)
{
	CHECK_FALSE_RETURN(dll);

	return Remove(dll->getPath());
}
bool DllFile::Remove(const string& path)
{
	static SpinMutex& mtx = *GetMutex();
	static ObjectMap& dllmap = *GetObjectMap();

	SpinLocker lk(mtx);
	auto it = dllmap.find(path);

	if (it == dllmap.end()) return false;

	dllmap.erase(it);

	return true;
}
DllFile::ObjectMap* DllFile::GetObjectMap()
{
	XG_DEFINE_GLOBAL_VARIABLE(ObjectMap)
}
sp<DllFile> DllFile::Get(const string& path, bool created, bool saved)
{
	sp<DllFile> dll;
	static SpinMutex& mtx = *GetMutex();
	static ObjectMap& dllmap = *GetObjectMap();

	SpinLocker lk(mtx);
	auto it = dllmap.find(path);
	
	if (it == dllmap.end())
	{
		if (created)
		{
			dll = newsp<DllFile>();

			if (dll->open(path))
			{
				if (saved) dllmap.insert(pair<string, sp<DllFile>>(path, dll));
			}
			else
			{
				dll = NULL;
			}
		}
	}
	else
	{
		dll = it->second;
	}

	return dll;
}

const char* stdx::GetProcessExePath()
{
	DWORD dw = MAX_PATH;
	static char buffer[4 * MAX_PATH];

	if (isalpha(*buffer)) return buffer;

#ifdef XG_LINUX
	sprintf(buffer, "/proc/%d/exe", getpid());

	if (readlink(buffer, buffer, sizeof(buffer)) < 0) return NULL;
#else
	char* ptr = buffer;

	if (QueryFullProcessPath)
	{
		dw = QueryFullProcessPath(GetCurrentProcess(), 0, buffer, &dw);
	}
	else
	{
		dw = GetModuleFileNameExA(GetCurrentProcess(), NULL, buffer, dw);
	}

	if (dw <= 0) return NULL;

	while (*ptr)
	{
		if (*ptr == '\\') *ptr = '/';

		++ptr;
	}

	strncpy(buffer, stdx::utfcode(buffer).c_str(), sizeof(buffer) - 1);
#endif

	return buffer;
}

///////////////////////////////////////////////////////////////////
#endif