#ifndef XG_STD_CPP
#define XG_STD_CPP
///////////////////////////////////////////////////////////////////
#include "../cmd.h"
#include "../Reflect.h"

string Exception::toString() const
{
	return stdx::format("%s[%d][%s]", getClassName(), errcode, errmsg.c_str());
}
const char* Exception::what() const noexcept
{
	return errmsg.c_str();
}

int stdx::atoi(const char* str)
{
	int n;
	int sign;
	const char* p = str;

	if (str == NULL || *str == 0) return 0;

	while (isspace(*p)) p++;

	sign = ('-' == *p) ? -1 : 1;

	if ('+' == *p || '-' == *p) p++;

	for (n = 0; isdigit(*p); p++) n = 10 * n + (*p - '0');

	return sign * n;
}
double stdx::atof(const char* str)
{
	return str && *str ? ::atof(str) : 0.0;
}
long long stdx::atol(const char* str)
{
	long long n;
	long long sign;
	const char* p = str;

	if (str == NULL || *str == 0) return 0;

	while (isspace(*p)) p++;

	sign = ('-' == *p) ? -1 : 1;

	if ('+' == *p || '-' == *p) p++;

	for (n = 0; isdigit(*p); p++) n = 10 * n + (*p - '0');

	return sign * n;
}
string stdx::format(const char* fmt, ...)
{
	int len;
	string str;
	va_list args;

	va_start(args, fmt);
	len = vformat(str, fmt, args);
	va_end(args);

	return std::move(str);
}
int stdx::format(string& str, const char* fmt, ...)
{
	int len;
	va_list args;

	str.clear();

	va_start(args, fmt);
	len = vformat(str, fmt, args);
	va_end(args);

	return len;
}
int stdx::append(string& str, const string& msg)
{
	return (str += msg).length();
}
int stdx::append(string& str, const char* fmt, ...)
{
	int len;
	va_list args;

	va_start(args, fmt);
	len = vformat(str, fmt, args);
	va_end(args);

	return len;
}
int stdx::vformat(string& str, const char* fmt, va_list args)
{
	int len;
	va_list dest;
	char buffer[64 * 1024];

	va_copy(dest, args);
	len = vsnprintf(buffer, sizeof(buffer), fmt, args);

	if (len > 0 && len < sizeof(buffer))
	{
		str += buffer;

		return len;
	}

	SmartBuffer tmp(len + sizeof(char));

	len = vsnprintf(tmp.str(), tmp.size(), fmt, dest);

	if (len > 0 && len < tmp.size()) str += tmp.str();

	return len;
}

const string& stdx::EmptyString()
{
	static string str;

	return str;
}
string stdx::DecodeURL(const string& msg)
{
	size_t len = msg.length();

	if (len == 0) return msg;

	if (len <= XG_CHARVALUE_BUFLEN)
	{
		char buffer[XG_CHARVALUE_BUFLEN];

		return URLDecode(msg.c_str(), len, buffer);
	}

	SmartBuffer buffer(len);

	return URLDecode(msg.c_str(), len, buffer.str());
}
string stdx::EncodeURL(const string& msg)
{
	size_t len = msg.length();

	if (len == 0) return msg;
	
	if (len <= XG_CHARVALUE_BUFLEN)
	{
		char buffer[XG_CHARVALUE_BUFLEN * 4 + sizeof(char)];

		return URLEncode(msg.c_str(), len, buffer);
	}

	SmartBuffer buffer(len * 4 + sizeof(char));

	return URLEncode(msg.c_str(), len, buffer.str());
}
SmartBuffer stdx::DecodeHex(const char* src, int len)
{
	if ((len /= 2) <= 0) return SmartBuffer();

	SmartBuffer dest(len);

	for (int i = 0, j = 0; i < len; i++, j++)
	{
		if (src[j] <= '9')
		{
			dest[i] = (src[j] - '0') * 16;
		}
		else if (src[j] >= 'a')
		{
			dest[i] = (src[j] - 'a' + 10) * 16;
		}
		else
		{
			dest[i] = (src[j] - 'A' + 10) * 16;
		}

		++j;

		if (src[j] <= '9')
		{
			dest[i] += src[j] - '0';
		}
		else if (src[j] >= 'a')
		{
			dest[i] += src[j] - 'a' + 10;
		}
		else
		{
			dest[i] += src[j] - 'A' + 10;
		}
	}

	return dest;
}
SmartBuffer stdx::EncodeHex(const void* src, int len, bool upper)
{
	SmartBuffer dest(len + len);
	u_char* data = (u_char*)(src);

	if (upper)
	{
		for (int i = 0; i < len; i++) sprintf(dest.str() + (i * 2), "%02X", data[i]);
	}
	else
	{
		for (int i = 0; i < len; i++) sprintf(dest.str() + (i * 2), "%02x", data[i]);
	}

	return dest;
}

#ifdef XG_LINUX

#include <iconv.h>

static int IconvTranslate(const char* fromcode, const char* tocode, const char* src, int srclen, char* dest, int destlen)
{
	if (srclen <= 0) srclen = strlen(src);

	if (fromcode == NULL) fromcode = GetLocaleCharset();
	if (tocode == NULL) tocode = GetLocaleCharset();

	iconv_t handle = iconv_open(tocode, fromcode);
	
	if (handle == (iconv_t)(-1)) return XG_ERROR;

	char* str = dest;
	size_t insz = (size_t)(srclen);
	size_t outsz = (size_t)(destlen);
	size_t val = iconv(handle, (char**)(&src), &insz, &str, &outsz);
	
	if (val == (size_t)(-1))
	{
		iconv_close(handle);
	
		return XG_ERROR;
	}

	iconv_close(handle);

	int len = str - dest;
	
	if (len < destlen) *str = 0;

	return len;
}

string stdx::syscode(const string& str)
{
	return str;
}
string stdx::gbkcode(const string& str)
{
	int len = str.length();
	SmartBuffer res(len + sizeof(char));

	return IconvTranslate("UTF-8", "GBK", str.c_str(), len, res.str(), res.size()) < 0 ? str : res.str();
}
string stdx::utfcode(const string& str)
{
	int len = str.length();
	SmartBuffer res(6 * len + sizeof(char));

	return IconvTranslate("GBK", "UTF-8", str.c_str(), len, res.str(), res.size()) < 0 ? str : res.str();
}

#else

string stdx::syscode(const string& str)
{
	return stdx::gbkcode(str);
}
string stdx::gbkcode(const string& str)
{
	int len;
	string res;

	len = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, NULL, 0);

	if (len <= 0) return str;

	wchar_t* wstr = new wchar_t[len + 1];
	MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, wstr, len);
	len = WideCharToMultiByte(CP_ACP, 0, wstr, -1, NULL, 0, NULL, NULL);

	if (len <= 0)
	{
		delete[] wstr;

		return str;
	}

	char* dest = new char[len + 1];
	WideCharToMultiByte(CP_ACP, 0, wstr, -1, dest, len, NULL, NULL);
	dest[len] = 0;
	res = dest;

	delete[] dest;
	delete[] wstr;

	return std::move(res);
}
string stdx::utfcode(const string& str)
{
	int len;
	string res;

	len = MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, NULL, 0);

	wchar_t* wstr = new wchar_t[len + 1];
	MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, wstr, len);
	len = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, NULL, 0, NULL, NULL);

	char* dest = new char[len + 1];
	WideCharToMultiByte(CP_UTF8, 0, wstr, -1, dest, len, NULL, NULL);
	dest[len] = 0;
	res = dest;

	delete[] wstr;
	delete[] dest;

	return std::move(res);
}

#endif

string stdx::translate(const string& str)
{
	if (str.empty()) return str;

	int res = 0;
	string val = str;
	char buffer[64 * 1024];

#ifdef XG_LINUX
	if (val.find('$') == string::npos && val.find('`') == string::npos) return val;
#else
	if (val.find('$') == string::npos)
	{
		if (val.find('%') == string::npos) return val;
	}
	else
	{
		string tmp;
		vector<string> vec;

		stdx::split(vec, val, "/");
		
		for (string& item : vec)
		{
			tmp += "/";

			size_t pos = item.find('$');
			
			if (pos == string::npos)
			{
				tmp += item;
			}
			else
			{
				if (pos > 0) tmp += item.substr(0, pos);

				tmp += "%" + trim(item.substr(pos + 1), "{()}") + "%";
			}
		}

		val = tmp.substr(1);
	}
#endif
	if ((res = cmdx::RunCommand("echo " + val, buffer, sizeof(buffer))) > 0)
	{
		if (buffer[res - 1] == '\n')
		{
			if (res > 1 && buffer[res - 2] == '\r')
			{
				buffer[res -= 2] = 0;
			}
			else
			{
				buffer[res -= 1] = 0;
			}
		}

		val = buffer;
	}

	val = stdx::replace(val, "\\r", "\r");
	val = stdx::replace(val, "\\n", "\n");
	val = stdx::replace(val, "\\t", "\t");

	return std::move(val);
}
string stdx::replace(const string& str, const string& src, const string& dest)
{
	if (str.empty()) return str;

	string res;
	const char* q = NULL;
	const char* p = str.c_str();

	while (*p)
	{
		if ((q = strstr(p, src.c_str())) == NULL)
		{
			res.append(p);

			break;
		}

		res.append(p, q);
		res.append(dest);
		p = q + src.length();
	}

	return std::move(res);
}
string stdx::GetParentPath(const string& path)
{
	if (path.empty()) return path;

	string res;
	size_t len = path.length();

	if (path.back() == '/' || path.back() == '\\')
	{
		res = path.substr(0, len - 1);
	}
	else
	{
		res = path;
	}

	if ((len = res.rfind('/')) == string::npos && (len = res.rfind('\\')) == string::npos) return stdx::EmptyString();

	return res.substr(0, len);
}
string stdx::GetExtNameFromPath(const string& path)
{
	if (path.empty()) return path;

	const char* end = NULL;
	const char* str = strrchr(path.c_str(), '.');
	
	if (str == NULL) return stdx::EmptyString();
	
	for (end = ++str; *end && isalnum(*end); end++){}
	
	return *end ? stdx::EmptyString() : str;
}
string stdx::GetFileNameFromPath(const string& path)
{
	if (path.empty()) return path;

	const char* name = strrchr(path.c_str(), '/');

	if (name == NULL && (name = strrchr(path.c_str(), '\\')) == NULL) return path;

	return name + 1;
}
string stdx::trim(const string& str, const string& space)
{
	if (str.empty()) return str;

	int len = str.length();

	const char* head = str.c_str();
	const char* tail = str.c_str() + len - 1;

	while (head <= tail && strchr(space.c_str(), *head)) ++head;

	if (*head == 0) return stdx::EmptyString();

	while (tail >= head && strchr(space.c_str(), *tail)) --tail;

	return string(head, tail + 1);
}
string stdx::fill(const string& str, int len, bool left, char ch)
{
	string dest;

	int cnt = len - str.length();

	if (cnt == 0) return str;

	if (cnt < 0) return left ? str.substr(-cnt) : str.substr(0, len);

	if (left)
	{
		for (int i = 0; i < cnt; i++) dest.push_back(ch);

		dest += str;
	}
	else
	{
		dest = str;

		for (int i = 0; i < cnt; i++) dest.push_back(ch);
	}

	return std::move(dest);
}
vector<string> stdx::split(const string& str, const string& space)
{
	vector<string> vec;

	stdx::split(vec, str, space);

	return std::move(vec);
}
int stdx::split(vector<string>& vec, const string& str, const string& space)
{
	int num = 0;

	if (str.length() > 0)
	{
		const char* end = NULL;
		const char* start = str.c_str();

		while (true)
		{
			if ((end = strstr(start, space.c_str())) == NULL)
			{
				vec.push_back(start);
				num++;

				break;
			}

			vec.push_back(string(start, end));
			start = end + space.length();
			num++;
		}
	}

	return num;
}

void Buffer::free()
{
	if (buf)
	{
		delete[] buf;
		buf = NULL;
		maxsz = 0;
		sz = 0;
	}
}
u_char* Buffer::malloc(int sz)
{
	this->free();

	buf = new u_char[sz + 1];
	this->maxsz = sz;
	this->sz = sz;
	buf[sz] = 0;

	return buf;
}
u_char* Buffer::truncate(int sz)
{
	if (this->maxsz >= sz)
	{
		this->sz = sz;
		buf[sz] = 0;

		return buf;
	}

	u_char* tmp = new u_char[sz + 1];

	if (buf)
	{
		memcpy(tmp, buf, this->sz);
		delete[] buf;
	}

	tmp[this->sz] = 0;
	this->maxsz = sz;
	this->sz = sz;
	tmp[sz] = 0;
	buf = tmp;

	return buf;
}

SmartBuffer SmartBuffer::clone()
{
	int sz = size();
	SmartBuffer data;

	if (sz > 0) memcpy(data.malloc(sz), ptr(), sz);

	return data;
}
u_char* SmartBuffer::malloc(int sz)
{
	buffer = newsp<Buffer>(sz);

	return buffer->ptr();
}
string SmartBuffer::toString() const
{
	const char* tmp = str();

	return tmp ? string(tmp, tmp + buffer->size()) : stdx::EmptyString();
}
SmartBuffer& SmartBuffer::append(const string& str)
{
	int sz = size();
	int len = str.length();

	if (len > 0 && truncate(sz + len)) memcpy(ptr() + sz, str.c_str(), len);

	return *this;
}
SmartBuffer& SmartBuffer::append(const SmartBuffer& obj)
{
	int sz = size();

	if (buffer == obj.buffer)
	{
		if (sz > 0 && truncate(sz << 1)) memcpy(ptr() + sz, ptr(), sz);
	}
	else
	{
		if (truncate(sz + obj.size())) memcpy(ptr() + sz, obj.str(), obj.size());
	}

	return *this;
}
SmartBuffer& SmartBuffer::operator = (const string& val)
{
	int sz = val.length();

	if (sz <= 0)
	{
		free();

		return *this;
	}

	if (sz > size()) malloc(sz);

	memcpy(str(), val.c_str(), sz);
	truncate(sz);

	return *this;
}

ContentNode::ContentNode()
{
	valspliter = ";";
	keyspliter = ": ";
	endspliter = "\r\n";
}
void ContentNode::clear()
{
	content.clear();
	vec.clear();
}
bool ContentNode::setValue(const string& key, const string& val, bool append)
{
	auto it = content.find(key);

	if (it == content.end())
	{
		vec.push_back(key);
		content[key] = val;
	}
	else if (append)
	{
		it->second += valspliter + val;
	}
	else
	{
		it->second = val;
	}

	return true;
}
string& ContentNode::operator [] (const string& key)
{
	if (content.find(key) == content.end()) vec.push_back(key);

	return content[key];
}
const string& ContentNode::getValue(const string& key) const
{
	const auto it = content.find(key);

	if (it == content.end()) return stdx::EmptyString();

	return it->second;
}
bool ContentNode::parse(const string& msg, bool inited)
{
	size_t pos = 0;
	vector<string> tmp;

	if (inited) clear();

	CHECK_FALSE_RETURN(stdx::split(tmp, msg, endspliter) > 0);

	for (const string& val : tmp)
	{
		if ((pos = val.find(keyspliter)) == string::npos) continue;

		CHECK_FALSE_RETURN(setValue(val.substr(0, pos), val.substr(pos + keyspliter.length()), true));
	}

	return vec.size() > 0;
}
string ContentNode::toString() const
{
	string str;

	if (vec.empty()) return str;

	for (const string& key : vec)
	{
		str += endspliter + key + keyspliter + content.find(key)->second;
	}

	return str.c_str() + endspliter.length();
}
bool ContentNode::setOrder(const string& key, int idx)
{
	if (idx < 0) idx += vec.size();

	CHECK_FALSE_RETURN (idx >= 0 && idx < vec.size());

	for (size_t i = 0; i < vec.size(); i++)
	{
		if (key == vec[i])
		{
			if (idx == i) return true;

			vec[i] = vec[idx];
			vec[idx] = key;

			return true;
		}
	}

	return false;
}
///////////////////////////////////////////////////////////////////
#endif