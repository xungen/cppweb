#ifndef XG_FILE_CPP
#define XG_FILE_CPP
///////////////////////////////////////////////////////////////////
#ifdef XG_LINUX
#include <dirent.h>
#include <sys/stat.h>
#include <sys/file.h>
#endif

#include "../cmd.h"
#include "../File.h"

long long IFile::tell() const
{
	return XG_ERROR;
}
long long IFile::size() const
{
	return XG_ERROR;
}
long long IFile::seek(long long offset, int mode)
{
	return XG_ERROR;
}

File::~File()
{
	close();
}
void File::close()
{
	if (handle)
	{
		fclose(handle);
		handle = NULL;
	}
}
bool File::open(const string& filename, const string& mode)
{
	close();

	handle = fopen(stdx::syscode(filename).c_str(), mode.c_str());

	return handle ? true : false;
}
bool File::create(const string& filename)
{
	return open(filename, "wb+");
}
bool File::eof() const
{
	return feof(handle) ? true : false;
}
int File::read(void* data, int size)
{
	return fread(data, 1, size, handle);
}
int File::write(const void* data, int size)
{
	return fwrite(data, 1, size, handle);
}
void File::flush() const
{
	fflush(handle);
}
long long File::seek(long long offset, int mode)
{
	return fseek(handle, (long)(offset), mode);
}
long long File::tell() const
{
	return ftell(handle);
}
long long File::size() const
{
	long long sz = 0;
	long long offset = 0;

	offset = tell();

	if (const_cast<File*>(this)->seek(0, SEEK_END) >= 0)
	{
		sz = tell();
	}
	else
	{
		sz = XG_ERROR;
	}

	const_cast<File*>(this)->seek(offset, SEEK_SET);

	return sz;
}


XFile::~XFile()
{
	close();
}
void XFile::close()
{
	if (HandleCanUse(handle))
	{
		Close(handle);
		handle = INVALID_HANDLE_VALUE;
	}
}
bool XFile::unlock()
{
#ifdef XG_LINUX
	return flock(handle, LOCK_UN) == 0;
#else
	OVERLAPPED info = {0};
	
	return UnlockFileEx(handle, 0, MAXDWORD, MAXDWORD, &info) == TRUE;
#endif
}
bool XFile::lock(bool waited)
{
#ifdef XG_LINUX
	if (waited)
	{
		return flock(handle, LOCK_EX) == 0;
	}
	else
	{
		return flock(handle, LOCK_EX | LOCK_NB) == 0;
	}
#else
	OVERLAPPED info = {0};
	
	if (waited)
	{
		return LockFileEx(handle, LOCKFILE_EXCLUSIVE_LOCK, 0, MAXDWORD, MAXDWORD, &info) == TRUE;
	}
	else
	{
		return LockFileEx(handle, LOCKFILE_EXCLUSIVE_LOCK | LOCKFILE_FAIL_IMMEDIATELY, 0, MAXDWORD, MAXDWORD, &info) == TRUE;
	}
						
#endif
}
bool XFile::open(const string& filename, E_OPEN_MODE mode)
{
	close();

	handle = Open(filename.c_str(), mode);
	
	return HandleCanUse(handle);
}
bool XFile::create(const string& filename)
{
	return open(filename, eCREATE);
}
long long XFile::seek(long long offset, int mode)
{
	return Seek(handle, offset, mode);
}
long long XFile::tell() const
{
	return const_cast<XFile*>(this)->seek(0, SEEK_CUR);
}
long long XFile::size() const
{
	long long offset = tell();
	long long sz = const_cast<XFile*>(this)->seek(0, SEEK_END);

	return offset == const_cast<XFile*>(this)->seek(offset) ? sz : XG_ERROR;
}
void XFile::flush() const
{
	Flush(handle);
}
int XFile::read(void* data, int size)
{
	return Read(handle, data, size);
}
int XFile::write(const void* data, int size)
{
	return Write(handle, data, size);
}

MemFile::~MemFile()
{
	close();
}
void MemFile::close()
{
	maxsz = 0;
	offset = 0;
	writed = 0;
	data = NULL;
	buffer.free();
}
bool MemFile::open(SmartBuffer buffer)
{
	CHECK_FALSE_RETURN(buffer.str());

	this->offset = 0;
	this->buffer = buffer;
	this->data = buffer.str();
	this->maxsz = this->writed = buffer.size();
	
	return true;
}
bool MemFile::open(void* data, int len)
{
	CHECK_FALSE_RETURN(data && len > 0);

	this->offset = 0;
	this->data = (char*)(data);
	this->maxsz = this->writed = len;

	return true;
}
bool MemFile::create(int maxsz)
{
	this->offset = 0;
	this->writed = 0;
	this->maxsz = maxsz;
	this->data = (char*)buffer.malloc(maxsz);

	return true;
}
long long MemFile::seek(long long offset, int mode)
{
	if (writed == 0) return 0;

	if (mode == SEEK_CUR)
	{
		offset += this->offset;
	}
	else if (mode == SEEK_END)
	{
		offset = writed - offset;
	}

	if (offset < 0)
	{
		this->offset = 0;
	}
	else if (offset < writed)
	{
		this->offset = offset;
	}
	else
	{
		this->offset = writed;
	}

	return this->offset;
}
long long MemFile::tell() const
{
	return offset;
}
long long MemFile::size() const
{
	return writed;
}
int MemFile::read(void* data, int size)
{
	if (writed <= offset) return 0;

	if (offset + size > writed) size = writed - offset;

	memcpy(data, this->data + offset, size);

	offset += size;

	return size;
}
bool MemFile::truncate(int len)
{
	CHECK_FALSE_RETURN(maxsz >= len);

	if (offset > len) offset = len;

	writed = len;
	maxsz = len;
	
	return true;
}
int MemFile::write(const void* data, int size)
{
	if (offset + size > maxsz) return 0;

	memcpy(this->data + offset, data, size);

	offset += size;

	if (offset > writed) writed = offset;

	return size;
}
bool MemFile::save(const string& filename) const
{
	File file;

	CHECK_FALSE_RETURN(data && file.create(filename));
	CHECK_FALSE_RETURN(file.write(data, writed) > 0);

	file.flush();

	return true;
}
bool MemFile::load(const string& filename)
{
	File file;

	CHECK_FALSE_RETURN(file.open(filename, "rb"));

	long long sz = file.size();

	if (sz <= 0 || sz > XG_MEMFILE_MAXSZ) return false;

	SmartBuffer buffer(sz);

	CHECK_FALSE_RETURN(file.read(buffer.str(), buffer.size()) == sz);

	return open(buffer);
}

int CacheFile::getContent(const string& path, SmartBuffer& content, time_t& utime)
{
	Item item;
	time_t now = time(NULL);

	auto get = [&](){
		int len;
		XFile file;

		len = file.open(path, eREAD) ? file.size() : XG_NOTFOUND;

		if (len <= 0 || len > maxfilesize) return len;

		return file.read(item.content.malloc(len), len);
	};

	if (cachemap.get(path, item))
	{
		if (item.ctime + 3 > now)
		{
			utime = item.utime;
		}
		else
		{
			utime = path::mtime(path);

			if (utime <= 0) return XG_NOTFOUND;

			if (utime >= item.ctime)
			{
				int len = get();

				if (len <= 0 || len > maxfilesize)
				{
					cachemap.remove(path);

					return len;
				}

				cachemap.update(path, [&](Item& data){
					data.content = item.content;
					data.utime = utime;
					data.ctime = now;
				});
			}
		}

		content = item.content;

		return content.size();
	}

	utime = path::mtime(path);

	if (utime <= 0) return XG_SYSERR;

	int len = get();

	if (len <= 0 || len > maxfilesize) return len;

	content = item.content;
	item.utime = utime;
	item.ctime = now;

	cachemap.set(path, item);

	return len;
}

TextFile::~TextFile()
{
	close();
}
void TextFile::close()
{
	if (handle)
	{
		if (handle == stdout)
		{
			fflush(handle);
		}
		else
		{
			fclose(handle);
		}

		handle = NULL;
	}
}
const TextFile& TextFile::printf(const char* fmt, ...) const
{
	int len;
	string str;
	va_list args;

	va_start(args, fmt);
	len = stdx::vformat(str, fmt, args);
	va_end(args);

	append(str);

	return *this;
}

LogThread* LogThread::Instance()
{
	XG_DEFINE_GLOBAL_VARIABLE(LogThread)
}

void LogThread::callback(function<void(const string&)> func)
{
	if (TaskQueue::Instance()->getThreads() > 0)
	{
		stdx::async([=](){
			this->mtx.lock();
			this->func = func;
			this->mtx.unlock();
		});
	}
	else
	{
		this->func = func;
	}
}
bool LogThread::init(YAMLoader* file)
{
	if (file == NULL) file = YAMLoader::Instance();

	int sync = yaml::config<int>("log.sync");
	int level = yaml::config<int>("log.level");
	auto maxsz = yaml::capacity("log.maxsize");
	string path = yaml::config<string>("log.path");

	if (level >= 0) LogThread::Instance()->setLevel(level);

	return init(path, maxsz, sync > 0);
}
bool LogThread::init(const string& path, size_t maxsz, bool sync)
{
	logpath = path.empty() ? Process::GetCurrentDirectory() + "/log" : path;

	maxfilesize = maxsz > 0 ? maxsz : 10 * 1024 * 1024;

	CHECK_FALSE_RETURN(path::mkdir(logpath));

	if (sync) return true;

	buffer.malloc(1024 * 1024);

	return mq.create(buffer.str(), buffer.size()) && start();
}
bool LogThread::printf(const char* fmt, ...)
{
	if (flag < 0) return true;

	int len;
	va_list args;
	char buffer[64 * 1024];

	va_start(args, fmt);
	len = vsnprintf(buffer, sizeof(buffer), fmt, args);
	va_end(args);

	CHECK_FALSE_RETURN(len > 0);

	if (len >= sizeof(buffer))
	{
		len = sizeof(buffer) - 1;
		buffer[len] = 0;
	}

	if (this->buffer.isNull()) return print(buffer, len);

	while (true)
	{
		{
			SpinLocker lk(mtx);

			if (mq.push(buffer, len) >= 0 || maxfilesize <= 0) break;
		}

		Sleep(1);
	}

	return true;
}
bool LogThread::trace(int level, const string& msg)
{
	return trace(level, "%s", msg.c_str());
}
bool LogThread::trace(int level, const char* fmt, ...)
{
	if (flag < 0 || level < this->level) return true;

	int len;
	int num;
	DateTime dt;
	va_list args;
	char buffer[64 * 1024];
	time_t now = (time_t)(GetTime() / 1000);
	thread_local int tid = (int)(GetCurrentThreadId()) % 1000000;
	static const char* taglist[] = {"DBG", "TIP", "INF", "IMP", "ERR"};

	CHECK_FALSE_RETURN(dt.update(now / 1000).canUse());
	CHECK_FALSE_RETURN(level >= 0 && level <= 4);

	va_start(args, fmt);
	len = snprintf(buffer, sizeof(buffer), "[%04d-%02d-%02d %02d:%02d:%02d.%03d][%s][%06d] ", dt.year, dt.month, dt.mday, dt.hour, dt.min, dt.sec, (int)(now % 1000), taglist[level], tid);
	num = vsnprintf(buffer + len, sizeof(buffer) - len - sizeof(char), fmt, args);
	va_end(args);

	CHECK_FALSE_RETURN(num > 0);

	len += num;

	if (len + sizeof(char) >= sizeof(buffer))
	{
		len = sizeof(buffer) - sizeof(char) - 1;
	}

	buffer[len++] = '\n';
	buffer[len] = 0;

	if (this->buffer.isNull()) return print(buffer, len);

	while (true)
	{
		{
			SpinLocker lk(mtx);

			if (mq.push(buffer, len) >= 0 || maxfilesize <= 0) break;
		}

		Sleep(1);
	}

	return true;
}
void LogThread::run()
{
	while (this->buffer.isNull()) return;

	u_int32 sz;
	u_char* msg;
	static char buffer[64 * 1024];

	while (true)
	{
		mtx.lock();

		msg = mq.pop(sz);

		if (msg == NULL || sz >= sizeof(buffer))
		{
			mtx.unlock();
			Sleep(1);

			continue;
		}

		memcpy(buffer, msg, sz);
		buffer[sz] = 0;

		if (func)
		{
			try
			{
				func(string(buffer, buffer + sz));
			}
			catch(const Exception& e)
			{
				fprintf(stderr, "catch exception[%d][%s]\n", e.getErrorCode(), e.getErrorString());
			}
			catch(const exception& e)
			{
				fprintf(stderr, "catch exception[%s]\n", e.what());
			}
			catch(...)
			{
				fprintf(stderr, "catch unknown exception\n");
			}
		}

		mtx.unlock();

		print(buffer, sz);
	}
}
bool LogThread::start()
{
	return Thread::start();
}
string LogThread::getLogFilePath()
{
	int cnt = 100;
	int idx = index;

	auto getFileName = [&](int idx){
		string filename = logpath;

		if (filename.back() == '/')
		{
			filename += Process::GetAppname();
		}
		else
		{
			filename += string("/") + Process::GetAppname();
		}

		stdx::append(filename, ".%02d.log", idx);

		return filename;
	};

	for (int i = 0; i < cnt; i++)
	{
		if (++idx >= cnt) idx = 0;

		filename = getFileName(idx);

		if (path::size(filename) < maxfilesize)
		{
			index = idx;

			return filename;
		}
	}

	if (++index >= cnt) index = 0;

	filename = getFileName(index);

	path::remove(filename);

	return filename;
}
bool LogThread::checkLogFile()
{
	auto reopen = [&](){
		string filepath = getLogFilePath();

		if (filename.empty())
		{
			file.close();
		}
		else
		{
			if (file.open(filepath, path::type(filepath) == eNONE ? eCREATE : eWRITE) && file.seek(0, SEEK_END) < 0) file.close();
		}
	};

	if (HandleCanUse(file.getHandle()))
	{
		long long sz = file.tell();

		if (sz < 0 || sz > maxfilesize) reopen();
	}
	else
	{
		reopen();
	}

	return HandleCanUse(file.getHandle());
}
bool LogThread::write(const char* msg, int len)
{
	CHECK_FALSE_RETURN(msg && len > 0);

	if (buffer.str()) return checkLogFile() && file.write(msg, len) > 0;

	static Mutex mtx;
	Locker lock(mtx);

	return checkLogFile() && file.write(msg, len) > 0;
}
bool LogThread::print(const char* msg, int len)
{
	if (logpath.empty() || flag == 1)
	{
		fwrite(msg, 1, len, stdout);
		fflush(stdout);

		return true;
	}

	if (flag == 0) return write(msg, len);

	fwrite(msg, 1, len, stdout);
	fflush(stdout);

	return write(msg, len);
}


Context::Context()
{
}
Context::~Context()
{
}
void Context::init(const string& traceid, const string& path)
{
	this->traceid = traceid;
	this->path = path;
	
	if (traceid.length() > 0 && path.length() > 0)
	{
		stdx::format(loghead, "[%s][%s] ", traceid.c_str(), path.c_str());
	}
	else if (traceid.length() > 0)
	{
		stdx::format(loghead, "[%s] ", traceid.c_str());
	}
	else if (path.length() > 0)
	{
		stdx::format(loghead, "[%s] ", path.c_str());
	}
}

string Context::getAttr(const string& key) const
{
	string val;

	attrmap.get(key, val);

	return val;
}
void Context::setAttr(const string& key, const string& val)
{
	attrmap.set(key, val);
}

void Context::trace(int level, const string& msg) const
{
	trace(level, "%s", msg.c_str());
}
void Context::trace(int level, const char* fmt, ...) const
{
	int len;
	string msg;
	va_list args;
	static LogThread* log = LogThread::Instance();

	va_start(args, fmt);
	len = stdx::vformat(msg, fmt, args);
	va_end(args);

	if (loghead.empty())
	{
		log->trace(level, msg);
	}
	else
	{
		string str;

		str.reserve(loghead.length() + msg.length());
		str += loghead;
		str += msg;

		log->trace(level, str);
	}
}

sp<Context> Context::Create(const string& traceid, const string& path)
{
	sp<Context> ctx = newsp<Context>();

	ctx->init(traceid, path);

	return ctx;
}


int stdx::GetFileContent(string& content, const string& path)
{
	int sz;
	SmartBuffer buffer;

	if ((sz = GetFileContent(buffer, path)) > 0) content = string(buffer.str(), sz);

	return sz;
}
int stdx::GetFileContent(SmartBuffer& content, const string& path)
{
	XFile file;
	
	if (!file.open(path, eREAD)) return XG_SYSERR;

	int sz = file.size();

	if (sz <= 0) return sz;

	sz = min(256 * 1024 * 1024, sz);

	if ((sz = file.read(content.malloc(sz), sz)) < 0) return sz;

	if (sz < content.size()) content.truncate(sz);

	return sz;
}

int stdx::FindFile(vector<string>& vec, const string& path, const string& filter)
{
	string cmd;
	int res = 0;
	vector<string> tmp;
	SmartBuffer buffer(4 * 1024 * 1024);

#ifdef XG_LINUX
	stdx::format(cmd, "find %s -name %s", stdx::quote(path).c_str(), stdx::quote(filter).c_str());
#else
	stdx::format(cmd, "for /r %s %%i in (%s) do @echo %%i", stdx::quote(path).c_str(), filter.c_str());
#endif
	if (cmdx::RunCommand(cmd, buffer.str(), buffer.size()) < 0) return XG_SYSERR;

	stdx::split(tmp, buffer.str(), "\n");

	for (string& str : tmp)
	{
		str = stdx::trim(str);

		if (str.length() > 0)
		{
			vec.push_back(stdx::replace(str, "\\", "/"));
			res++;
		}
	}

	return res;
}
int stdx::GetFolderContent(vector<string>& vec, const string& path, int flag, bool containdots)
{
	return GetFolderContent([&](string path, int type, int size, time_t mtime){
		vec.push_back(path);
	}, path, flag, containdots);
}

#ifdef XG_LINUX

int stdx::GetFolderContent(function<void(string, int, long, time_t)> func, const string& path, int flag, bool containdots)
{
	string str;
	int res = 0;
	DIR* dir = NULL;
	char buf[MAX_PATH];
	int len = path.length();

	if ((dir = opendir(path.c_str())) == NULL) return XG_ERROR;

	strcpy(buf, path.c_str());

	if (buf[len - 1] != '/' && buf[len - 1] != '\\')
	{
		buf[len++] = '/';
		buf[len] = 0;
	}

	for (struct dirent* ent = readdir(dir); ent; ent = readdir(dir))
	{
		struct stat statbuf;
		const char* name = ent->d_name;

		if (strcmp(name, ".") == 0 || strcmp(name, "..") == 0) continue;
		
		str = buf;
		str += name;

		if (lstat(str.c_str(), &statbuf) < 0)
		{
			closedir(dir);

			return XG_ERROR;
		}

		if (flag == eNONE)
		{
			if (containdots || *name != '.')
			{
				func(name, S_ISDIR(statbuf.st_mode) ? ePATH : eFILE, statbuf.st_size, statbuf.st_mtime);

				res++;
			}
		}
		else if (flag == ePATH)
		{
			if (S_ISDIR(statbuf.st_mode))
			{
				if (containdots || *name != '.')
				{
					func(name, ePATH, statbuf.st_size, statbuf.st_mtime);

					res++;
				}
			}
		}
		else
		{
			if (S_ISDIR(statbuf.st_mode)) continue;

			if (containdots || *name != '.')
			{
				func(name, eFILE, statbuf.st_size, statbuf.st_mtime);

				res++;
			}
		}
	}

	closedir(dir);

	return res;
}

#else

int stdx::GetFolderContent(function<void(string, int, long, time_t)> func, const string& path, int flag, bool containdots)
{
	int res = 0;
	HANDLE handle;
	string str = path;
	char buf[MAX_PATH];
	int len = str.length();
	WIN32_FIND_DATAA findData;

	auto mtime = [&](){
		struct tm t;
		FILETIME ft;
		SYSTEMTIME st;
		SYSTEMTIME utc;

		if (FileTimeToSystemTime(&findData.ftLastWriteTime, &utc))
		{
			SystemTimeToTzSpecificLocalTime(NULL, &utc, &st);

			t.tm_year = st.wYear - 1900;
			t.tm_mon = st.wMonth - 1;
			t.tm_mday = st.wDay;

			t.tm_sec = st.wSecond;
			t.tm_min = st.wMinute;
			t.tm_hour = st.wHour;

			return mktime(&t);
		}

		return (time_t)(0);
	};

	strcpy(buf, path.c_str());

	if (buf[len - 1] == '/' || buf[len - 1] == '\\')
	{
		str += "*";
	}
	else
	{
		str += "/*";
		strcat(buf, "/");
	}

	handle = FindFirstFileA(stdx::syscode(str).c_str(), &findData);

	if (INVALID_HANDLE_VALUE == handle) return XG_ERROR;

	while (FindNextFileA(handle, &findData))
	{
		const char* name = findData.cFileName;

		if (strcmp(name, ".") == 0 || strcmp(name, "..") == 0) continue;

		if (flag == eNONE)
		{
			if (containdots || *name != '.')
			{
				if (FILE_ATTRIBUTE_DIRECTORY & findData.dwFileAttributes)
				{
					func(stdx::utfcode(name), ePATH, findData.nFileSizeLow, mtime());
				}
				else
				{
					func(stdx::utfcode(name), eFILE, findData.nFileSizeLow, mtime());
				}

				res++;
			}
		}
		else if (flag == ePATH)
		{
			if (FILE_ATTRIBUTE_DIRECTORY & findData.dwFileAttributes)
			{
				if (containdots || *name != '.')
				{
					func(stdx::utfcode(name), ePATH, findData.nFileSizeLow, mtime());

					res++;
				}
			}
		}
		else
		{
			if ((FILE_ATTRIBUTE_DIRECTORY & findData.dwFileAttributes) == 0)
			{
				if (containdots || *name != '.')
				{
					func(stdx::utfcode(name), eFILE, findData.nFileSizeLow, mtime());

					res++;
				}
			}
		}
	}

	FindClose(handle);

	return res;
}

#endif

///////////////////////////////////////////////////////////////////
#endif