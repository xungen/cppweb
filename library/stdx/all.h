#ifndef XG_STDALL_H
#define XG_STDALL_H
////////////////////////////////////////////////////////
#include "cmd.h"

#include "Reflect.h"
#include "MemQueue.h"
#include "DateTime.h"

#include "File.h"
#include "Socket.h"
#include "Sharemem.h"
#include "Semaphore.h"
////////////////////////////////////////////////////////
#endif
