#ifndef XG_ZLIB_H
#define XG_ZLIB_H
////////////////////////////////////////////////////////////////////
#include "src/zlib.h"

#ifdef __cplusplus
extern "C" {
#endif

  int GZIPCompress(const void* data, unsigned int ndata, void* zdata, unsigned int* nzdata, int level);

  int GZIPDecompress(const void* zdata, unsigned int nzdata, void* data, unsigned int* ndata);

#ifdef __cplusplus
}
#endif

////////////////////////////////////////////////////////////////////
#endif