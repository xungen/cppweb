#ifndef XG_WINX_GUI_H
#define XG_WINX_GUI_H
//////////////////////////////////////////////////////////////////
#include <shlobj.h>
#include <commdlg.h>
#include <stdx/all.h>

#ifndef XG_FILTER
#define XG_FILTER		L"ALL(*.*)\0*.*\0"
#endif

namespace winx
{
	string WStringToString(const wstring& str);
	wstring StringToWString(const string& str);

	HWND GetTrayWnd();
	bool IsKeyDown(int key);
	bool GetScreenSize(SIZE& sz);
	bool GetScrollSize(SIZE& sz);
	void SetRgn(HWND hwnd, HRGN hrgn);
	bool GetDesktopPath(LPWSTR path);
	bool GetProgramsPath(LPWSTR path);
	bool GetQuickLaunchPath(LPWSTR path);
	int GetTitleHeight(HWND hwnd = NULL);
	bool GetScreenClientBounds(RECT& rect);
	void SetWindowIcon(HWND hwnd, HICON hicon);
	void SetAlpha(HWND hwnd, float alpha = 0.8f);
	bool CenterWindow(HWND hwnd, int cx, int cy, HWND parent = NULL);
	bool ShowMessage(LPCWSTR text, LPCWSTR title, bool confirm = false, bool topmost = true);
	bool CreateShortcut(LPCWSTR exepath, LPCWSTR linkname = NULL, LPCWSTR linkpath = NULL, WORD hotkey = 0, LPCWSTR description = NULL, int showcmd = SW_SHOWNORMAL);
};


class FileChooser
{
private:
	OPENFILENAMEW ofn;

public:
	int getFilterIndex();
	void setFlag(DWORD flag = 0);
	void setFile(LPCWSTR path);
	void setTitle(LPCWSTR title);
	void setParent(HWND hwnd = NULL);
	void setFilterIndex(int index = 0);
	LPWSTR getFile(bool saved = false);
	void setFilter(LPCWSTR filter = XG_FILTER);
	void init(HWND hwnd = NULL, bool multisel = false, LPCWSTR filter = XG_FILTER);
	FileChooser(HWND hwnd = NULL, bool multisel = false, LPCWSTR filter = XG_FILTER);
};


class FolderChooser
{
private:
	BROWSEINFOW browseinfo;

public:
	LPWSTR getFolder();
	void init(HWND hwnd = NULL);
	void setTitle(LPCWSTR title);
	FolderChooser(HWND hwnd = NULL);
	void setParent(HWND hwnd = NULL);
	void setFlag(DWORD flag = BIF_RETURNONLYFSDIRS);
};

class FileFinder
{
private:
	HANDLE handle;
	WIN32_FIND_DATAW findata;

public:
	void close();
	FileFinder();
	~FileFinder();
	
	LPCWSTR getNextFile();
	LPCWSTR getFirstFile(LPCWSTR filter);

	bool isDots();
	bool isFolder();
	bool isHidden();
	bool isReadOnly();
	DWORD getAttributes();
};
//////////////////////////////////////////////////////////////////
#endif
