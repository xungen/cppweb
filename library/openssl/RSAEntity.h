#ifndef XG_OPENSSL_RSAENTITY_H
#define XG_OPENSSL_RSAENTITY_H
/////////////////////////////////////////////////////////////////
#include "SSLSocket.h"

string SHAEncode(const string& msg);

class RSAEntity : public Object
{
	RSA* pubkey = NULL;
	RSA* prikey = NULL;
	bool crypted = true;
	int padding = RSA_PKCS1_PADDING;

public:
	CONSTRUCTOR_FORBID_COPY(RSAEntity)

	~RSAEntity();
	void close();
	void setPadding(int padding);
	void setCrypted(bool crypted);
	string sign(const string& msg);
	string encode(const string& msg);
	string decode(const string& msg);
	bool verify(const string& msg, const string& sign);
	bool init(const char* pubkeypath, const char* prikeypath);
};
/////////////////////////////////////////////////////////////////
#endif