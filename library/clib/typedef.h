#ifndef XG_HEAD_TYPEDEF
#define XG_HEAD_TYPEDEF
///////////////////////////////////////////////////////

#define XG_OK			 1
#define XG_FAIL			 0
#define XG_ERROR		-1
#define XG_IOERR		-2
#define XG_SYSERR		-3
#define XG_NETERR		-4
#define XG_TIMEOUT		-5
#define XG_DATAERR		-6
#define XG_SYSBUSY		-7
#define XG_PARAMERR		-8
#define XG_NOTFOUND		-9
#define XG_NETCLOSE		-10
#define XG_NETDELAY		-11
#define XG_SENDFAIL		-12
#define XG_RECVFAIL		-13
#define XG_AUTHFAIL		-14
#define XG_DAYLIMIT		-15
#define XG_DUPLICATE	-16
#define XG_UNINSTALL	-17
#define XG_NOTCHANGED	-18
#define XG_DETACHCONN	-999999

#ifndef XG_MEMFILE_MAXSZ
#define XG_MEMFILE_MAXSZ	64 * 1024 * 1024
#endif

#ifdef _MSC_VER

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#endif

#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <assert.h>
#include <stdarg.h>

#define BIT_AT(val, idx)		((val)&(1<<(idx)))
#define BIT_SET(val, idx, bit)	((bit)?((val)|=(1<<(idx))):((val)&=~(1<<(idx))))

#define SWAP_INTEGER(a, b)				\
{										\
	(a) ^= (b);							\
	(b) ^= (a);							\
	(a) ^= (b);							\
}

#define POINTER_ASSIGN(ptr, val)		\
if (ptr)								\
{										\
	*(ptr) = (val);						\
}

#define ErrorExit(code)																	\
{																						\
	if (code) printf("EXIT[%s:%d][%s][%d]\n", __FILE__, __LINE__, __FUNCTION__, code);	\
	SystemExit(code);																	\
}

typedef enum
{
	eNONE = 0,
	ePATH = 1,
	eFILE = 2
} E_PATH_TYPE;

typedef enum
{
	eGET = 'G',
	ePUT = 'U',
	ePOST = 'P',
	eHEAD = 'H',
	eDELETE = 'D',
	eOPTION = 'O',
	eUNKNOWN = 0
} E_HTTP_METHOD;

#ifdef XG_LINUX					/******* linux ******/
	#include <errno.h>
	#include <fcntl.h>
	#include <signal.h>
	#include <unistd.h>
	#include <pthread.h>
	#include <sys/types.h>

	#ifndef MAX_PATH
	#define MAX_PATH	256
	#endif

	#ifndef XG_IPC_FLAG
	#define	XG_IPC_FLAG	0666
	#endif

	#ifndef XG_ENV_PATH_SPLITER
	#define XG_ENV_PATH_SPLITER	":"
	#endif
	
	#define TRUE 		1
	#define FALSE 		0
	#define Sleep(ms)	usleep((ms)*1000)

	#define START_DLL_MAIN()
	#define INVALID_SOCKET	(SOCKET)(-1)
	#define INVALID_THREAD	(THREAD_T)(-1)
	#define EXTERN_DLL_FUNC  extern "C"
	#define INVALID_HANDLE_VALUE (HANDLE)(-1)

	typedef int SOCKET;
	typedef pthread_t THREAD_T;
	typedef pthread_mutex_t MutexHandle;

	#define UnlockMutex(__CS__)		(pthread_mutex_unlock(&__CS__) == 0)
	#define InitMutex(__CS__)		(pthread_mutex_init(&__CS__, NULL) == 0)
	#define FreeMutex(__CS__)		pthread_mutex_destroy(&__CS__)
	#define LockMutex(__CS__)		(pthread_mutex_lock(&__CS__) == 0)

	typedef enum
	{
		eRED = 31,
		eBLUE = 34,
		eGREEN = 32,
		eWHITE = 37,
		eYELLOW = 33
	} E_CONSOLE_COLOR;
#else							/****** windows *****/
	#ifndef UNICODE
	#define UNICODE
	#endif

	#ifndef _UNICODE
	#define _UNICODE
	#endif

	#ifndef XG_ENV_PATH_SPLITER
	#define XG_ENV_PATH_SPLITER	";"
	#endif

	#include <windows.h>
	#include <conio.h>
	#include <psapi.h>

	#ifdef _MSC_VER
	#define strcasecmp stricmp
	#pragma warning(disable:4996)
	#pragma comment (lib,"PSAPI.lib")
	#pragma comment(lib, "WS2_32.lib")
	#endif
	
	#define getch				_getch
	#define snprintf			_snprintf
	#define sleep(s)			Sleep((s)*1000)
	#define INVALID_THREAD		(THREAD_T)(-1)
	#define localtime_r(a,b)	localtime_s(b,a)
	#define EXTERN_DLL_FUNC	extern "C" __declspec(dllexport)

	#define START_DLL_MAIN()				\
	BOOL APIENTRY DllMain(HMODULE handle,	\
			DWORD  ul_reason_for_call,		\
			LPVOID lpReservedx)				\
	{										\
		return TRUE;						\
	}

	typedef int socklen_t;
	typedef HANDLE THREAD_T;
	typedef HANDLE MutexHandle;

	#define UnlockMutex(__CS__)		(SetEvent(__CS__) == TRUE)
	#define InitMutex(__CS__)		(__CS__ = CreateEvent(NULL, FALSE, TRUE, NULL))
	#define FreeMutex(__CS__)		CloseHandle(__CS__)
	#define LockMutex(__CS__)		(WaitForSingleObject(__CS__, INFINITE) != WAIT_FAILED)

	typedef enum
	{
		eRED = FOREGROUND_RED,
		eBLUE = FOREGROUND_BLUE,
		eGREEN = FOREGROUND_GREEN,
		eYELLOW = FOREGROUND_RED | FOREGROUND_GREEN,
		eWHITE = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN
	} E_CONSOLE_COLOR;
#endif

#ifndef XG_AIX
typedef char 				int8;
typedef long long			int64;
typedef unsigned long long	u_int64;
#endif

typedef int					int32;
typedef short				int16;

typedef unsigned int		u_int;
typedef unsigned char		u_int8;
typedef unsigned char		u_char;
typedef unsigned long		u_long;
typedef unsigned short		u_short;

typedef unsigned int		u_int32;
typedef unsigned short		u_int16;

#ifndef INT8
#define INT8	int8
#endif

#ifndef INT16
#define INT16	int16
#endif

#ifndef INT32
#define INT32	int32
#endif

#ifndef INT64
#define INT64	int64
#endif

#ifndef UINT8
#define UINT8	u_int8
#endif

#ifndef UINT16
#define UINT16	u_int16
#endif

#ifndef UINT32
#define UINT32	u_int32
#endif

#ifndef UINT64
#define UINT64	u_int64
#endif

#ifndef INT_MAX
#define INT_MAX	2147483647
#endif

#ifndef INT_MIN
#define INT_MIN (-INT_MAX - 1)
#endif

#ifndef ARR_LEN
#define ARR_LEN(arr)	(sizeof(arr)/sizeof(arr[0]))
#endif

#ifndef MAKEDWORD
#define MAKEDWORD(low, hight) 	(DWORD)(((DWORD)(low)&0x0000FFFF)|((DWORD)(hight<<16)&0xFFFF0000))
#endif

#ifdef XG_LINUX

typedef int 			BOOL;
typedef long			LONG;
typedef u_int			UINT;
typedef u_int8			BYTE;
typedef u_int16			WORD;
typedef u_int32			DWORD;
typedef wchar_t			WCHAR;

typedef char*			LPSTR;
typedef WCHAR*			LPWSTR;
typedef const char*		LPCSTR;
typedef const WCHAR* 	LPCWSTR;

typedef int				HANDLE;
typedef u_int32			WPARAM;
typedef u_int32			LPARAM;

#define LOWORD(val)		((WORD)((val)&0xFFFF))
#define HIWORD(val)		((WORD)(((val)>>16)&0xFFFF))

typedef struct
{
	LONG cx;
	LONG cy;
} SIZE, *PSIZE, *LPSIZE;

typedef struct
{
	LONG	left;
	LONG	top;
	LONG	right;
	LONG	bottom;
} RECT, *PRECT, *LPRECT;

typedef struct
{
	LONG  x;
	LONG  y;
} POINT, *PPOINT, *LPPOINT;

#endif

#ifdef __cplusplus

#define CHECK_FALSE_RETURN(FUNC) if (FUNC){} else return false

#else

#ifndef min
#define min(a,b) (a)>(b)?(b):(a)
#endif

#ifndef max
#define max(a,b) (a)>(b)?(a):(a)
#endif

#ifndef inline
#define inline
#endif

#define CHECK_FALSE_RETURN(FUNC) if (FUNC){} else return FALSE

#endif

///////////////////////////////////////////////////////
#endif