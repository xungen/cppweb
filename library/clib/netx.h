#ifndef XG_HEADER_NETX
#define XG_HEADER_NETX
//////////////////////////////////////////////////////////
#include "system.h"

#ifndef HOST_IP
#define HOST_IP						"0.0.0.0"
#endif

#ifndef LOCAL_IP
#define LOCAL_IP					"127.0.0.1"
#endif

#ifndef SOCKECT_FRAMESIZE
#define SOCKECT_FRAMESIZE			8 * 1024
#endif

#ifndef SOCKECT_RECVTIMEOUT
#define SOCKECT_RECVTIMEOUT			10
#endif

#ifndef SOCKECT_SENDTIMEOUT
#define SOCKECT_SENDTIMEOUT			20
#endif

#ifndef SOCKET_CONNECT_TIMEOUT
#define SOCKET_CONNECT_TIMEOUT		5000
#endif

#ifndef SOCKET_TIMEOUT_LIMITSIZE
#define SOCKET_TIMEOUT_LIMITSIZE	10
#endif

#ifndef SOCKET_TIMEOUT_REDOTIMES
#define SOCKET_TIMEOUT_REDOTIMES	100
#endif

#ifdef __cplusplus
extern "C" {
#endif
	void SocketSetup();

	BOOL IsSocketTimeout();

	void SocketClose(SOCKET sock);

	BOOL IsSocketClosed(SOCKET sock);

	BOOL SocketSetSendTimeout(SOCKET sock, int ms);

	BOOL SocketSetRecvTimeout(SOCKET sock, int ms);

	SOCKET SocketConnect(const char* ip, int port);

	BOOL GetSocketAddress(SOCKET sock, char* address);

	SOCKET ServerSocketAccept(SOCKET svr, char* address);

	const char* GetHostAddress(const char* host, char* ip);

	SOCKET CreateServerSocket(const char* ip, int port, int backlog);

	SOCKET SocketConnectTimeout(const char* ip, int port, int timeout);

	int SocketPeek(SOCKET sock, void* data, int size);

	int SocketRead(SOCKET sock, void* data, int size);

	int SocketWrite(SOCKET sock, const void* data, int size);

	int SocketReadEx(SOCKET sock, void* data, int size, BOOL completed);

	int SocketWriteEx(SOCKET sock, const void* data, int size, BOOL completed);

	typedef struct
	{
		char flag[8];
		char addr[32];
	} stConnectData;

	BOOL ServerSocketAttach(SOCKET sock, const char* address);

	void ServerSocketSetLockFunction(void(*lock)(), void(*unlock)());

	void ServerSocketSetProcessFunction(int(*func)(SOCKET, stConnectData*));

	void ServerSocketLoop(const char* host, int port, int backlog, int timeout);

	void ServerSocketSetConnectClosedFunction(int(*func)(SOCKET, stConnectData*));
	
	void ServerSocketSetConnectFunction(int(*func)(SOCKET, stConnectData*, const char*, int));
#ifdef __cplusplus
}
#endif

//////////////////////////////////////////////////////////
#endif