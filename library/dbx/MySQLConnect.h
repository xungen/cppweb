#ifndef XG_MYSQLCONNECT_H
#define XG_MYSQLCONNECT_H
//////////////////////////////////////////////////////////////
#include <mysql.h>
#include <mysqld_error.h>

#include "DBConnect.h"

class MySQLRowData : public RowData
{
	friend class MySQLQueryResult;

	bool m_bNull = true;
	MYSQL_ROW m_pSQLRow = NULL;
	MYSQL_RES* m_pSQLRes = NULL;

public:
	CONSTRUCTOR_FORBID_COPY(MySQLRowData)

	bool isNull();
	string getString(int index);
	int getDataLength(int index);
	int getData(int index, char* data, int len);
	MySQLRowData(MYSQL_ROW row, MYSQL_RES* res);
};

class MySQLQueryResult : public QueryResult
{
	friend class MySQLConnect;

	DBConnect* m_pConn = NULL;
	MYSQL_RES* m_pSQLRes = NULL;

public:
	CONSTRUCTOR_FORBID_COPY(MySQLQueryResult)

	~MySQLQueryResult();
	MySQLQueryResult(MYSQL_RES* res, DBConnect* conn);

	int rows();
	int cols();
	void close();
	sp<RowData> next();
	bool seek(int ofs);
	int getErrorCode();
	string getErrorString();
	string getColumnName(int index);
	bool getColumnData(ColumnData& data, int index);
};

class MySQLConnect : public DBConnect
{
	friend class MySQLQueryResult;

	MYSQL* m_pConn = NULL;
	
public:
	CONSTRUCTOR_FORBID_COPY(MySQLConnect)

	void close();
	bool commit();
	bool rollback();
	~MySQLConnect();
	const char* getTableSQL();
	const char* getSystemName();
	bool begin(bool commited = true);
	bool setCharset(const string& charset);
	int getPrimaryKeys(vector<string>& vec, const string& tabname);
	bool connect(const string& host, int port, const string& name, const string& user, const string& password);

	int getErrorCode();
	string getErrorString();
	int execute(const string& sqlcmd);
	sp<QueryResult> query(const string& sqlcmd);
	sp<QueryResult> quickQuery(const string& sqlcmd);
	int execute(const string& sqlcmd, const vector<DBData*>& vec);
	string translate(const string& sqlcmd, const vector<DBData*>& vec);
	sp<QueryResult> query(const string& sqlcmd, const vector<DBData*>& vec);

	static bool Setup();
};
//////////////////////////////////////////////////////////////
#endif