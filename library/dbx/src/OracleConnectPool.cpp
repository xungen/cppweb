#ifndef XG_DB_ORACLECONNECTPOOL_CPP
#define XG_DB_ORACLECONNECTPOOL_CPP
//////////////////////////////////////////////////////////////
#include "../DBConnectPool.h"
#include "../OracleConnect.h"

class OracleConnectPool : public DBConnectPool
{
public:
	OracleConnectPool()
	{
		OracleConnect::Setup();
	}
	
protected:
	DBConnect* createConnect()
	{
		OracleConnect* conn = new OracleConnect();

		if (conn->init(cfg)) return conn;

		delete conn;

		return NULL;
	}
};

DEFINE_DBCONNECTPOOL_EXPORT_FUNC(OracleConnectPool)

//////////////////////////////////////////////////////////////
#endif