#ifndef XG_SQLITECONNECT_H
#define XG_SQLITECONNECT_H
//////////////////////////////////////////////////////////////
#include "DBConnect.h"
#include "../stdx/cmd.h"
#include "../sqlite/sqlite3.h"

class SQLiteRowData : public RowData
{
	friend class SQLiteQueryResult;

	const char* data = NULL;
	sqlite3_stmt* stmt = NULL;

	const char* getData(int index);

public:
	CONSTRUCTOR_FORBID_COPY(SQLiteRowData)

	~SQLiteRowData();
	SQLiteRowData(sqlite3_stmt* stmt);

	void close();
	bool isNull();
	int getDataLength(int index);
	string getString(int index);
	int getData(int index, char* data, int len);
};

class SQLiteQueryResult : public QueryResult
{
	friend class SQLiteConnect;

	int res = 0;
	sqlite3* conn = NULL;
	sqlite3_stmt* stmt = NULL;

public:
	CONSTRUCTOR_FORBID_COPY(SQLiteQueryResult)

	~SQLiteQueryResult();
	SQLiteQueryResult(sqlite3* conn, sqlite3_stmt* stmt);

	int rows();
	int cols();
	void close();
	sp<RowData> next();
	bool seek(int ofs);
	int getErrorCode();
	string getErrorString();
	string getColumnName(int index);
	bool getColumnData(ColumnData& data, int index);
};

class SQLiteConnect : public DBConnect
{
	int res = 0;
	string errmsg;
	string filepath;
	sqlite3* conn = NULL;
	vector<SmartBuffer> bindvec;

	int bind(void* stmt, const vector<DBData*>& vec);

public:
	CONSTRUCTOR_FORBID_COPY(SQLiteConnect)
	
	void close();
	bool commit();
	bool rollback();
	~SQLiteConnect();
	const char* getTableSQL();
	const char* getSystemName();
	bool connect(const string& filename);
	int getPrimaryKeys(vector<string>& vec, const string& tabname);
	bool connect(const string& host, int port, const string& name, const string& user, const string& password);

	int getErrorCode();
	string getErrorString();
	int execute(const string& sqlcmd);
	sp<QueryResult> query(const string& sqlcmd);
	int execute(const string& sqlcmd, const vector<DBData*>& vec);
	sp<QueryResult> query(const string& sqlcmd, const vector<DBData*>& vec);

	static bool Setup();
	static const char* GetShellExePath();
	static bool Check(const string& filename);
	static bool Modify(const string& filename);
	static bool Backup(const string& filename);
	static bool Restore(const string& filename);
};
//////////////////////////////////////////////////////////////
#endif