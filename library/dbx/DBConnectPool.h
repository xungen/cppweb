#ifndef XG_DB_CONNECTPOOL_H
#define XG_DB_CONNECTPOOL_H
////////////////////////////////////////////////////////
#include "DBConnect.h"
#include "../stdx/File.h"

class DBConnectPool : public ResPool<DBConnect>
{
protected:
	DBConnectConfig cfg;

	virtual DBConnect* createConnect() = 0;

public:
	DBConnectPool();
	sp<DBConnect> get();
	virtual bool init(const string& path);
	virtual bool init(const YAMLoader* file);

	static DBConnectPool* Create(const YAMLoader* file);
	static string GetDefaultLibraryPath(const string& type);
};

typedef DBConnectPool* (*CREATE_DBCONNECTPOOL_FUNC)();
typedef void (*DESTROY_DBCONNECTPOOL_FUNC)(DBConnectPool*);


#define DEFINE_DBCONNECTPOOL_EXPORT_FUNC(__CLASS__)							\
EXTERN_DLL_FUNC DBConnectPool* CreateDBConnectPool()						\
{																			\
	return new __CLASS__();													\
}																			\
EXTERN_DLL_FUNC void DestroyDBConnectPool(DBConnectPool* pool)				\
{																			\
	delete pool;															\
}

////////////////////////////////////////////////////////
#endif
