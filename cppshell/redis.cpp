#include <stdx/all.h>
#include <dbx/RedisConnect.h>

/*服务端口*/
int PORT = 6379;

/*连接超时*/
int TIMEOUT = 3000;

/*服务地址*/
const char* HOST = NULL;

/*验证密码*/
const char* PASSWORD = NULL;

/*********************************************************************************/

const char* GetCmdParam(int idx)
{
	return Process::GetCmdParam(idx);
}

const char* GetCmdParam(const char* param)
{
	string name = param;
	const char* key = Process::GetCmdParam(param);

	if (key == NULL) key = Process::GetCmdParam(stdx::toupper(name).c_str());
	if (key == NULL) key = Process::GetCmdParam(stdx::tolower(name).c_str());

	return key;
}

int main(int argc, char** argv)
{
	Process::Instance(argc, argv);

	static string ehost = Process::GetEnv("REDIS_HOST");
	static string epass = Process::GetEnv("REDIS_PASSWORD");

	string val;
	RedisConnect redis;
	const char* ptr = NULL;
	const char* cmd = GetCmdParam(1);
	const char* key = GetCmdParam(2);
	const char* field = GetCmdParam(3);

	if (HOST == NULL) HOST = ehost.c_str();
	if (PASSWORD == NULL) PASSWORD = epass.c_str();

	if (ptr = strchr(HOST, ':'))
	{
		static string host(HOST, ptr);

		PORT = stdx::atoi(ptr + 1);
		HOST = host.c_str();
	}

	if (HOST == NULL || *HOST == 0) HOST = "127.0.0.1";

	if (redis.connect(HOST, PORT, TIMEOUT))
	{
		if (PASSWORD && *PASSWORD)
		{
			if (redis.auth(PASSWORD) < 0)
			{
				ColorPrint(eRED, "REDIS[%s][%d]验证失败\n", HOST, PORT);

				return XG_SYSERR;
			}
		}

		if (cmd == NULL)
		{
			ColorPrint(eRED, "%s\n", "请输入要执行的命令");

			return XG_FAIL;
		}

		int res = 0;
		string tmp = cmd;

		stdx::toupper(tmp);

		if (tmp == "DELS" && key && *key)
		{
			vector<string> vec;

			if (redis.keys(vec, key) > 0)
			{
				if (cmdx::CheckCommand("确认要删除键值[%s]？", key))
				{
					ColorPrint(eWHITE, "%s\n", "--------------------------------------");

					for (int i = 0; i < vec.size(); i++)
					{
						if (redis.del(vec[i]))
						{
							ColorPrint(eGREEN, "删除键值[%s]成功\n", vec[i].c_str());
						}
						else
						{
							ColorPrint(eRED, "删除键值[%s]失败\n", vec[i].c_str());
						}
					}

					ColorPrint(eWHITE, "%s\n\n", "--------------------------------------");
				}
			}
			else
			{
				ColorPrint(eRED, "删除键值[%s]失败\n", key);
			}
		}
		else
		{
			int idx = 1;
			RedisConnect::Command request;

			while (true)
			{
				const char* data = GetCmdParam(idx++);

				if (data == NULL) break;

				request.add(data);
			}

			if ((res = redis.execute(request)) >= 0)
			{
				ColorPrint(eWHITE, "执行命令[%s]成功[%d][%d]\n", cmd, res, redis.getStatus());

				const vector<string>& vec = request.getDataList();

				if (vec.size() > 0)
				{
					ColorPrint(eWHITE, "%s\n", "--------------------------------------");

					for (const string& msg : vec)
					{
						ColorPrint(eGREEN, "%s\n", msg.c_str());
					}

					ColorPrint(eWHITE, "%s\n", "--------------------------------------");
					ColorPrint(eWHITE, "共返回%d条记录\n\n", vec.size());
				}
			}
			else
			{
				ColorPrint(eRED, "执行命令[%s]失败[%d][%s]\n", cmd, res, redis.getErrorString().c_str());
			}
		}
	}
	else
	{
		ColorPrint(eRED, "REDIS[%s][%d]连接失败\n", HOST, PORT);
	}

	return 0;
}
