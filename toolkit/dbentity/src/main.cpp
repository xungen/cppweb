#define XG_ENTITY_EXPORT_INC_PATH	""
#define XG_ENTITY_EXPORT_SRC_PATH	"src"


#include "EntityCreator.h"

int main(int argc, char* argv[])
{
	Process::Instance(argc, argv);

	string srcpath;
	string nmregex;
	sp<DBConnect> conn;
	DBConnectConfig addr;
	DBConnectPool* dbconnpool = NULL;
	
	if (argc < 2)
	{
		puts("please input configure filename");

		goto __EXIT__;
	}

	if (!yaml::open(argv[1]) || !addr.load(YAMLoader::Instance()))
	{
		puts("load configure failed");

		goto __EXIT__;
	}

	srcpath = yaml::config("database.sourcepath");
	nmregex = yaml::config("database.tableregex");
	dbconnpool = DBConnectPool::Create(YAMLoader::Instance());

	if (dbconnpool && (conn = dbconnpool->get()))
	{
		printf("DBTYPE : %s\n", conn->getSystemName());
		printf("DBNAME : %s\n", addr.name.c_str());

		puts("---------------------------------------------");

		EntityCreator::ExportEntity(conn.get(), nmregex, srcpath);
	}
	else
	{
		puts("connect the database failed");

		goto __EXIT__;
	}

	puts("---------------------------------------------");

__EXIT__:

	return 0;
}
