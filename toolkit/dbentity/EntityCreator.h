#ifndef XG_ENTITYCREATOR_H
#define XG_ENTITYCREATOR_H
///////////////////////////////////////////////////////////////////////
#include <regex>
#include <stdx/all.h>
#include <dbx/DBConnectPool.h>

class EntityCreator
{
protected:
	string head_name;
	string class_name;
	const string tab_name;
	vector<ColumnData>& vec;
	vector<string>& pkey_vec;

public:
	string getFileName()
	{
		string name = tab_name;
		return stdx::toupper(name);
	}
	const string& getClassName()
	{
		return class_name = "C" + getFileName();
	}
	EntityCreator(vector<ColumnData>& vColumnData, vector<string>& vPKeys, const string& name) : vec(vColumnData), pkey_vec(vPKeys), tab_name(name)
	{
	}

	bool create(const string& path);
	string getHeaderFilePath(const string& path);
	string getSourceFilePath(const string& path);
	const ColumnData& getPrimaryKeyData(const char* key_name);
	bool createHeaderFile(const string& filename);
	bool createSourceFile(const string& filename);
	bool writeHeader(TextFile& _out);
	bool writeSourceHeader(TextFile& _out);
	bool writeFindSource(TextFile& _out);
	bool writeNextSource(TextFile& _out);
	bool writeUpdateSource(TextFile& _out, bool flag = false);
	bool writeInsertSource(TextFile& _out);
	bool writeRemoveSource(TextFile& _out);
	bool writeClearSource(TextFile& _out);
	bool writeInitSource(TextFile& _out);
	bool writeCloseSource(TextFile& _out);
	bool writeGetValueSource(TextFile& _out);
	bool writeSetValueSource(TextFile& _out);
	
	static string GetTableQuerySQL(const string& tab_name)
	{
		return "SELECT * FROM " + tab_name + " WHERE 1=0";
	}

	static int ExportEntity(DBConnect* db, const string& nmregex, const string& srcpath);
	static int GetTableColumnData(DBConnect* conn, vector<ColumnData>& vec, const string& tab_name);
};
///////////////////////////////////////////////////////////////////////
#endif

