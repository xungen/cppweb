#include <webx/route.h>
#include <http/HttpHelper.h>

class ExecModule : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBCGI(CGI_PROTECT, "${filename}")
DEFINE_HTTP_CGI_EXPORT_FUNC(ExecModule)

static HttpServer* app = HttpServer::Instance();

static bool SystemCommand(char cmd)
{
	return stdx::async([cmd](){
		static char* data = app->getShareData()->data;

		data[0] = cmd;
		data[1] = '-';

		Socket().connect(app->getHost(), app->getPort());
	}) ? true : false;
}

EXTERN_DLL_FUNC int HttpPluginInit(HttpServer* app, const char* path)
{
	return XG_OK;
}

int ExecModule::process()
{
	param_string(cmd);
	param_string(type);
	param_string(name);

	int res = 0;
	string root = app->getPath();

	stdx::tolower(cmd);
	stdx::tolower(type);
	webx::CheckSystemRight(this);

	if (cmd == "get")
	{
		out << webx::GetRemoteConfig(type, name);

		return XG_OK;
	}
	else if (cmd == "sync")
	{
		res = webx::SyncRemoteConfig(type, name);
	}
	else if (cmd == "call")
	{
		param_int(async);
		param_string(path);
		param_string(param);

		webx::CheckFilePath(path);

		if (async > 0)
		{
			res = stdx::async([=](){
				webx::GetRemoteResult(ctx, path, param);
			}) ? XG_OK : XG_SYSBUSY;
		}
		else
		{
			SmartBuffer data = webx::GetRemoteResult(ctx, path, param);

			if (data.isNull()) throw Exception(XG_SYSERR);

			clearResponse();

			out << data.str();

			return data.size();
		}
	}
	else if (cmd == "updatemodule")
	{
		param_int(port);
		param_string(src);
		param_string(dest);
		param_string(host);
		param_string(flag);

		if (src.length() < 7) throw Exception(XG_PARAMERR);

		if (memcmp(src.c_str(), "http:", 5) == 0)
		{
			XFile file;
			SmartBuffer data = HttpHelper::GetResult(src);

			if (data.size() < 0xFF) throw Exception(XG_ERROR);

			file.open(src = root + "dat/" + path::name(dest), eCREATE);

			if (file.write(data.str(), data.size()) < 0) throw Exception(XG_ERROR);
		}
		else
		{
			src = root + src;
			src = stdx::replace(src, "\\", "/");
			src = stdx::replace(src, "//", "/");
		}

		if (host.empty())
		{
			dest = root + dest;
			dest = stdx::replace(dest, "\\", "/");
			dest = stdx::replace(dest, "//", "/");

			res = app->updateModuleFile(src, dest) ? XG_OK : XG_ERROR;
		}
		else
		{
			string path;
			HttpRequest request(this->request->getPath());

			if (flag == "R")
			{
				src = src.substr(root.length());
			}
			else
			{
				vector<string> addrlist;

				if (Socket::GetLocalAddress(addrlist) <= 0) throw Exception(XG_ERROR);

				path::rename(src, path = root + "dat/pub/" + path::name(src));

				src = HttpHelper::GetLink(path.substr(root.length()), addrlist.front(), app->getPort());
			}

			request.setHeadHost(host, port);
			request.setParameter("cmd", cmd);
			request.setParameter("src", src);
			request.setParameter("dest", dest);

			SmartBuffer data = request.getResult(host, port);

			if (path.length() > 0) path::remove(path);

			if (data.isNull()) throw Exception(XG_ERROR);

			out << data.str();

			return XG_OK;
		}
	}
	else
	{
		param_int(port);
		param_string(host);

		if (host.length() > 0)
		{
			HttpRequest request(this->request->getPath());

			request.setHeadHost(host, port);
			request.setParameter("cmd", cmd);

			SmartBuffer data = request.getResult(host, port);

			if (data.isNull()) throw Exception(XG_ERROR);

			out << data.str();

			return XG_OK;
		}

		if (cmd == "exit")
		{
			res = SystemCommand('k') ? XG_OK : XG_ERROR;
		}
		else if (cmd == "reload")
		{
			res = SystemCommand('r') ? XG_OK : XG_ERROR;
		}
		else if (cmd == "restart")
		{
			res = SystemCommand('s') ? XG_OK : XG_ERROR;
		}
		else
		{
			throw Exception(XG_PARAMERR, "invalid parameter[cmd]");
		}
	}

	json["datetime"] = DateTime::ToString();
	json["code"] = res;
	out << json;

	return XG_OK;
}