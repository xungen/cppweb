<%@ path=${filename}%>
<%@ include="http/HttpHelper.h"%>
<%
	param_int(port);
	param_string(host);
	param_string(path);
	param_string(padding);

	if (host.length() > 0)
	{
		HttpRequest request(this->request->getPath());

		request.setHeadHost(host, port);
		request.setParameter("path", path);
		request.setParameter("padding", padding);

		SmartBuffer data = request.getResult(host, port);

		if (data.isNull()) throw Exception(XG_ERROR);

		out << data.str();

		return XG_OK;
	}

	if (padding.empty()) padding = "2vh 20vw";

	tuple<string, string, string> item = app->getCgiDoc(path);
	string reqdoc = get<0>(item);
	string rspdoc = get<1>(item);
	string remark = get<2>(item);

	if (reqdoc.empty() && rspdoc.empty() && remark.empty())
	{
		reqdoc = rspdoc = "<td style='color:#B00;height:24px'>没有参数或没有编写文档</td><td></td><td></td><td></td>";
	}

	size_t pos = path.find('@');

	path = CgiMapData::GetKey(path);

	if (pos == string::npos)
	{
		path = stdx::replace(path, "/", ".");
	}
	else
	{
		path = path.substr(0, pos);
	}

	reqdoc = stdx::replace(reqdoc, "<td>required</td>", "<td>必填</td>");
	rspdoc = stdx::replace(rspdoc, "<td>required</td>", "<td>必填</td>");
	reqdoc = stdx::replace(reqdoc, "<td>optional</td>", "<td>可选</td>");
	rspdoc = stdx::replace(rspdoc, "<td>optional</td>", "<td>可选</td>");

	int line = GetStringCount(reqdoc.c_str(), "\n") + GetStringCount(rspdoc.c_str(), "\n");
%>
<!DOCTYPE HTML>
<html>
<head>
<title>接口文档</title>
<meta name='referrer' content='always'/>
<meta http-equiv='x-ua-compatible' content='ie=edge,chrome=1'/>
<meta http-equiv='content-type' content='text/html; charset=utf-8'/>
<meta name='viewport' content='width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no'/>
<style>
.CgiDocDiv{
	width: 100%;
	font-size: 13px;
	padding: <%=padding%>;
	display: inline-block;
	box-sizing: border-box;
	font-family: '宋体', 'Helvetica', 'Tahoma', 'Verdana', 'Courier New';
}
.CgiDocHeadDiv{
	width: 100%;
	padding: 4px;
	display: inline-block;
	box-sizing: border-box;
	border-bottom: 1px solid #AAA;
}
.CgiDocPathDiv{
	color: #B00;
	font-size: 2.4rem;
	letter-spacing: -1px;
	display: inline-block;
}
.CgiDocRemarkDiv{
	color: #456;
	font-size: 1.8rem;
	font-weight: bold;
	padding-left: 8px;
	letter-spacing: -1px;
	display: inline-block;
}
.CgiDocParamDiv{
	padding: 4px;
	margin-top: 24px;
	font-weight: bold;
	font-size: 1.4rem;
}
.CgiDocListTable{
	width: 100%;
	background: #FFF;
	border-radius: 4px;
	border-collapse: collapse;
}
.CgiDocListTable span{
	color: #098;
	padding-right: 4px;
}
.CgiDocListTable td{
<%if (line > 30){%>
	padding: 1px 8px;
<%}else if (line > 20){%>
	padding: 2px 8px;
<%}else{%>
	padding: 3px 8px;
<%}%>
	line-height: 18px;
}
.CgiDocListTable tr:first-child{
	background: #888;
	font-weight: bold;
	font-size: 0.95rem;
}
.CgiDocListTable tr td:nth-child(1){
	min-width: 15vw;
}
.CgiDocListTable tr td:nth-child(2){
	min-width: 5vw;
}
.CgiDocListTable tr td:nth-child(3){
	min-width: 5vw;
}
.CgiDocListTable tr td:nth-child(4){
	min-width: 25vw;
}
</style>
</head>

<body>
	<div class='CgiDocDiv'>
		<div class='CgiDocHeadDiv'>
			<div class='CgiDocPathDiv'><%=path%></div>
			<div class='CgiDocRemarkDiv'><%=remark%></div>
		</div>

		<div class='CgiDocParamDiv'>请求参数</div>
		<table class='CgiDocListTable'>
				<tr><td>字段</td><td>类型</td><td>属性</td><td>说明</td></tr>
				<%=reqdoc%>
		</table>

		<div class='CgiDocParamDiv'>应答参数</div>
		<table class='CgiDocListTable'>
				<tr><td>字段</td><td>类型</td><td>属性</td><td>说明</td></tr>
				<%=rspdoc%>
		</table>
	</div>
</body>
</html>