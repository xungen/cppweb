<%@ path=${filename}%>
<%@ include="dbentity/T_XG_GROUP.h"%>
<%@ include="dbentity/T_XG_ACCESS.h"%>

<div><button v-show='editaccessright>=0' @click='addAccess()' class='TextButton'><green>添加资源</green></div>

<%
	param_string(flag);

	if (flag.length() > 0)
	{
		param_string(path);
		param_string(name);
		param_string(param);
		param_string(folder);

		checkLogin();

		path = CgiMapData::GetKey(path);

		int res = 0;
		CT_XG_ACCESS tab;
		sp<DBConnect> dbconn;

		tab.init(dbconn = webx::GetDBConnect(ctx));

		auto getMenuId = [&](const string& folder){
			string id;
			string sql = "SELECT ID FROM T_XG_MENU WHERE FOLDER=? AND (TITLE IS NULL OR TITLE='')";

			dbconn->select(id, sql, folder);

			if (id.empty()) throw Exception(XG_PARAMERR);

			return id;
		};

		tab.path = path;
		tab.name = name;
		tab.param = param;
		tab.statetime.update();

		if (flag == "A")
		{
			tab.menuid = getMenuId(folder);

			res = tab.insert();
		}
		else if (flag == "U")
		{
			tab.menuid = getMenuId(folder);

			res = tab.update();
		}
		else if (flag == "D")
		{
			if (tab.find() && tab.next())
			{
				string tag;
				string pathlist;
				CT_XG_GROUP grptab;
				map<string, string> upmap;
				
				if (tab.param.val().empty())
				{
					tag = "," + tab.path.val() + ",";
				}
				else
				{
					tag = "," + tab.path.val() + "?" + tab.param.val() + ",";
				}

				grptab.init(dbconn);
				grptab.find("1=1");

				while (grptab.next())
				{
					string old = grptab.pathlist.toString();

					pathlist = "," + old + ",";
					pathlist = stdx::replace(pathlist, tag, ",");
					pathlist = pathlist.substr(1, pathlist.length() - 2);

					if (old == pathlist) continue;

					upmap[grptab.id.val()] = pathlist;
				}

				grptab.clear();
				grptab.statetime.update();

				for (auto& item : upmap)
				{
					grptab.id = item.first;
					grptab.pathlist = item.second;

					if (grptab.update() < 0) throw Exception(XG_SYSERR);
				}

				res = tab.remove();
			}

			sp<Session> session = webx::GetLocaleSession("ACCESS");

			Process::SetObject("HTTP_GROUP_SET", NULL);
			Process::SetObject("HTTP_MENU_SET", NULL);

			if (session) session->disable();
		}

		return simpleResponse(res);
	}

	webx::PrintRecordview(out, "${access}");
%>

<script>
{
	var menulist = [];
	var folderlist = [];
	var editaccessright = getAccess('${filename}');

	getHttpResult('/getmenulist', {level: 1}, function(data){
		if (data.code < 0){
		}
		else{
			for (let i = 0; i < data.list.length; i++){
				folderlist.push(data.list[i].folder);
				menulist.push(data.list[i]);
			}
		}
	});

	function getMenuFolder(id){
		for (let i = 0; i < menulist.length; i++){
			if (id == menulist[i].id) return menulist[i].folder;
		}
		return id;
	}

	$recordvmdata.filter = {
		menuid: function(text){
			return getMenuFolder(text);
		}
	}

	$recordvmdata.button = [{
		title: '编辑',
		click: editAccess,
		disable: editaccessright < 0
	},{
		title: '删除',
		click: removeAccess,
		disable: editaccessright < 0
	}];

	var vmdata = {
		title: ['所属模块', '接口名称', '接口路径', '请求参数'],
		model: {folder: '', name: '', path: '', param: ''},
		style: [
			{select: folderlist, minlength: 1},
			{size: 20, minlength: 2, maxlength: 32},
			{size: 20, minlength: 1, maxlength: 128},
			{type: 'textarea', minlength: 0, maxlength: 256}
		]
	};

	function addAccess(){
		var data = Object.assign({}, vmdata);

		data.model = Object.assign({}, vmdata.model);

		var elem = showConfirmDialog(data, '添加资源', function(flag){
			if (flag){
				data.model['flag'] = 'A';
				
				getHttpResult('/${filename}', data.model, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code == XG_DUPLICATE){
						showToast('资源配置已存在');
					}
					else if (data.code < 0){
						showToast('添加资源失败（请检查参数格式）');
					}
					else{
						$recordvmdata.reload();
						showToast('添加资源成功');
					}
				});
			}
		});

		$(elem.param).width($(elem.path).width());
	}

	function editAccess(item){
		var data = Object.assign({}, vmdata);

		data.model = {folder: getMenuFolder(item.menuid), name: item.name, path: item.path, param: item.param};

		var elem = showConfirmDialog(data, '编辑资源', function(flag){
			if (flag){
				data.model['flag'] = 'U';
				
				getHttpResult('/${filename}', data.model, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('修改资源失败（请检查参数格式）');
					}
					else{
						$recordvmdata.reload();
						showToast('修改资源成功');
					}
				});
			}
		});

		$(elem.path).attr('disabled', true);
		$(elem.param).attr('disabled', true).width($(elem.path).width());
	}

	function removeAccess(item){
		showConfirmMessage('是否决定删除资源[' + item.name + ']配置？', '删除选项', function(flag){
			if (flag){
				getHttpResult('/${filename}', {flag: 'D', path: item.path, param: item.param}, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('删除资源失败');
					}
					else{
						$recordvmdata.reload();
						showToast('删除资源成功');
					}
				});
			}
		});
	}
}
</script>