<%@ path=${filename}%>
<%@ include="dbentity/T_XG_GROUP.h"%>

<div><button v-show='editgroupaccess>=0' @click='addGroup()' class='TextButton'><green>添加角色</green></div>

<%
	param_string(flag);

	if (flag.length() > 0)
	{
		param_string(icon);
		param_string(name);
		param_string(remark);
		param_name_string(id);

		checkLogin();

		int res = 0;
		CT_XG_GROUP tab;

		tab.init(webx::GetDBConnect(ctx));
		tab.statetime.update();
		tab.remark = remark;
		tab.id = id;

		if (flag == "A")
		{
			if (id == "root") throw Exception(XG_PARAMERR);
			if (name.empty()) throw Exception(XG_PARAMERR);
			if (icon.empty()) icon = "/res/img/menu/group.png";

			tab.name = name;
			tab.icon = icon;
			tab.enabled = 2;

			if ((res = tab.insert()) > 0)
			{
				json["icon"] = tab.icon.val();
				json["id"] = tab.id.val();
			}
		}
		else if (flag == "U")
		{
			param_int(force);
			param_string(menulist);
			param_string(pathlist);

			tab.remark = remark;

			if (icon.length() > 0) tab.icon = icon;
			if (name.length() > 0) tab.name = name;
			if (force > 0 || menulist.length() > 0) tab.menulist = menulist;
			if (force > 0 || pathlist.length() > 0) tab.pathlist = pathlist;

			res = tab.update();
		}
		else if (flag == "D")
		{
			if (tab.find() && tab.next())
			{
				if (tab.enabled.val() < 2)
				{
					res = XG_AUTHFAIL;
				}
				else
				{
					res = tab.remove();
				}
			}
			else
			{
				res = XG_NOTFOUND;
			}
		}
		else
		{
			res = XG_PARAMERR;
		}

		if (res >= 0)
		{
			sp<Session> session = webx::GetLocaleSession("ACCESS");

			Process::SetObject("HTTP_GROUP_SET", NULL);
			Process::SetObject("HTTP_MENU_SET", NULL);

			if (session) session->disable();
		}

		return simpleResponse(res);
	}

	webx::PrintRecordview(out, "${group}");
%>

<script>
{
	var editgroupaccess = getAccess('${filename}');

	$recordvmdata.sortindex = 1;

	$recordvmdata.filter['icon'] = function(text){
		return "<img style='width:20px;height:20px' src='" + text + "'/>"
	}

	$recordvmdata.button = [{
		title: '菜单管理',
		click: editMenu,
		disable: function(item){
			return item.enabled <= 0 || editgroupaccess < 0;
		}
	},{
		title: '编辑',
		click: editGroup,
		disable: function(item){
			return item.enabled <= 1 || editgroupaccess < 0;
		}
	},{
		title: '删除',
		click: removeGroup,
		disable: function(item){
			return item.enabled <= 1 || editgroupaccess < 0;
		}
	}];

	function addGroup(){
		editGroup(null);
	}

	function editMenu(item){
		let msg = getHttpResult('/app/workspace/pub/groupmenu.htm').replace('${groupid}', item.id);

		showToastMessage(msg, null, null, null, true);
	}

	function editGroup(item){
		let id = null;
		let name = null;
		let icon = null;
		let remark = null;

		showConfirmMessage("<table class='DialogTable'><tr><td><v-icon title='角色图标' path='menu' id='GroupIcon'></v-icon></td></tr><tr><td><v-text id='GroupIdText' title='角色编号' maxlength='16' size='14'></v-text></td></tr><tr><td><v-text id='GroupNameText' title='角色名称' maxlength='16' size='14'></v-text></td></tr><tr><td><v-textarea id='GroupRemarkText' title='角色说明' maxlength='256'></v-textarea></td></tr></table>", item ? '编辑角色' : '添加角色', function(flag){
			if (flag == 0) return true;

			id = $('#GroupIdText').val();
			name = $('#GroupNameText').val();
			remark = $('#GroupRemarkText').val();

			if (strlen(id) == 0){
				$('#GroupIdText').focus();
				return false;
			}
			else if (strlen(name) == 0){
				$('#GroupNameText').focus();
				return false;
			}
			else{
				icon = getBackgroundImage(getCtrl('GroupIcon'));

				getHttpResult('/${filename}', {flag: item ? 'U' : 'A', id: id, name: name, icon: icon, remark: remark}, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code == XG_DUPLICATE){
						showToast('角色编号已经存在');
					}
					else if (data.code < 0){
						showToast(flag == 'A' ? '添加角色失败' : '修改角色失败');
					}
					else{
						$recordvmdata.reload();
						showToast(flag == 'A' ? '添加角色成功' : '修改角色成功');
					}
				});
			}
		});

		if (item){
			$('#GroupIcon').css('background-color', '#FFF').css('background-image', 'url(' + item.icon + ')');
			$('#GroupIdText').attr('disabled', true).val(item.id);
			$('#GroupRemarkText').val(item.remark);
			$('#GroupNameText').val(item.name);
		}
		else{
			$('#GroupIcon').css('background-color', '#FFF').css('background-image', 'url(/res/img/menu/group.png)');
			$('#GroupIdText').focus();
		}
	}

	function removeGroup(item){
		showConfirmMessage('是否决定删除角色[' + item.name + ']？', '删除选项', function(flag){
			if (flag){
				getHttpResult('/${filename}', {flag: 'D', id: item.id}, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('删除角色失败');
					}
					else{
						$recordvmdata.reload();
						showToast('删除角色成功');
					}
				});
			}
		});
	}
}
</script>