<%@ path=${filename}%>
<%@ include="dbentity/T_XG_ROUTE.h"%>

<div><button v-show='editrouteaccess>=0' @click='addRoute()' class='TextButton'><green>添加服务</green></div>

<%
	param_string(flag);

	if (flag.length() > 0)
	{
		param_string(id);
		param_string(name);
		param_string(host);
		param_string(port);
		param_string(remark);
		param_string(enabled);

		webx::CheckFileName(id, 0);
		webx::CheckFileName(name, 0);

		int res = 0;
		CT_XG_ROUTE tab;
		const string& user = checkLogin();

		tab.init(webx::GetDBConnect(ctx));

		tab.user = user;
		tab.remark = remark;

		if (id.length() > 0) tab.id = id;
		if (name.length() > 0) tab.name = name;
		if (host.length() > 0) tab.host = host;
		if (port.length() > 0) tab.port = port;
		if (enabled.length() > 0) tab.enabled = enabled;

		tab.statetime.update();

		if (flag == "D")
		{
			if (id.empty()) throw Exception(XG_PARAMERR);

			res = tab.remove();
		}
		else if (flag == "U")
		{
			if (id.empty()) throw Exception(XG_PARAMERR);

			res = tab.update();
		}
		else if (flag == "A")
		{
			if (name.empty() || host.empty() || port.empty() || enabled.empty()) throw Exception(XG_PARAMERR);

			for (int i = 0; i < 5; i++)
			{
				tab.id = DateTime::GetBizId();

				if ((res = tab.insert()) >= 0) break;
			}
		}

		if (res >= 0)
		{
			auto session = webx::GetLocaleSession("SYSTEM_ROUTELIST");

			if (session) session->clear();

			json["statetime"] = tab.statetime.toString();
		}

		return simpleResponse(res);
	}

	webx::PrintRecordview(out, "${route}");
%>

<script>
{
	var editrouteaccess = getAccess('${filename}');

	$recordvmdata.title = {
		name: '服务名称',
		host: '服务地址',
		port: '监听端口',
		enabled: centerText('服务权重'),
		proctime: '响应时间&nbsp;&nbsp;',
		statetime: '更新时间',
		remark: '服务说明'
	}

	$recordvmdata.filter['enabled'] = function(val){
		if (val == 0) val = '<red>离线</red>';
		return centerText('<b>' + val + '</b>');
	}

	$recordvmdata.filter['proctime'] = function(val){
		if (strlen(val) <= 0){
			return '<b><yellow>等待检测</yellow></b>';
		}
		else if (val <= 0){
			return '<b><red>连接异常</red></b>';
		}
		else{
			val = Math.floor(val / 1000);

			if (val > 500){
				return '用时<b><red>' + val + '</red></b>毫秒';
			}
			else if (val > 200){
				return '用时<b><blue>' + val + '</blue></b>毫秒';
			}
			else{
				return '用时<b><green>' + val + '</green></b>毫秒';
			}
		}
	}

	$recordvmdata.button = [{
		color: '#009',
		title: '编辑',
		click: editRoute,
		disable: function(item){
			return editrouteaccess < 0;
		}
	},{
		color: '#C00',
		title: '删除',
		click: removeRoute,
		disable: function(item){
			return editrouteaccess < 0;
		}
	},{
		title: '接口管理',
		click: function(item){
			window.open('/singlepage?title=接口管理&path=/app/workspace/pub/cgilist.htm&host=' + item.host + '&port=' + item.port);
		},
		disable: function(item){
			return editrouteaccess < 0;
		}
	},{
		title: '应用管理',
		click: function(item){
			window.open('/singlepage?title=应用管理&path=/app/workspace/pub/modulelist.htm&host=' + item.host + '&port=' + item.port);
		},
		disable: function(item){
			return editrouteaccess < 0;
		}
	}];

	function checkPort(port){
		return port == parseInt(port);
	}

	function checkHost(host){
		var reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/
		return reg.test(host);
	}

	let vmdata = {
		title: ['服务权重', '服务名称', '服务地址', '服务端口', '服务说明'],
		model: {enabled: 0, name: '', host: '', port: '', remark: ''},
		style: [
			{select: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]},
			{size: 18, minlength: 2, maxlength: 32},
			{size: 18, minlength: 7, maxlength: 15, check: checkHost},
			{size: 18, minlength: 1, maxlength: 5, check: checkPort},
			{size: 18, minlength: 0, maxlength: 128, type: 'textarea'}
		]
	};
		
	function addRoute(){
		var data = Object.assign({}, vmdata);

		data.model = Object.assign({}, vmdata.model);

		var elem = showConfirmDialog(data, '添加服务', function(flag){
			if (flag){
				data.model['flag'] = 'A';

				getHttpResult('/${filename}', data.model, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code == XG_DUPLICATE){
						showToast('参数已存在');
					}
					else if (data.code < 0){
						showToast('参数添加失败');
					}
					else{
						$recordvmdata.reload();
					}
				});
			}
		});

		$(elem.remark).width($(elem.name).width());
	}

	function editRoute(item){
		var data = Object.assign({}, vmdata);

		data.model = {enabled: item.enabled, name: item.name, host: item.host, port: item.port, remark: item.remark};

		var elem = showConfirmDialog(data, '编辑服务', function(flag){
			if (flag){
				data.model['flag'] = 'U';
				data.model['id'] = item.id;

				getHttpResult('/${filename}', data.model, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('修改服务失败');
					}
					else{
						$recordvmdata.reload();
					}
				});
			}
		});

		$(elem.remark).width($(elem.name).width());
		$(elem.name).attr('disabled', true);
	}

	function removeRoute(item){
		showConfirmMessage('是否决定服务[' + item.name + ']？', '删除选项', function(flag){
			if (flag){
				getHttpResult('/${filename}', {flag: 'D', id: item.id}, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('删除服务失败');
					}
					else{
						$recordvmdata.reload();
						showToast('删除服务成功');
					}
				});
			}
		});
	}

	setSingletonInterval('routelist_timer', 3000, function(){
		$recordvmdata.reload();
	});
}
</script>