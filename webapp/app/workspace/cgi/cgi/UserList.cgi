<%@ path=${filename}%>

<div><button v-show='edituseraccess>=0' @click='addUser()' class='TextButton'><green>添加用户</green></div>

<%
	webx::PrintRecordview(out, "${user}");
%>

<script>
{
	var optionhtml = '';
	var groupidlist = [];
	var groupnamelist = [];
	var edituseraccess = getAccess('edituser');

	getHttpResult('/getgrouplist', null, function(data){
		if (data.code == XG_TIMEOUT){
			sessionTimeout();
		}
		else if (data.code == XG_AUTHFAIL){
			showNoAccessToast();
		}
		else if (data.code < 0){
			showToast('加载数据失败');
		}
		else{
			var html = '';

			$.each(data.list, function(idx, item){
				groupidlist[idx] = item.id;
				groupnamelist[idx] = item.name;
				html += "<option value='" + item.id + "'>" + item.name + "</option>";
			});

			optionhtml = html;
		}
	});

	$recordvmdata.title = {
		user: '用户',
		name: '昵称',
		mail: '邮箱',
		phone: centerText('电话', 'bold'),
		maingroup: centerText('主群组', 'bold'),
		addgroup: centerText('附加群组', 'bold'),
		enabled: centerText('状态', 'bold'),
		address: '地址',
	}

	$recordvmdata.check = function(data){
		$.each(data.list, function(index, item){
			var arr = item.grouplist.split(',');
			item['addgroup'] = arr.length > 1 ? arr[1] : '';
			item['maingroup'] = arr.length > 0 ? arr[0] : '';
			item['disabled'] = edituseraccess < 0 || item.user == 'root' || item.user == 'system' ? 'disabled' : '';
		});
	}

	$recordvmdata.button = [{
		title: '修改密码',
		click: resetUserPasswd,
		disable: function(item){
			return edituseraccess < 0;
		}
	},{
		title: '删除',
		click: removeUserRecord,
		disable: function(item){
			return edituseraccess < 0 || item.user == 'root' || item.user == 'system';
		}
	}];

	$recordvmdata.filter['enabled'] = function(val, item){
		var msg = "<select " + item.disabled + " onchange='updateUserStatus(this)' user='" + item.user + "' class='TextSelect' style='float:none'><option value='0' selected>停用</option><option value='1'>启用</option></select>"
		if (val > 0) msg = "<select " + item.disabled + " onchange='updateUserStatus(this)' user='" + item.user + "' class='TextSelect' style='float:none'><option value='0'>停用</option><option value='1' selected>启用</option></select>"
		return centerText(msg);
	}
	
	$recordvmdata.filter['addgroup'] = function(val, item){
		var msg = "<select " + item.disabled + " onchange='updateUserGroup(1,this)' user='" + item.user + "' class='TextSelect' style='float:none'><option value=''></option>" + optionhtml + "</select>";
		var tag = "value='" + val + "'";
		var pos = msg.indexOf(tag);
		if (pos > 0){
			pos += tag.length;
			msg = msg.substring(0, pos) + ' selected ' + msg.substring(pos);
		}
		return centerText(msg);
	}

	$recordvmdata.filter['maingroup'] = function(val, item){
		var msg = "<select " + item.disabled + " onchange='updateUserGroup(0,this)' user='" + item.user + "' class='TextSelect' style='float:none'>" + optionhtml + "</select>";
		var tag = "value='" + val + "'";
		var pos = msg.indexOf(tag);
		if (pos > 0){
			pos += tag.length;
			msg = msg.substring(0, pos) + ' selected ' + msg.substring(pos);
		}
		return centerText(msg);
	}

	function addUser(){
		var data = {
			title: ['登录名称', '用户昵称', '用户邮箱', '联系电话', '登录密码', '确认密码'],
			model: {user: '', name: '', mail: '', phone: '', password: '', checkpassword: ''},
			style: [
				{size: 24, minlength: 4, maxlength: 32},
				{size: 24, minlength: 4, maxlength: 32},
				{size: 24, minlength: 8, maxlength: 64},
				{size: 24, minlength: 0, maxlength: 18},
				{size: 24, minlength: 6, maxlength: 64, type: 'password'},
				{size: 24, minlength: 6, maxlength: 64, type: 'password'}
			]
		};

		var elem = showConfirmDialog(data, '添加用户', function(flag){
			if (flag){
				var param = Object.assign({flag: 'A', enabled: 1}, data.model);

				if (!checkEmail(param.mail)){
					setMessageErrorText('邮箱地址格式错误', elem.mail);
					return false;
				}

				if (param.password != param.checkpassword){
					setMessageErrorText('两次输入密码不一致', elem.checkpassword);
					return false;
				}

				getHttpResult('/edituser', param, function(data){
					if (data.code ==  XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code == XG_DUPLICATE){
						showToast('用户已经存在');
						$(elem.user).focus();
					}
					else if (data.code < 0){
						showToast('添加用户失败');
					}
					else{
						$recordvmdata.reload();
						showToast('添加用户成功');
					}
				});
			}
		});
	}

	function updateUserGroup(type, elem){
		var item = null;
		var user = $(elem).attr('user');
		var list = $recordvmdata.recordview.data.list;

		for (var i = 0; i < list.length; i++){
			if (user == list[i].user){
				item = list[i];
				break;
			}
		}

		if (type == 0){
			item.maingroup = $(elem).val();
		}
		else{
			item.addgroup = $(elem).val();
		}

		if (item.addgroup == item.maingroup){
			if (type) $(elem).val('');

			item.addgroup = '';
		}

		item.grouplist = item.maingroup;

		if (strlen(item.addgroup) > 0) item.grouplist += ',' + item.addgroup;

		getHttpResult('/edituser', {flag: 'U', user: user, group: item.grouplist}, function(data){
			if (data.code ==  XG_TIMEOUT){
				sessionTimeout();
			}
			else if (data.code == XG_AUTHFAIL){
				showNoAccessToast();
			}
			else if (data.code < 0){
				showMessage('修改用户群组失败。', '错误提示', null, null, function(flag){
					$recordvmdata.reload();
				});
			}
			else{
				$recordvmdata.recordview.update();
				showToast('用户群组已修改');
			}
		});
	}

	function resetUserPasswd(item){
		var msgbox = showConfirmMessage("<table class='DialogTable'><tr><td><v-text title='输入密码' id='PasswdText' type='password' maxlength='32'></v-text></td></tr><tr><td><v-text title='确认密码' id='PasswdConfirmText' type='password' maxlength='32'></v-text></td></tr></table>", '修改用户[' + item.name + ']密码', function(flag){
			if (flag == 0) return true;
			var password = $('#PasswdText').val();
			var confirm = $('#PasswdConfirmText').val();

			if (strlen(password) == 0){
				$('#PasswdText').focus();
				return false;
			}
			else if (strlen(confirm) == 0){
				$('#PasswdConfirmText').focus();
				return false;
			}
			else if (password != confirm){
				$('#PasswdConfirmText').focus();
				showToast('再次输入密码不一致');
				return false;
			}

			getHttpResult('/edituser', {flag: 'S', user: item.user, password: password}, function(data){
				if (data.code ==  XG_TIMEOUT){
					sessionTimeout();
				}
				else if (data.code == XG_AUTHFAIL){
					showNoAccessToast();
				}
				else if (data.code < 0){
					showToast('设置登录密码失败');
				}
				else{
					showToast('登录密码更新成功');
				}
			});
		});
		
		$('#PasswdText').focus().keydown(function(e){
			if (e.which == 13) $('#PasswdConfirmText').focus();
		});
		$('#PasswdConfirmText').keydown(function(e){
			if (e.which == 13) msgbox.confirm();
		});
	}

	function removeUserRecord(item){
		if (item.user == 'system') return showToast('不能删除系统管理员用户');

		showConfirmMessage('是否要删除[' + item.name + ']用户?', '删除用户', function(flag){
			if (flag == 0) return true;

			getHttpResult('/edituser', {flag: 'D', user: item.user}, function(data){
				if (data.code ==  XG_TIMEOUT){
					sessionTimeout();
				}
				else if (data.code ==  XG_AUTHFAIL){
					showNoAccessToast();
				}
				else if (data.code < 0){
					showMessage('删除用户失败。', '错误提示', null, null, function(flag){
						$recordvmdata.reload();
					});
				}
				else{
					$recordvmdata.reload();
					showToast('删除用户成功');
				}
			});
		});
	}

	function updateUserStatus(elem){
		var val = $(elem).val();
		var user = $(elem).attr('user');

		getHttpResult('/edituser', {flag: 'U', user: user, enabled: val}, function(data){
			if (data.code ==  XG_TIMEOUT){
				sessionTimeout();
			}
			else if (data.code ==  XG_AUTHFAIL){
				showNoAccessToast();
			}
			else if (data.code < 0){
				showMessage('修改用户状态失败。', '错误提示', null, null, function(flag){
					$recordvmdata.reload();
				});
			}
			else{
				showToast('用户已' + (val ? '启用' : '停用'));
			}
		});
	}
}
</script>