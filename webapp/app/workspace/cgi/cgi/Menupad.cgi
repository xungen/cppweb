<%@ path=${filename}%>

<table id='MenuEditTable'>
	<tr>
		<td id='MenuListDiv'>
			<table id='MenuListTable'></table>
		</td>
		<td id='MenuEditDiv'>
			<table id='MenuContentTable' v-bind:width="editmenuaccess>0?'960px':'700px'">
				<tr>
					<td><v-text id='MenuTitleText' title='菜单名称' maxlength='20'></v-text></td>
					<td><v-text id='MenuUrlText' title='菜单路径' maxlength='128'></v-text></td>
					<td><v-select id='MenuEnabledSelect' title='可用标志' option='可编辑|可展示'></v-select></td>
					<td><v-icon id='MenuIcon' path='menu' size='13' title='菜单图标'></v-icon></td>
					<td v-show='editmenuaccess>0'>
						<v-button id='SaveMenuButton' title='保存'></v-button>
						<v-button id='DeleteMenuButton' title='删除'></v-button>
						<v-button id='AddMenuButton' title='添加菜单'></v-button>
					</td>
				</tr>
			</table>
			<table id='GroupListTable'></table>
			<div id='EmptyMenuContentDiv'>
				<div style='color:#555;font-size:1.3rem;padding-top:20px'>您还没有添加任何菜单</div>
				<div style='color:#555;font-size:1.3rem;padding-top:20px'>菜单包含图标+标题+链接</div>
				<a href='javascript:void(0)' class='TextLink'><div id='AddMenuLink' style='font-size:1.3rem;padding-top:20px'>点击此处马上添加一个菜单</div></a>
			<div>
		</td>
	</tr>
</table>

<script>
{
	var param = null;
	var curid = null;
	var cururl = null;
	var curicon = null;
	var curtitle = null;
	var titlelist = null;
	var curmenupad = null;
	var curenabled = null;
	var modifygroup = null;
	var curgrouplist = null;
	var folderenabled = null;
	var editmenuaccess = getAccess('editmenu');
	var curfolder = '<%=request->getParameter("folder")%>';

	getVue('MenuEditTable');
	autoModifyContentSize('MenuEditTable');
	$('#MenuEnabledSelect').attr('disabled', true);

	var menubox = new ContextMenu(['前移', '后移'], function(text, elem){
		let arr = [];
		let item = $.pack(getContextMenuAttach());

		if (text == '前移'){
			item.prev().insertAfter(item);
		}
		else{
			item.next().insertBefore(item);
		}

		item.parent().children().each(function(){
			arr.push($(this).children('td:last').html());
		});

		getHttpResult('/editmenu', {flag: 'M', folder: curfolder, title: arr.join(',')});
	});

	function addMenuResult(flag){
		if (flag == 1) return editMenuItem(param, 'A');
	}

	function delMenuResult(flag){
		if (flag == 1) return editMenuItem(param, 'D');
	}

	function delMenuFolderResult(flag){
		if (flag == 1) return editMenuItem(param, 'R');
	}

	$('#AddMenuLink').click(function(){
		$('#AddMenuButton').click();
	});

	$('#AddMenuButton').click(function(){
		showConfirmMessage("<table class='DialogTable'><tr><td><v-icon id='AddMenuIcon' path='menu' title='菜单图标'></v-icon></td></tr><tr><td><v-select id='AddMenuEnabledSelect' title='菜单级别' option='可编辑|可展示'></v-select></td></tr><tr><td><v-text id='AddMenuTitleText' title='菜单名称' maxlength='20' size='14'></v-text></td></tr><tr><td><v-text id='AddMenuUrlText' title='菜单路径' maxlength='128' size='14'></v-text></td></tr></table>", '添加菜单', function(flag){
			if (flag == 0) return true;
			
			var icon = getBackgroundImage(getCtrl('AddMenuIcon'));
			var enabled = $('#AddMenuEnabledSelect').val();
			var title = $('#AddMenuTitleText').val();
			var url = $('#AddMenuUrlText').val();
	
			enabled = (parseInt(enabled) ? 1 : 2);
		
			if ((len = strlen(title)) == 0){
				showToast('菜单名称不能为空');
				$('#AddMenuTitleText').focus();
				return false;
			}
			else if (len > 24){
				showToast('菜单名称最多12个汉字或24个英文字母');
				$('#AddMenuTitleText').focus();
				return false;
			}
			else if (!isFileName(title)){
				showToast('菜单名称不能包含特殊字符');
				$('#AddMenuTitleText').focus();
				return false;
			}
			else if ((titlelist.indexOf('|' + title + '|')) >= 0){
				showToast('菜单名称与现有菜单冲突');
				$('#AddMenuTitleText').focus();
				return false;
			}
			else if ((len = strlen(url)) == 0){
				showToast('菜单URL不能为空');
				$('#AddMenuUrlText').focus();
				return false;
			}
			else if (len > 128){
				showToast('菜单URL不能超过128个字符');
				$('#AddMenuUrlText').focus();
				return false;
			}

			param = {};
			param['url'] = url;
			param['icon'] = icon;
			param['title'] = title;
			param['enabled'] = enabled;
			param['folder'] = curfolder;
			
			return editMenuItem(param, 'A');
		});
		
		$('#AddMenuIcon').css('background-color', '#FFF').css('background-image', 'url(' + submenuicon + ')');
		$('#AddMenuTitleText').focus();
	});

	function loadMenuItem(flag){
		param = {};
		param['folder'] = curfolder;
		$('#MenuListTable').html('');
		$('#GroupListTable').html('');
		
		modifygroup = null;
		titlelist = '|';

		getHttpResult('/getmenulist', param, function(data){
			$('#EmptyMenuContentDiv').hide();

			if (strlen(data.enabled) == 0){
				folderenabled = 0;
			}
			else{
				folderenabled = data.enabled;
			}
				
			if (data.code == XG_TIMEOUT){
				sessionTimeout();
			}
			else if (data.code == XG_AUTHFAIL){
				showNoAccessToast();
			}
			else if (data.code < 0){
				showToast('加载数据失败');
			}
			else if (data.code == 0){
				enableMenuItem(1);

				curid = null;
				cururl = null;
				curtitle = null;
				curenabled = null;
				
				$('#MenuUrlText').val('');
				$('#MenuTitleText').val('');
				$('#MenuIcon').css('background-image', 'url(' + submenuicon + ')');

				if (folderenabled > 1) $('#DeleteMenuButton').removeAttr('disabled');

				$('#EmptyMenuContentDiv').show();
			}
			else{
				$.each(data.list, function(idx, item){
					titlelist += item.title + '|';
					addMenuItem(item.id, item.title, item.icon, item.enabled, item.statetime);
				});

				selectMenuItem($('.MenupadItem').first());
				
				$(".MenupadItem").each(function(){
					menubox.bind(this);
				});
			}
		});
		
		return flag;
	}
	function enableMenuItem(enabled){
		if (enabled == null || enabled < 2){
			$('#DeleteMenuButton').attr('disabled', true);
			$('#SaveMenuButton').attr('disabled', true);
			$('#MenuTitleText').attr('disabled', true);
			$('#MenuUrlText').attr('disabled', true);
		}
		else{
			$('#DeleteMenuButton').removeAttr('disabled');
			$('#SaveMenuButton').removeAttr('disabled');
			$('#MenuTitleText').removeAttr('disabled');
			$('#MenuUrlText').removeAttr('disabled');
		}
	}
	function addMenuItem(id, title, icon, enabled, statetime){
		$('#MenuListTable').append("<tr class='MenupadItem' id='Menu" + id + "' value='" + id + "' enabled='" + enabled + "'><td class='MenuIconButton' style='background-image:url(" + icon + ")'></td><td>" + title + "</td></tr>");
		$('.MenupadItem').click(function(){
			selectMenuItem($(this));
		});
	}

	function addGroupItem(id, name, icon, remark, checked){
		if (id == null){
			$('#GroupListTable').html('').append("<tr class='GroupItem'><td class='GroupIconTd'>授权</td><td class='GroupIconTd'>图标</td><td class='GroupIdTd'>角色编号</td><td>角色名称</td><td>角色说明</td></tr>");
		}
		else{
			$('#GroupListTable').append("<tr class='GroupItem'><td class='GroupIconTd' ><input id='GroupItem" + id + "' type='checkbox'/></td><td class='GroupIconTd'><img src='" + icon + "'/></td><td class='GroupIdTd'>" + id + "</td><td>" + name + "</td><td>" + remark + "</td></tr>");
			
			$('#GroupItem' + id).attr('checked', checked).click(function(){
				$.each(curgrouplist, function(idx, item){
					if (item.id == id){
						item.checked = item.checked ? false : true;
						modifygroup = true;
					}
				});
			});

			if (curenabled < 2 || editmenuaccess <= 0){
				$('#GroupItem' + id).attr('disabled', true);
			}
		}
	}

	function editMenuItem(param, flag){
		if (flag == 'A'){
			param['id'] = '';
		}

		if (flag == 'C' || flag == 'D' || flag == 'R'){
			param['title'] = '';
			param['url'] = '';
		}

		param['flag'] = getString(flag);
		
		if (modifygroup){
			param['grouplist'] = JSON.stringify(curgrouplist);
		}
		
		getHttpResult('/editmenu', param, function(data){
			if (data.code == XG_TIMEOUT){
				sessionTimeout();
			}
			else if (data.code == XG_AUTHFAIL){
				showNoAccessToast();
			}
			else if (data.code < 0){
				if (flag == 'A'){
					showToast('新建菜单失败');
				}
				else if (flag == 'D'){
					showToast('删除菜单失败');
				}
				else if (flag == 'U'){
					showToast('修改菜单失败');
				}
				else if (flag == 'R'){
					showToast('删除目录失败');
				}
				else{
					showToast('保存菜单失败');
				}
			}
			else{
				if (flag == 'R' || strlen(curfolder) == 0){
					selectMenu(curmenuitem, true);
				}
				else if (flag == 'U'){
					if (curtitle == param['title']){
						showToast('菜单保存成功');
					}
					else{
						showToast('菜单保存成功');

						var pos = titlelist.indexOf('|' + curtitle + '|');

						if (pos++ >= 0) titlelist = titlelist.substring(0, pos) + param['title'] + titlelist.substring(pos + curtitle.length);
					}
					
					curenabled = param['enabled'];
					curtitle = param['title'];
					curicon = param['icon'];
					cururl = param['url'];
					modifygroup = null;

					curmenupad.children('.MenuIconButton').css('background-image', 'url(' + curicon + ')');
					curmenupad.children().last().html(curtitle);
				}
				else{
					flag = loadMenuItem(flag);
				}
			}
		});
		
		return flag;
	}

	function checkMenuStatus(){
		if (curtitle == null) return false;

		var icon = getBackgroundImage(getCtrl('MenuIcon'));
		var enabled = $('#MenuEnabledSelect').val();
		var title = $('#MenuTitleText').val();
		var url = $('#MenuUrlText').val();
		
		enabled = (parseInt(enabled) ? 1 : 2);

		return (title == curtitle && url == cururl && icon == curicon && enabled == curenabled && modifygroup == null) ? false : true;
	}

	function selectMenuItem(menu){
		function loadMenuContent(){
			var id = menu.attr('value');

			$('.MenupadItem').css('background', 'none');
			menu.css('backgroundColor', 'rgba(0, 0, 0, 0.3)');
			curmenupad = menu;

			if (strlen(id) == 0){
				curmenupad = null;
				curfolder = null;
				curtitle = null;
				curicon = null;
				cururl = null;
				curid = null;
				selectMenu(curmenuitem, true);
			}
			else{
				$('#MenuTitleText').val('');

				param = {};
				param['id'] = getString(id);
				
				getHttpResult('/getmenucontent', param, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('加载数据失败');
					}
					else{
						$('#MenuEnabledSelect').val((parseInt(data.enabled) == 1) ? '1' : '0');
						$('#MenuIcon').css('background-image', 'url(' + data.icon + ')');
						$('#MenuTitleText').val(data.title);
						$('#MenuUrlText').val(data.url);
						
						modifygroup = null;

						curenabled = data.enabled;
						curtitle = data.title;
						curicon = data.icon;
						cururl = data.url;
						curid = data.id;
						
						addGroupItem();
						
						curgrouplist = [];
						
						$.each(data.grouplist, function(idx, item){
							addGroupItem(item.id, item.name, item.icon, item.remark, item.checked);
							curgrouplist[idx] = {id: item.id, checked: item.checked};
						});

						enableMenuItem(curenabled);
					}
				});
			}
		}
		
		if (checkMenuStatus()){
			showConfirmMessage('是否要保存[' + curtitle + ']菜单？', '保存菜单', function(flag){
				if (flag) $('#SaveMenuButton').click();

				loadMenuContent();
			});
		}
		else{
			loadMenuContent();
		}
	}

	$('#MenuTitleText').bind('input onpropertychange', function(){
		var title = $(this).val();

		if (strlen(title) == 0){
			enableMenuItem(1);
			showToast('菜单名称不能为空');
			$('#MenuTitleText').removeAttr('disabled').focus();
		}
		else if (title == curtitle){
			enableMenuItem(curenabled);
		}
		else{
			enableMenuItem(2);
		}

		if (curid == null){
			if (folderenabled > 1){
				$('#DeleteMenuButton').removeAttr('disabled');
			}
			else{
				$('#DeleteMenuButton').attr('disabled', true);
			}
		}
		else if (curenabled < 2){
			$('#DeleteMenuButton').attr('disabled', true);
		}
	});

	$('#DeleteMenuButton').click(function(){
		if (curid == null){
			showConfirmMessage('目录[' + curfolder + ']' + '无菜单信息。<br>是否决定删除该目录？', '删除目录', delMenuFolderResult);
		}
		else{
			param = {};
			param['id'] = getString(curid);
			showConfirmMessage('是否决定删除当前菜单？', '删除菜单', delMenuResult);
		}
	});

	$('#SaveMenuButton').click(function(){
		var icon = getBackgroundImage(getCtrl('MenuIcon'));
		var enabled = $('#MenuEnabledSelect').val();
		var title = $('#MenuTitleText').val();
		var url = $('#MenuUrlText').val();
		
		enabled = (parseInt(enabled) ? 1 : 2);
		
		param = {};

		param['id'] = getString(curid);
		param['url'] = getString(url);
		param['icon'] = getString(icon);
		param['title'] = getString(title);
		param['enabled'] = getString(enabled);
		param['folder'] = getString(curfolder);

		if ((len = strlen(title)) == 0){
			showToast('菜单名称不能为空');
			$('#MenuTitleText').focus();
		}
		else if (len > 24){
			showToast('菜单名称最多12个汉字或24个英文字母');
			$('#MenuTitleText').focus();
		}
		else if ((len = strlen(url)) == 0){
			showToast('菜单URL不能为空');
			$('#MenuUrlText').focus();
		}
		else if (len > 128){
			showToast('菜单URL不能超过128个字符');
			$('#MenuUrlText').focus();
		}
		else{
			if (title == curtitle){
				if (url == cururl && icon == curicon && enabled == curenabled && modifygroup == null){
					showToast('菜单内容未更新');
				}
				else{
					editMenuItem(param, 'U');
				}
			}
			else{
				if (strlen(titlelist) <= 1){
					editMenuItem(param, 'A');
				}
				else if ((titlelist.indexOf('|' + title + '|')) >= 0){
					showToast('菜单名称与现有菜单冲突');
					$('#MenuTitleText').focus();
				}
				else{
					editMenuItem(param, 'U');
				}
			}
			
			setSaveNeeded(null);
		}
	});

	loadMenuItem();

	setSingletonInterval('menu_check_timer', 100, function(){
		if (checkMenuStatus()){
			setSaveNeeded(function(func){
				showConfirmMessage('是否要保存['+ curtitle + ']菜单？', '保存菜单', function(flag){
					if (flag) $('#SaveMenuButton').click();
					if (func) func();
				});

				setSaveNeeded(null);
			});
		}
	});
}
</script>