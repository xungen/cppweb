#include <webx/route.h>
#include <webx/route.h>
#include <dbentity/T_XG_CONF.h>

class EditNote : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(EditNote, "/confile/${filename}")

int EditNote::process()
{
	param_string(id);
	param_string(flag);
	param_string(icon);
	param_string(level);
	param_string(title);
	param_string(folder);
	param_string(remark);
	param_string(content);
	param_string(deficon);

	if (deficon.empty()) deficon = "/res/img/note/code.png";

	checkLogin();

	int res = 0;
	string sqlcmd;
	CT_XG_CONF tab;
	string user = folder == "WEBPAGE" ? "system" : token->getUser();
	sp<DBConnect> dbconn = webx::GetDBConnect(ctx, token->getDataId());

	tab.init(dbconn);
	tab.id = id;

	if (flag == "A" || flag == "C" || flag == "R")
	{
		webx::CheckFileName(title, 0);
		webx::CheckFileName(folder, 0);

		if ((flag == "A" && title.empty()) || (flag == "C" && folder == "WEBPAGE")) throw Exception(XG_PARAMERR);

		if (icon.empty())
		{
			if (flag == "C")
			{
				icon = "/res/img/menu/folder.png";
			}
			else
			{
				icon = deficon;
			}
		}

		if (flag == "R")
		{
			res = tab.removeWhere("USER=? AND FOLDER=?", user, folder);
		}
		else
		{
			sp<RowData> row;
			sp<QueryResult> rs;

			sqlcmd = "SELECT COUNT(*) FROM T_XG_CONF WHERE USER=?";

			if (!(rs = dbconn->query(sqlcmd, user))) throw Exception(XG_SYSERR);

			if ((row = rs->next()) && row->getInt(0) > 1000) throw Exception(XG_AUTHFAIL);

			if (level.empty()) level = "1";

			tab.user = user;
			tab.icon = icon;
			tab.level = level;
			tab.title = title;
			tab.folder = folder;
			tab.remark = remark;
			tab.content = content;
			tab.statetime.update();

			for (int i = 0; i < 5; i++)
			{
				tab.id = DateTime::GetBizId();
				tab.position = (int)(time(NULL));

				if ((res = tab.insert()) >= 0) break;
			}

			if (res > 0)
			{
				json["statetime"] = tab.statetime.toString();
				json["icon"] = tab.icon.toString();
				json["id"] = tab.id.val();
			}
		}
	}
	else if (flag == "D")
	{
		res = XG_NOTFOUND;

		if (tab.find() && tab.next() && user == tab.user.val())
		{
			res = tab.remove();
		}
	}
	else if (flag == "U")
	{
		res = XG_NOTFOUND;

		if (tab.find() && tab.next() && user == tab.user.val() && folder == tab.folder.val())
		{
			if (icon.length() > 0) tab.icon = icon;
			if (level.length() > 0) tab.level = level;
			if (title.length() > 0) tab.title = title;
			if (remark.length() > 0) tab.remark = remark;

			if (tab.position.val() == 0) tab.position = (int)(time(NULL));

			if (content.empty() > 0)
			{
				content = tab.content.toString();
			}
			else
			{
				tab.content = content;
			}
			
			tab.statetime.update();

			if ((res = tab.update()) < 0) throw Exception(res);

			string tmp = tab.icon.toString();
			
			if (tmp.length() < 128) json["icon"] = tmp;
		}
	}
	else if (flag == "M")
	{
		int position = 0;
		vector<string> vec = stdx::split(folder, ",");

		if (vec.empty()) throw Exception(XG_PARAMERR);

		dbconn->begin();

		if (vec.size() > 1)
		{
			const char* sql = "UPDATE T_XG_CONF SET POSITION=? WHERE FOLDER=? AND (TITLE IS NULL OR TITLE='')";

			for (string& folder : vec)
			{
				dbconn->execute(sql, ++position, folder);
			}
		}
		else
		{
			vec = stdx::split(title, ",");

			const char* sql = "UPDATE T_XG_CONF SET POSITION=? WHERE FOLDER=? AND TITLE=?";

			for (string& title : vec)
			{
				dbconn->execute(sql, ++position, folder, title);
			}
		}

		dbconn->commit();

		res = vec.size();
	}
	else
	{
		res = XG_PARAMERR;
	}

	if (res >= 0) webx::RemoveConfileCache(stdx::EmptyString());

	json["code"] = res;
	out << json;

	return XG_OK;
}