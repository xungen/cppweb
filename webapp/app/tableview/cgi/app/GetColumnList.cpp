#include <webx/route.h>

namespace
{
	JsonEntity(Column)
	{
	public:
		rstring(name);
		rstring(title);
		rstring(filter);
		rstring(remark);
		rint(enabled);
		rint(position);
		rint(querytype);
	};

	JsonEntity(Request)
	{
	public:
		nstring(flag);
		rstring(tabid);
	};

	JsonEntity(Response)
	{
	public:
		rint(code);
		rarray(Column, list);
	};

	void dowork(sp<Context> ctx, Request& req, Response& rsp)
	{
		vector<Column> vec;

		if (req.flag == "A")
		{
			webx::GetDBConnect(ctx)->selectList(vec, "SELECT * FROM T_XG_TABCOLS WHERE TABID=? ORDER BY POSITION ASC", req.tabid);

			for (Column& item : vec)
			{
				rsp.list.add(newsp<Column>(item));
			}
		}
		else
		{
			webx::GetDBConnect(ctx)->selectList(vec, "SELECT * FROM T_XG_TABCOLS WHERE TABID=? AND ENABLED>0 ORDER BY POSITION ASC", req.tabid);

			for (Column& item : vec)
			{
				if (item.title.empty()) item.title = item.name;

				rsp.list.add(newsp<Column>(item));
			}
		}

		rsp.code = vec.size();
	}

	HTTP_WEBAPI(CGI_PUBLIC, "${filename}", {
		Request req;
		Response rsp;

		parse(req);

		dowork(ctx, req, rsp);

		out << rsp.toString();

		return XG_OK;
	})
}