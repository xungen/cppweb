#include <webx/route.h>

class GetIconList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetIconList)

int GetIconList::process()
{
	param_string(path);

	int res = 0;
	string urlpath;
	string filepath;
	vector<string> vec;
	const CgiMapData& cfg = app->getCgiMapData("/res/img/logo.png");

	urlpath = "/res/img/";

	if (cfg.url.empty())
	{
		filepath = app->getPath() + urlpath.substr(1);
	}
	else
	{
		filepath = path::parent(cfg.url);
	}

	if (path.length() > 0)
	{
		webx::CheckFilePath(path, 0);

		urlpath += "/" + path + "/";
		filepath += "/" + path + "/";
		
		urlpath = stdx::replace(urlpath, "\\", "/");
		urlpath = stdx::replace(urlpath, "//", "/");
	}

	stdx::GetFolderContent(vec, filepath, eFILE);

	JsonElement arr = json.addArray("list");

	for (string& item : vec)
	{
		arr[res++] = urlpath + item;
	}

	json["code"] = res;
	out << json;

	return XG_OK;
}