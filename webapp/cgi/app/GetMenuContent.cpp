#include <webx/route.h>
#include <dbentity/T_XG_MENU.h>
#include <dbentity/T_XG_GROUP.h>

class GetMenuContent : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetMenuContent)

int GetMenuContent::process()
{
	param_seqno_string(id);

	sp<QueryResult> rs;
	int res = XG_SYSERR;
	sp<DBConnect> dbconn = webx::GetDBConnect(ctx);
	string sqlcmd = "SELECT ID,URL,ICON,TITLE,ENABLED,STATETIME FROM T_XG_MENU WHERE ID=?";

	if (rs = dbconn->query(sqlcmd, id))
	{
		sp<RowData> row;

		while (row = rs->next())
		{
			json["id"] = row->getString(0);
			json["url"] = row->getString(1);
			json["icon"] = row->getString(2);
			json["title"] = row->getString(3);
			json["enabled"] = row->getString(4);
			json["statetime"] = row->getString(5);
		}
		
		sqlcmd = "SELECT ID,NAME,ICON,REMARK,ENABLED,MENULIST FROM T_XG_GROUP ORDER BY ID";

		if (rs = dbconn->query(sqlcmd))
		{
			res = 0;

			string tag = "," + id + ",";
			
			while (row = rs->next())
			{
				string group = "," + row->getString(5) + ",";
				JsonElement item = json["grouplist"][res++];

				item["id"] = row->getString(0);
				item["name"] = row->getString(1);
				item["icon"] = row->getString(2);
				item["remark"] = row->getString(3);
				item["enabled"] = row->getString(4);
				item["checked"] = group.find(tag) == string::npos ? false : true;
			}
		}
	}

	json["code"] = res;
	out << json;
	
	return XG_OK;
}