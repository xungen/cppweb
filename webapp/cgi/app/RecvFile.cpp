#include <webx/route.h>

class RecvFile : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(RecvFile)

int RecvFile::process()
{
	SmartBuffer buffer;
	vector<webx::FileParamItem> vec;
	int res = webx::GetFileParamList(vec, buffer, app, request, response);

	if (res == XG_NETERR || res == XG_TIMEOUT) return XG_NETERR;

	if (res < 0) throw Exception(res);

	res = 0;

	JsonElement data = json["url"];
	const string& path = app->getPath();

	for (auto& item : vec)
	{
		data[res++] = item.filepath.substr(path.length() - 1);
	}

	json["error"] = (int)(0);
	json["code"] = res;
	out << json;
	
	return XG_OK;
}

struct RecvFileSetup
{
	RecvFileSetup()
	{
		path::mkdir(HttpServer::Instance()->getPath() + "/dat/pub");
	}
};

static RecvFileSetup setup;