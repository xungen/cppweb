#include <webx/route.h>
#include <dbentity/T_XG_NOTE.h>

class GetNoteContent : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetNoteContent)

int GetNoteContent::process()
{
	param_seqno_string(id);

	string sqlcmd;
	const string& user = checkLogin();
	sp<DBConnect> dbconn = webx::GetDBConnect(ctx, token->getDataId());
	
	stdx::format(sqlcmd, "SELECT ID,TYPE,USER,ICON,LEVEL,TITLE,FOLDER,CONTENT,STATETIME FROM T_XG_NOTE WHERE (USER=? OR LEVEL>2) AND ID=?");

	sp<QueryResult> rs = dbconn->query(sqlcmd, user, id);

	if (!rs) throw Exception(XG_SYSERR);

	sp<RowData> row = rs->next();

	if (!row) throw Exception(XG_NOTFOUND);

	string author = row->getString(2);

	json["id"] = row->getString(0);
	json["type"] = row->getString(1);
	json["icon"] = row->getString(3);
	json["level"] = row->getString(4);
	json["title"] = row->getString(5);
	json["folder"] = row->getString(6);
	json["content"] = row->getString(7);
	json["statetime"] = row->getString(8);

	if (user == author)
	{
		CT_XG_NOTE tab;

		tab.init(dbconn);

		tab.id = id;
		tab.statetime.update();

		tab.update();
	}
	
	json["code"] = XG_OK;
	out << json;
	
	return XG_OK;
}