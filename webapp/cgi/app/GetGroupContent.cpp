#include <webx/route.h>

class GetGroupContent : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetGroupContent)

int GetGroupContent::process()
{
	param_string(id);

	checkLogin();

	sp<DBConnect> dbconn = webx::GetDBConnect(ctx);
	string sqlcmd = "SELECT * FROM T_XG_GROUP WHERE ID=?";

	json["code"] = webx::PackJson(dbconn->query(sqlcmd, id), json);
	out << json;

	return XG_OK;
}