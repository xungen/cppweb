#include <webx/route.h>

class GetAccessList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetAccessList)

int GetAccessList::process()
{
	checkLogin();

	string sqlcmd;
	sp<DBConnect> dbconn = webx::GetDBConnect(ctx);

	stdx::format(sqlcmd, "SELECT A.*,B.ICON,B.FOLDER FROM T_XG_ACCESS A,T_XG_MENU B WHERE A.MENUID=B.ID ORDER BY B.POSITION ASC");

	json["code"] = webx::PackJson(dbconn->query(sqlcmd), "list", json);
	out << json;

	return XG_OK;
}