#include <webx/route.h>

class GetGroupList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetGroupList)

int GetGroupList::process()
{
	checkLogin();

	sp<DBConnect> dbconn = webx::GetDBConnect(ctx);
	string sqlcmd = "SELECT * FROM T_XG_GROUP ORDER BY ID ASC";

	json["code"] = webx::PackJson(dbconn->query(sqlcmd), "list", json);
	out << json;

	return XG_OK;
}