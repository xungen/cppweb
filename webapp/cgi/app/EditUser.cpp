#include <webx/route.h>
#include <dbentity/T_XG_USER.h>
#include <dbentity/T_XG_NOTE.h>
#include <dbentity/T_XG_CODE.h>
#include <dbentity/T_XG_DBETC.h>
#include <dbentity/T_XG_PARAM.h>

class EditUser : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(EditUser)

int EditUser::process()
{
	param_string(flag);
	param_string(user);
	param_string(name);

	webx::CheckFileName(user, 0);
	webx::CheckFileName(name, 0);

	auto GetDataId = [](sp<DBConnect> dbconn){
		string dbid;
		string sqlcmd = "SELECT ID FROM T_XG_DBETC WHERE ENABLED>1 ORDER BY STRESS ASC LIMIT 1";

		if (dbconn->select(dbid, sqlcmd) < 0) throw Exception(XG_SYSERR);

		return dbid;
	};
	
	auto UpdateDataStress = [](sp<DBConnect> dbconn, const string& dbid){
		if (dbid.length() > 0) dbconn->execute("UPDATE T_XG_DBETC SET STRESS=STRESS+STEP WHERE ID=?", dbid);
	};

	param_string(mail);
	param_string(code);
	param_string(password);

	stdx::tolower(mail);
	stdx::tolower(code);

	int res = 0;
	string sqlcmd;
	CT_XG_USER tab;
	sp<DBConnect> dbconn = webx::GetDBConnect(ctx);

	tab.init(dbconn);

	if (flag == "R")
	{
		checkSession();

		if (user.empty() || password.empty() || mail.find('@') == string::npos) throw Exception(XG_PARAMERR);

		if (mail != token->getMail() || code != token->getExtdata("code"))
		{
			throw Exception(token->checkTimes() > 0 ? XG_PARAMERR : XG_AUTHFAIL);
		}

		string dbid = GetDataId(dbconn);

		if (dbid.empty()) throw Exception(XG_SYSERR);

		if (name.empty()) name = mail.substr(0, mail.find('@'));

		tab.dbid = dbid;
		tab.user = user;
		tab.name = name;
		tab.mail = mail;
		tab.password = password;
		tab.level = "0";
		tab.enabled = "1";
		tab.language = "CN";
		tab.grouplist = "normal";
		tab.statetime.update();
		
		if ((res = tab.insert()) < 0) throw Exception(res);

		LogTrace(eIMP, "create user[" + user + "] success");

		UpdateDataStress(dbconn, dbid);
		webx::InitNewUser(user);
		token->update(tab);
	}
	else if (flag == "P")
	{
		checkSession();

		if (password.empty() || mail != token->getMail() || code != token->getExtdata("code"))
		{
			throw Exception(token->checkTimes() > 0 ? XG_PARAMERR : XG_AUTHFAIL);
		}

		token->disable();
		tab.password = password;
		res = tab.updateWhere(false, "MAIL=?", mail);
	}
	else
	{
		if (user.empty()) throw Exception(XG_PARAMERR);

		checkLogin();

		tab.user = user;

		if (flag == "S")
		{
			if (user == "root" && token->getUser() != "root") throw Exception(XG_AUTHFAIL);

			if (password.empty()) throw Exception(XG_PARAMERR);

			tab.password = password;

			res = tab.update();
		}
		else if (flag == "A" || flag == "U" || flag == "M")
		{
			string dbid;

			param_int(force);
			param_string(icon);
			param_string(mail);
			param_string(cert);
			param_string(addr);
			param_string(lang);
			param_string(phone);
			param_string(group);
			param_string(password);
			param_string(remark);
			param_string(enabled);

			stdx::tolower(mail);

			if (flag == "A")
			{
				if (name.empty() || mail.empty()) throw Exception(XG_PARAMERR);

				if (lang.empty()) lang = "CN";
				if (group.empty()) group = "normal";
				if (enabled.empty()) enabled = "0";

				if (tab.find() && tab.next()) throw Exception(XG_DUPLICATE);

				dbid = GetDataId(dbconn);

				tab.statetime.update();
				tab.level = 0;		
			}
			else if (flag == "U")
			{
				if (user == "root" || user == "system") throw Exception(XG_AUTHFAIL);
			}
			else
			{
				if (user != token->getUser()) throw Exception(XG_AUTHFAIL);

				enabled.clear();
				group.clear();
				dbid.clear();

				force = 0;
			}

			if (force > 0 || dbid.length() > 0) tab.dbid = dbid;
			if (force > 0 || name.length() > 0) tab.name = name;
			if (force > 0 || mail.length() > 0) tab.mail = mail;
			if (force > 0 || icon.length() > 0) tab.icon = icon;
			if (force > 0 || cert.length() > 0) tab.certno = cert;
			if (force > 0 || addr.length() > 0) tab.address = addr;
			if (force > 0 || phone.length() > 0) tab.phone = phone;
			if (force > 0 || lang.length() > 0) tab.language = lang;
			if (force > 0 || group.length() > 0) tab.grouplist = group;
			if (force > 0 || remark.length() > 0) tab.remark = remark;
			if (force > 0 || enabled.length() > 0) tab.enabled = enabled;
			if (force > 0 || password.length() > 0) tab.password = password;

			if (flag == "A")
			{
				if ((res = tab.insert()) < 0)  throw Exception(res);

				UpdateDataStress(dbconn, dbid);
				webx::InitNewUser(user);
			}
			else
			{
				if ((res = tab.update()) < 0) throw Exception(res);

				if (user == token->getUser() && tab.find() && tab.next()) token->update(tab);
			}
		}
		else if (flag == "D")
		{
			if (user == "root" || user == "system") throw Exception(XG_AUTHFAIL);

			res = tab.remove();
		}
		else
		{
			res = XG_PARAMERR;
		}
	}

	json["code"] = res;
	out << json;
	
	return XG_OK;
}