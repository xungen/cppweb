#include <webx/route.h>

class GetUserList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetUserList)

int GetUserList::process()
{
	param_string(user);

	bool sysright = false;
	vector<string> vec = stdx::split(user, ",");

	if (vec.empty()) throw Exception(XG_PARAMERR);

	try
	{
		webx::CheckSystemRight(this);

		sysright = true;
	}
	catch(Exception e)
	{
		sysright = false;
	}

	CT_XG_USER tab;
	ParamVector param;

	tab.init(webx::GetDBConnect(ctx));

	for (const string& user : vec) param.add(user);

	if (!tab.findWhere("USER IN (" + param.placeholders() + ")", param)) throw Exception(XG_SYSERR);

	int res = 0;
	JsonElement list = json.addArray("list");

	while (tab.next())
	{
		JsonElement item = list[res++];

		item["user"] = tab.user.val();
		item["name"] = tab.name.val();

		if (tab.icon.val().empty())
		{
			item["icon"] = "/res/img/user/user.png";
		}
		else
		{
			item["icon"] = tab.icon.val();
		}

		if (sysright)
		{
			item["dbid"] = tab.dbid.val();
			item["level"] = tab.level.val();
			item["phone"] = tab.phone.val();
		}
	}

	json["code"] = res;
	out << json;

	return XG_OK;
}