#include <webx/route.h>
#include <webx/route.h>

class GetBatchContent : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetBatchContent, "${filename}")

int GetBatchContent::process()
{
	param_string(path);

	int res = 0;
	string link;
	string param;
	JsonElement arr = json.addArray("list");
	vector<string> vec = stdx::split(path, ",");
	string cookie = request->getHeadValue("Cookie");

	for (string& path : vec)
	{
		if (path.empty()) continue;

		size_t pos = path.find('?');

		if (pos == string::npos)
		{
			link = path;
			param.clear();
		}
		else
		{
			link = path.substr(0, pos);
			param = path.substr(pos + 1);
		}

		JsonElement item = arr[res++];
		SmartBuffer data = webx::GetRemoteResult(ctx, link, param, stdx::EmptyString(), cookie);

		item["path"] = path;
		item["status"] = webx::GetLastRemoteStatus();

		if (data.isNull())
		{
			item["content"] = stdx::EmptyString();
		}
		else
		{
			item["content"] = data.str();
		}
	}

	json["code"] = res;
	out << json;

	return XG_OK;
}