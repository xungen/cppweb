#include <webx/route.h>
#include <webx/route.h>

class Broadcast : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(Broadcast)

int Broadcast::process()
{
	param_string(path);
	param_string(param);

	if (path.empty()) throw Exception(XG_PARAMERR);

	webx::CheckSystemRight(this);

	return simpleResponse(webx::Broadcast(path, param));
}