import imp;

def update(py):
	return imp.reload(py);
	
def getcgimap():
	cgimap = {};

	from pyc import pythontest;
	cgimap['pythontest'] = lambda app: update(pythontest).main(app);

	return cgimap;