var styleloadmap = {};
var scriptloadmap = {};
var autoresizemap = {};
var automouseupmap = {};
var mousemovevent = null;
var contextmenuview = null;
var contextmenuattach = null;
var defaultcontextmenu = document.oncontextmenu;

$.extend({
	pack: function(elem){
		if (isObject(elem)){
			if (isFunction(elem.attr)) return elem;
		}
		else{
			elem = getString(elem);
			var ch = elem.charAt(0);
			if (ch == '_') return $('#' + elem);
			if (ch >= 'a' && ch <= 'z') return $('#' + elem);
			if (ch >= 'A' && ch <= 'Z') return $('#' + elem);
			if (ch >= '0' && ch <= '9') return $('#' + elem);
		}

		return $(elem);
	}
});

function getClientWidth(){
	return document.documentElement.clientWidth || document.body.clientWidth;
}

function getClientHeight(){
	return document.documentElement.clientHeight || document.body.clientHeight;
}


function getCtrl(elem){
	return $.pack(elem)[0];
}

function appendCtrl(elem){
	var ctrl = getCtrl(elem);
	if (getCtrl(ctrl.id) == null) document.body.appendChild(ctrl);
	return getCtrl(ctrl.id);
}

function createCtrl(tag, id, clazz, text, html){
	var ctrl = null;
	if (id != null) {
		ctrl = getCtrl(id);
	}
	if (ctrl == null){
		ctrl = document.createElement(tag);
		if (isArray(clazz)) {
			clazz.forEach(function(item) {
				ctrl.classList.add(item);
			});
		} else if (strlen(clazz) > 0) {
			ctrl.classList.add(clazz);
		}
		if (strlen(text) > 0) {
			ctrl.innerText = text;
		}
		if (strlen(html) > 0) {
			ctrl.innerHTML = html;
		}
		if (strlen(id) > 0) {
			ctrl.id = id;
		}
	}
	return ctrl;
}

function getTop(elem){
	var rect = getCtrl(elem).getBoundingClientRect(); 
	return rect.top - document.documentElement.clientTop; 
}

function getLeft(elem){
	var rect = getCtrl(elem).getBoundingClientRect(); 
	return rect.left - document.documentElement.clientLeft; 
}

function getWidth(elem){
	return $.pack(elem).width();
}

function getHeight(elem){
	return $.pack(elem).height();
}

function setWidth(elem, width){
	return $.pack(elem).width(width);
}

function setHeight(elem, height){
	return $.pack(elem).height(height);
}

function getScrollTop(){ 
    var top = 0;
	
    if(document.documentElement && document.documentElement.scrollTop){
        top = document.documentElement.scrollTop;
    }
	else if(document.body){
        top = document.body.scrollTop;
    }

    return top;   
}

function centerWindow(elem, ox, oy){
	var wnd = getCtrl(elem);
	var x = (getClientWidth() - wnd.offsetWidth) / 2;
	var y = (getClientHeight() - wnd.offsetHeight) / 2;
	if (ox) x += ox;
	if (oy) y += oy;
	if (y < 8) y = 8;
	if (y > 200) y -= y / 8;

	wnd.style.left = x + 'px';
	wnd.style.top = getScrollTop() + y + 'px';
}

function removeResizeCallback(name){
	delete autoresizemap[name];
}

function setResizeCallback(name, func){
	autoresizemap[name] = func;
}

function removeMouseupCallback(name){
	delete automouseupmap[name];
}

function setMouseupCallback(name, func){
	automouseupmap[name] = func;
}

function centerText(text, weight){
	if (weight) return "<div style='text-align:center;font-weight:" + weight + "'>" + text + "</div>";
	else return "<div style='text-align:center'>" + text + "</div>";
}

function loadStyle(url){
	var elem = styleloadmap[url];
	if (elem) return elem;
	elem = document.createElement('link');
	elem.href = url;
	elem.type = 'text/css';
	elem.rel = 'stylesheet';
	document.getElementsByTagName('head')[0].appendChild(elem);
	return styleloadmap[url] = elem;
}

function loadScript(url, async){
	var elem = scriptloadmap[url];
	if (elem) return elem;
	elem = document.createElement('script');
	elem.type = 'text/javascript';
	if (async){
		elem.src = url;
	}
	else{
		$(elem).html(getHttpResult(url));
	}
	document.getElementsByTagName('head')[0].appendChild(elem);
	return scriptloadmap[url] = elem;
}

function dispatch(elem, name){
	var event = document.createEvent('Event');
	event.initEvent(name, true, true);
	getCtrl(elem).dispatchEvent(event);
}

function setLabelText(elem, text){
	$.pack(elem).attr('size', getTextSize(text)).val(text);
}

function getCursorLocation(){
	if (mousemovevent == null){
		document.onmousemove = function(e){
			mousemovevent = e || window.event;
		}

		return null;
	}

	var ev = mousemovevent;
	
	if (ev.pageX || ev.pageY){
		return {x: ev.pageX, y: ev.pageY};
	}
	
	return {
		x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y: ev.clientY + document.body.scrollTop - document.body.clientTop
	};
}

function getBackgroundImage(elem){
	var icon = $.pack(elem).css('background-image');

	if (icon.indexOf('/res') >= 0){
		icon = icon.substring(icon.indexOf('/res'));
	}
	else if (icon.indexOf('/app') >= 0){
		icon = icon.substring(icon.indexOf('/app'));
	}
	else if (icon.indexOf('\'') >= 0){
		icon = icon.substring(icon.indexOf('\'') + 1);
	}
	else if (icon.indexOf('\"') >= 0){
		icon = icon.substring(icon.indexOf('\"') + 1);
	}
	
	icon = icon.substring(0, icon.lastIndexOf(')')) || icon;
	icon = icon.substring(0, icon.lastIndexOf('\'')) || icon;
	icon = icon.substring(0, icon.lastIndexOf('\"')) || icon;
	
	return icon;
}

function graspCanvas(elem, scale, func){
	elem = getCtrl(elem);

	if (scale == null) scale = 1;

	var canvas = document.createElement('canvas');
	var width = elem.offsetWidth;
	var height = elem.offsetHeight;
	var context = canvas.getContext('2d');

	canvas.width = width * scale;
	canvas.height = height * scale;
	context.scale(scale, scale);

	var opts = {
		dpi: window.devicePixelRatio * scale,
		scale: 1,
		canvas: canvas,
		useCORS: true
	};

	html2canvas(elem, opts).then(func);
}

function setFrameStyle(top, side, main, bottom, topheight, sidewidth, bottomheight, margin){
	top = getCtrl(top);
	side = getCtrl(side);
	main = getCtrl(main);
	bottom = getCtrl(bottom);
	
	if (margin == null || margin < 0) margin = 0;
	if (topheight == null || topheight < 0) topheight = 0;
	if (sidewidth == null || sidewidth < 0) sidewidth = 0;
	if (bottomheight == null || bottomheight < 0) bottomheight = 0;

	top.style.position = 'absolute';
	top.style.top = margin + 'px';
	top.style.left = margin + 'px';
	top.style.right = margin + 'px';
	top.style.height = (topheight + 1)+ 'px';

	if (topheight > 0){
		top.style.display = 'block';
	}
	else{
		top.style.display = 'none';
	}

	side.style.overflow = 'auto';
	side.style.position = 'absolute';
	side.style.top = (topheight + margin + margin) + 'px';
	side.style.left = top.style.left;
	side.style.width = sidewidth + 'px';
	side.style.bottom = (bottomheight + margin + margin) + 'px';

	if (sidewidth > 0){
		side.style.display = 'block';
	}
	else
	{
		side.style.display = 'none';
	}

	main.style.overflow = 'auto';
	main.style.position = 'absolute';
	main.style.top = side.style.top;
	main.style.left = (sidewidth + margin + margin) + 'px';
	main.style.right = top.style.right;
	main.style.bottom = side.style.bottom;

	bottom.style.position = 'absolute';
	bottom.style.left = top.style.left;
	bottom.style.right = top.style.right;
	bottom.style.bottom = margin + 'px';
	bottom.style.height = bottomheight + 'px';

	if (bottomheight > 0){
		bottom.style.display = 'block';
	}
	else{
		bottom.style.display = 'none';
	}	
}

function getContextMenuAttach(){
	return contextmenuattach;
}

function bindContextMenu(elem, menu){
	let view = getCtrl(menu);
	let attach = getCtrl(elem);

	view.onclick = function(){
		document.oncontextmenu = defaultcontextmenu;
		view.style.display = 'none';
	}

	view.onclick();

	attach.onmouseup = function(e){
		e.stopPropagation();

		if (e.button == 2){
			document.oncontextmenu = function(e){
				e.preventDefault();
			}

			var x = e.clientX - 1;
			var y = e.clientY - 1;

			view.style.cssText = 'z-index:10000;position:fixed;display:block;left:' + x + 'px;' + 'top:' + y + 'px;';
			contextmenuattach = elem;
			contextmenuview = menu;
		}
	}
}

registInit(function(){
	window.onresize = function(){
		for (var id in autoresizemap){
			var func = autoresizemap[id];
			if (isFunction(func)) func();
		}
	};

	document.onmouseup = function(){
		var display = $('#XG_TOAST_MODESCREEN_DIV_ID').css('display');
		
		if (display == null || display == 'none') $('#XG_TOAST_DIV_ID').fadeOut();

		if (contextmenuview){
			document.oncontextmenu = defaultcontextmenu;
			$.pack(contextmenuview).hide();
			contextmenuview = null;
		}

		for (var id in automouseupmap){
			var func = automouseupmap[id];
			if (isFunction(func)) func();
		}
	};
});