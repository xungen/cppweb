var XG_OK = 1;
var XG_FAIL = 0;
var XG_ERROR = -1;
var XG_IOERR = -2;
var XG_SYSERR = -3;
var XG_NETERR = -4;
var XG_TIMEOUT = -5;
var XG_DATAERR = -6;
var XG_SYSBUSY = -7;
var XG_PARAMERR = -8;
var XG_NOTFOUND = -9;
var XG_NETCLOSE = -10;
var XG_NETDELAY = -11;
var XG_SENDFAIL = -12;
var XG_RECVFAIL = -13;
var XG_AUTHFAIL = -14;
var XG_DAYLIMIT = -15;
var XG_DUPLICATE = -16;
var XG_UNINSTALL = -17;
var XG_NOTCHANGED = -18;
var XG_DETACHCONN = -999999;

function checkHttpResult(data, title, quiet){
	var res = false;

	if (strlen(title) <= 0){
		title = translate('process');
	}

	if (isObject(data)){
		if (data.$code == 401){
			sessionTimeout();
			return false;
		}

		if (data.$code == 403){
			showNoAccessToast();
			return false;
		}

		if (typeof(data.code) == 'undefined'){
			res = data.$code == 200;
		}
		else{
			if (data.code == XG_TIMEOUT){
				sessionTimeout();
				return false;
			}

			if (data.code == XG_AUTHFAIL){
				showNoAccessToast();
				return false;
			}

			if (data.code == XG_DAYLIMIT){
				showErrorToast(translate('operate frequently'));
				return false;
			}

			res = data.code >= 0;
		}
	}

	if (res){
		if (quiet) return res;

		showToast(title + translate('success', EnSpaceFront));
	}
	else{
		showErrorToast(title + translate('failed', EnSpaceFront));
	}

	return res;
}

function getHttpResult(url, data, func, async, json, cache){
	var result = {code: XG_NETERR};
	var contype = 'application/x-www-form-urlencoded';

	if (json){
		contype = 'application/json';
		data = JSON.stringify(data);
	}

	if (cache == null){
		var ext = getFileExtname(url).toLowerCase();
		if (ext == 'js' || ext == 'css' || ext == 'htm' || ext == 'html' || ext == 'gzip'){
			cache = true;
		}
		else{
			cache = false;
		}
	}

	$.ajax({
		url: url,
		data: data,
		cache: cache,
		contentType: contype,
		type: data ? 'POST' : 'GET',
		async: async ? true : false,
		success: function(msg){
			try{
				result = JSON.parse(msg);
				result['$desc'] = 'OK';
				result['$code'] = 200;
			}
			catch(e){
				result = msg;
			}

			if (func) func(result, msg, 200, 'OK');
		},
		error: function(data, message){
			try{
				result = JSON.parse(data.responseText);
				result['$desc'] = data.statusText;
				result['$code'] = data.status;
			}
			catch(e){
				result = data.responseText;
			}

			if (func) func(result, data.responseText, data.status, data.statusText);
		}
	});

	return async ? true : result;
}

function getAccess(path){
	var arr = [];
	var flag = isArray(path);

	if (flag){
		for (var i = 0; i < path.length; i++) arr.push(0);
	}
	else{
		arr.push(0);
	}

	getHttpResult('/checkaccess', {path: flag ? path.join(',') : path}, function(data){
		if (data.code > 0){
			for (var i = 0; i < data.list.length; i++){
				arr[i] = data.list[i].access;
			}
		}
	});

	return flag ? arr : arr[0];
}

function getAccessMap(path){
	var map = {};
	var arr = getAccess(path);

	if (isArray(path)){
		for (var i = 0; i < path.length; i++){
			map[path[i]] = arr[i];
		}
	}
	else{
		map[path] = arr;
	}

	return map;
}

function getIconList(path){
	var iconlist = [];

	getHttpResult('/geticonlist', {path: getString(path)}, function(data){
		if (data.code > 0) iconlist = data.list;
	});

	return iconlist;
}