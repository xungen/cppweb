﻿var commonfilter = {
	name: function(text){
		return new RegExp(/^\w+$/).test(text) ? text : '';
	}
};

var getSequence = ((
	function(){
		var id = 0; 
		return function(){
			return ++id;
		};
	}
)());

Array.prototype.unique = function(){
	return Array.from(new Set(this));
}

Array.prototype.index = function(obj){
	for (var i = 0; i < this.length; i++){
		if (obj == this[i]) return i;
	}
	return -1;
}

Array.prototype.pushFront = function(){
	for (var i = 0; i < arguments.length; i++) this.splice(i, 0, arguments[i]);
}

Array.prototype.popFront = function(count){
	if (typeof(count) === 'undefined') this.splice(0, 1);
	if (typeof(count) === 'number') this.splice(0, count);
}

String.prototype.format = function(){
	var args = arguments;

	if(args.length == 0) return this;

	return this.replace(/{(\d+)}/g, function(match, index) { 
		var param = args[index];

		return typeof(param) != 'undefined' ? param : match;
	});
}

String.prototype.colorHex = function(){
	var res = '#';
	var self = this;
	if(/^(rgb|RGB)/.test(self)){
		var color = self.replace(/(?:\(|\)|rgb|RGB)*/g, '').split(',');
		for (var i = 0; i < color.length; i++){
			var hex = Number(color[i]).toString(16);
			if (hex === '0') hex += hex;	
			res += hex;
		}
		return res.length == 7 ? res : self;
	}
	else{
		var num = self.replace(/#/, '').split('');
		if (num.length === 6) return self;	
		if (num.length === 3){
			for (var i = 0; i < num.length; i += 1) res += num[i] + num[i];
			return res;
		}
	}
	return self;
}

String.prototype.replaceAll = function(src, dest){
    return this.replace(new RegExp(src, 'gm'), dest);
}

function strlen(str){
	if (str == null) return 0;

	var ch;
	var len = 0;
	var val = str + '';

	for (var i = 0; i < val.length; i++){
		ch = val.charCodeAt(i);
		if ((ch >= 0x0001 && ch <= 0x007E) || (0xFF60 <= ch && ch <= 0xFF9F)){
			len++;
		}
		else{
			len += 2;
		}
	}

	return len;
}

function isArray(val){
	return Array.isArray(val);
}

function isObject(val){
	return typeof(val) == 'object';
}

function isString(val){
	return typeof(val) == 'string';
}

function isFunction(val){
	return typeof(val) == 'function';
}

function getString(val){
	if (strlen(val) == 0) return '';
	return '' + val;
}

function isJsonString(str){
	try{
		JSON.parse(str);
		return true;
	}
	catch(e){
		return false;
	}
}

function htmlEscape(html){
	return html.replace(/[<>&"]/g, function(c){
		return {
			'<': '&lt;',
			'>': '&gt;',
			'&': '&amp;',
			'"': '&quot;'
		}[c];
	});
}

function getFileName(path){
	var pos = path.lastIndexOf('/');
	return pos >= 0 ? path.substring(pos + 1) : path;
}

function getFileExtname(path){
	var pos = path.lastIndexOf('.');
	return pos >= 0 ? path.substring(pos + 1).toLowerCase() : '';
}

function isFileName(name, required, maxlen){
	var len = strlen(name);

	if (len == 0) return required ? false : true;

	if (maxlen == null) maxlen = 64;

	var errstr = "`'^*<>,:;?|%\r\n\t\"\\/";

	if (len > maxlen) return false;

	for (var i = 0; i < len; i++){
		if (errstr.indexOf(name[i]) >= 0) return false;
	}

	return true;
}

function isFilePath(name, required, maxlen){
	var len = strlen(name);

	if (len == 0) return required ? false : true;

	if (maxlen == null) maxlen = 64;

	var errstr = "`'^*<>,:;?|%\r\n\t\"";

	if (len > maxlen) return false;

	for (var i = 0; i < len; i++){
		if (errstr.indexOf(name[i]) >= 0) return false;
	}

	return true;
}

function checkEmail(mail){
	var reg = /^[a-zA-Z0-9_-]+@([a-zA-Z0-9]+\.)+(com|cn|net|org)$/;
	return reg.test(mail);
}

function getAmountString(val){
	if (strlen(val) == 0) return '0.00';
	if (getString(val).indexOf('.') < 0) return val + '.00';
	if ((val = parseFloat(getString(val))) == null) return '0.00';

	val = getString(val);
	
	var pos = val.indexOf('.');
	
	if (pos > 0){
		var len = strlen(val.substring(pos + 1));

		if (len == 2) return val;
		else if (len == 1) return val + '0';
		else if (len == 0) return val + '00';
		else return val.substring(0, pos + 3);
	}

	return val + '.00';
}

function markdown(msg){
	var str = 0;
	var end = 0;
	var res = '';
	var src = msg;
	var tmp = null;
	var updated = false;
	var linetag = 'falryepqnfapfywrwd';
	
	while (true){
		str = msg.indexOf('```');
		if (str < 0){
			res += msg;
			break;
		}

		str += 3
		res += msg.substr(0, str);

		end = msg.indexOf('```', str);
		if (end < 0){
			res += msg.substr(str);
			updated = false;
			break;
		}

		tmp = msg.substring(str, end += 3).replace(new RegExp('\n\t', 'gm'), '\n\t\t');
		tmp = tmp.replace(new RegExp('\n--', 'gm'), '\n#' + linetag);
		tmp = tmp.replace(new RegExp('\n', 'gm'), linetag + '\n');
		msg = msg.substr(end);
		updated = true;
		res += tmp;
	}

	if (typeof(marked) == 'undefined'){
		loadScript('/res/lib/marked.js.gzip');
	}

	if (updated){
		msg = marked(res).replace(new RegExp('#' + linetag, 'gm'), '--');
		msg = msg.replace(new RegExp(linetag, 'gm'), '');
	}
	else{
		msg = marked(src);
	}

	return "<div class='MarkdownContentDiv'>" + msg + "</div>";
}

function setMarkedOptions(option){
	if (typeof(marked) == 'undefined'){
		loadScript('/res/lib/marked.js.gzip');
	}
	marked.setOptions(option);
}

function getMapFromList(list, field){
	let map = new Map();
	if (isArray(list) && list.length > 0){
		list.forEach(item => {
			if (isFunction(field)){
				map.set(field(item), item);
			}
			else{
				map.set(item[field], item);
			}
		});
	}
	return map;
}