
var curuser = null;
var logincallback = null;

function isTimeout(){
	return strlen(curuser) <= 0;
}

function sessionTimeout(){
	setTimeout(function(){
		showLoginDialog(true);
	}, 10);
}

function getCurrentUser(){
	return curuser;
}

function setLoginCallback(func){
	logincallback = func;
}

function showLoginDialog(password){
	var msg = getHttpResult('/app/workspace/pub/userlogin.htm');

	showToastMessage(msg, null, null, null, true);
	
	if (password) $('#UserLoginLabel').click();
}

function setCurrentUser(user, updated){
	if (curuser = user){
		if (strlen(user) > 0) setStorage('user', user);

		if (updated){
			if (isFunction(logincallback)){
				logincallback();
			}
			else{
				window.location.reload();
			}
		}
	}
}

function graspUserInfo(list, field, func){
	if (field == null){
		field = 'user';
	}
	else if (isFunction(field)){
		if (func == null){
			func = field;
		}
		field = 'user';
	}
	if (isArray(list)){
		if (list.length > 0){
			let arr = [];
			list.forEach(item => {
				arr.push(item[field]);
			});
			getHttpResult('/getuserlist', {user: arr.unique().join(',')}, function(data) {
				list.forEach(item => {
					for (let i = 0; i < data.list.length; i++) {
						if (item[field] == data.list[i].user){
							if (func){
								func(item, data.list[i]);
							}
							else{
								item['$user'] = data.list[i];
							}
							break;
						}
					}
				})
			})
		}
	}
	else{
		getHttpResult('/getuserlist', {user: list[key]}, function(data){
			if (func){
				func(list, data.list[0]);
			}
			else{
				list['$user'] = data.list[0];
			}
		});
	}
}

registInit(function(){
	var checklogintimer = null;

	function checkLogin(){
		getHttpResult('/checklogin', {flag: 'C'}, function(data){
			if (data.code > 0){
				setTimeout(function(){
					setCurrentUser(data.user);
				}, 1000);

				setCurrentUser(data.user);
			}
			else if (data.code == XG_TIMEOUT){
				setCurrentUser(null);
			}
			else if (checklogintimer){
				if (data.status && data.status == 404){
					clearInterval(checklogintimer);
					checklogintimer = null;
				}
			}
		});
	}

	checklogintimer = setInterval(checkLogin, 10000);

	checkLogin();
});