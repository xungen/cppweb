var singletontimermap = {};

function clearSingletonInterval(name){
	if (name == null){
		for (var name in singletontimermap) clearInterval(singletontimermap[name]);
		singletontimermap = {};
	}
	else{
		if (name in singletontimermap){
			clearInterval(singletontimermap[name]);
			delete singletontimermap[name];
		}
	}
}

function setSingletonInterval(name, delay, func){
	if (name in singletontimermap) clearInterval(singletontimermap[name]);
	singletontimermap[name] = setInterval(func, delay);
}

function isLeapYear(year){
	if (year % 4) return false;

	if (year % 400 == 0){
		if (year % 3200 == 0) return (year % 153600 == 0) ? true : false;

		return true;
	}

	return (year % 100 == 0) ? false : true;
}

function getMonthDayCount(month, leap){
	var count = -1;

	if (month <= 0 || month > 12) return -1;

	if (month == 2){
		count = leap ? 29 : 28;
	}
	else if (month <= 7){
		count = (month % 2 == 0) ? 30 : 31;
	}
	else{
		count = (month % 2 == 0) ? 31 : 30;
	}

	return count;
}

function getDateCheckCode(year, month, date){
	if (year < -999) return 1;
	if (year > 9999) return 2;
	if (month < 1) return 3;
	if (month > 12) return 4;
	if (date < 1) return 5;

	var days = getMonthDayCount(month, isLeapYear(year));
	
	if (date > days) return 6;

	return 0;
}

function getDateTime(diff, seperator){
	var now = new Date();
	
	if (diff) now.setDate(now.getDate() + diff);

	var len = strlen(seperator);
	var D = now.getDate();
	var M = now.getMonth();
	var h = now.getHours();
	var m = now.getMinutes();
	var s = now.getSeconds();

	if (len == 0){
		seperator = '- :';
	}
	else if (len == 1){
		seperator = seperator + ' :';
	}
	else if (len == 2){
		seperator = seperator + ':';
	}
	
	M = M + 1;

	if (D < 10) D = '0' + D;
	if (M < 10) M = '0' + M;
	if (h < 10) h = '0' + h;
	if (m < 10) m = '0' + m;
	if (s < 10) s = '0' + s;
	
	return now.getFullYear() + seperator.charAt(0) + M + seperator.charAt(0) + D + seperator.charAt(1) + h + seperator.charAt(2) + m + seperator.charAt(2) + s;
}

function getDateTimeString(date){
	if (date == null) date = new Date();

	return date.getFullYear() + '-' + getString(100 + date.getMonth() + 1).substr(1)
			+ '-' + getString(100 + date.getDate()).substr(1)
			+ ' ' + getString(100 + date.getHours()).substr(1)
			+ ':' + getString(100 + date.getMinutes()).substr(1)
			+ ':' + getString(100 + date.getSeconds()).substr(1);
}

function getDateTimeStringFromId(id){
	return id.substr(0, 4) + '-' + id.substr(4, 2) + '-' + id.substr(6, 2) + ' ' + id.substr(8, 2) + ':' + id.substr(10, 2) + ':' + id.substr(12, 2);
}

function getShortDateTimeString(date){
	if (date == null) date = new Date();

	return date.getFullYear() + getString(100 + date.getMonth() + 1).substr(1)
			+ getString(100 + date.getDate()).substr(1)
			+ getString(100 + date.getHours()).substr(1)
			+ getString(100 + date.getMinutes()).substr(1)
			+ getString(100 + date.getSeconds()).substr(1);
}