function getVue(elem, data, method, filter){
	return new Vue({el: '#' + getCtrl(elem).id, data: data, methods: method, filters: filter});
}

(function(){
	Vue.directive('inserted', {
		inserted: function(elem){
			dispatch(elem, 'click');
		}
	});

	Vue.component('v-ul', {
		props: ['id', 'title', 'option'],
		template: "<span><ul :id='id'><li v-for=\"text in option.split('|')\" v-html='text'></li></ul></span>"
	});
	
	Vue.component('v-ol', {
		props: ['id', 'title', 'option'],
		template: "<span><ol :id='id'><li v-for=\"text in option.split('|')\" v-html='text'></li></ol></span>"
	});
	
	Vue.component('v-icon', {
		props: ['id', 'title', 'size', 'path'],
		template: "<span @click='init' v-inserted><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled/><span @click='click'><input :id='id' class='PreviewIcon' style='cursor:default' readonly/></span></span>",
		methods: {
			init : function(){
				if (strlen(this.size) == 0){
					this.size = 15;
				}
				else{
					this.size = parseInt(this.size);
				}

				if (this.size > 0) $('#' + this.id).width(this.size).height(this.size).parent().prev().height(this.size);
			},
			click : function(){
				var pick = new IconPicker(this.id, this.path);
				$('#' + this.id).blur(function(){
					pick.hide();
				});
			}
		}
	});

	Vue.component('v-text', {
		props: ['id', 'size', 'type', 'title', 'readonly', 'maxlength', 'placeholder'],
		template: "<span><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled/><input class='TextField' autocomplete='off' :id='id' :type='type' :size='size' :maxlength='maxlength' :readonly='readonly' :placeholder='placeholder'/></span>"
	});
	
	Vue.component('v-label', {
		props: ['id', 'title'],
		template: "<span><input class='TextLabel' type='label' :value='title' :size='getTextSize(title)' :id='id' disabled/></span>"
	});
	
	Vue.component('v-border', {
		props: ['size', 'radius', 'color', 'padding'],
		template: "<div :style=\"{padding:padding,display:'inline-block',borderRadius:radius,border:parseInt(size||1) + 'px solid ' + (color||'#999')}\"><slot></slot></div>"
	});
	
	Vue.component('v-color', {
		props: ['id', 'title', 'value'],
		template: "<span @click='init' v-inserted><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled/><span @click='click'><input :id='id' size='7' class='TextField' style='cursor:default' readonly/></span></span>",
		methods: {
			init : function(){
			},
			click : function(){
				var pick = new ColorPicker(this.id);
				$('#' + this.id).blur(function(){
					pick.hide();
				});
			}
		}
	});
	
	Vue.component('v-button', {
		props: ['id', 'title'],
		template: "<span><input class='TextButton' :value='title' :size='getTextSize(title)' :id='id' onfocus='this.blur()' readonly/></span>"
	});
	
	Vue.component('v-select', {
		props: ['id', 'title', 'option'],
		template: "<span @click='init' v-inserted><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled/><select class='TextSelect' :id='id'><option v-for=\"(text, idx) in option.split('|')\" :value='idx' v-html='text'></option></select></span>",
		methods: {
			init : function(){
				setTimeout(function(){
					var widget = $('#' + this.id);
					var height = widget.prev().height();
					widget.height(height).css('line-height', '17px');
				});
			}
		}
	});

	Vue.component('v-qrcode', {
		props: ['id', 'size', 'value', 'background', 'foreground'],
		template: "<span @click='init' :id='id' :size='size' :value='value' :background='background' :foreground='foreground' v-inserted></span>",
		methods: {
			init : function(){
				var widget = $('#' + this.id);
				var size = widget.attr('size');
				if (strlen(size) == 0 || (size = parseInt(size)) <= 32) size = 32;
				if (typeof(widget.qrcode) == 'undefined') loadScript('/res/lib/qrcode.js.gzip');
				widget.html('').qrcode({text: widget.val(), width: size, height: size, background: widget.attr('background'), foreground: widget.attr('foreground')});
			}
		}
	});
	
	Vue.component('v-textarea', {
		props: ['id', 'title', 'height', 'readonly', 'maxlength', 'placeholder'],
		template: "<span @click='init' v-inserted><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled/><textarea class='TextArea' :id='id' :maxlength='maxlength' :readonly='readonly' :placeholder='placeholder'/></span>",
		methods: {
			init : function(){
				if (strlen(this.height) == 0){
					this.height = 60;
				}
				else{
					this.height = parseInt(this.height);
				}
				$('#' + this.id).height(this.height).css('resize', 'none').prev().height(this.height);
			}
		}
	});

	Vue.component('v-password', {
		props: ['id', 'size', 'type', 'title', 'readonly', 'maxlength', 'placeholder'],
		template: "<table class='TextFieldTable' @click='init' v-inserted><tr><td><input type='password' :id='id' :type='type' :size='size' :maxlength='maxlength' :readonly='readonly' :placeholder='placeholder'></td><td><img/></td></tr></table>",
		methods: {
			init : function(){
				if (this.inited) return true;

				var txt = $('#' + this.id);
				var tab = $('#' + this.id).parent().parent().parent();

				if (strlen(this.type) == 0 || this.type.toLowerCase() == 'password'){
					tab.find('img').attr('src', '/res/img/eyeclose.png');
					txt.attr('type', 'password');
				}
				else{
					tab.find('img').attr('src', '/res/img/eyeopen.png');
					txt.attr('type', 'text');
				}

				tab.find('td').css('padding', '0px').css('margin', '0px');
				tab.css('background', txt.css('background'));

				txt.focus(function(){
					tab.addClass('TextFieldTableFocus');
				}).blur(function(){
					tab.removeClass('TextFieldTableFocus');
				});

				tab.find('img').click(function(){
					if (this.src.indexOf('open') > 0){
						this.src = '/res/img/eyeclose.png';
						txt.attr('type', 'password');
					}
					else{
						this.src = '/res/img/eyeopen.png';
						txt.attr('type', 'text');
					}
				});

				this.inited = true;

				return true;
			}
		}
	});

	Vue.component('v-datetext', {
		props: ['id', 'min', 'max', 'title', 'readonly', 'placeholder'],
		template: "<span><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled='disabled'/><input class='TextField' @click='init' type='text' :id='id' :size='10' :readonly='readonly' :placeholder='placeholder'/></span>",
		methods: {
			init: function(){
				var dt = getDateTime().substr(0, 10);

				if ((strlen(this.min) == 0 || dt >= this.min) && (strlen(this.max) == 0 || dt <= this.max)){
					dt = true;
				}
				else{
					dt = false;
				}
				
				laydate({
					elem: '#' + this.id,
					istoday: dt,
					istime: false,
					min: this.min,
					max: this.max,
					format: 'YYYY-MM-DD',
				});
			}
		}
	});
	
	Vue.component('v-datetimetext', {
		props: ['id', 'min', 'max', 'title', 'readonly', 'placeholder'],
		template: "<span><input class='TextLabel' :value='title' :size='getTextSize(title)' disabled='disabled'/><input class='TextField' @click='init' type='text' :id='id' :size='19' :readonly='readonly' :placeholder='placeholder'/></span>",
		methods: {
			init: function(){
				var dt = getDateTime();
				
				if ((strlen(this.min) == 0 || dt >= this.min) && (strlen(this.max) == 0 || dt <= this.max)){
					dt = true;
				}
				else{
					dt = false;
				}
				
				laydate({
					elem: '#' + this.id,
					istoday: true,
					istime: true,
					min: this.min,
					max: this.max,
					format: 'YYYY-MM-DD hh:mm:ss',
				});
			}
		}
	});
}());