var initfuncs = [];
var machinetype = 'PC';
var browserinfo = null;

function getHost(){
	var host = window.location.href;
	var pos = host.indexOf('/', 8);
	return pos < 0 ? host : host.substring(0, pos);
}

function registInit(func){
	initfuncs.push(func);
}

function getParameter(name){
	var reg = new RegExp('(^|&)'+ name +'=([^&]*)(&|$)');
	var res = window.location.search.substr(1).match(reg);
	return res ? decodeURIComponent(res[2]) : null;
}

function clearStorage(){
	localStorage.clear();
}

function getStorage(key){
	return localStorage.getItem(key);
}

function removeStorage(key){
	localStorage.removeItem(key);
}

function setStorage(key, val){
	localStorage.setItem(key, val);
}

function clearSession(){
	sessionStorage.clear();
}

function getSession(key){
	return sessionStorage.getItem(key);
}

function removeSession(key){
	sessionStorage.removeItem(key);
}

function setSession(key, val){
	sessionStorage.setItem(key, val);
}

function clearCookie(){
	var keys = document.cookie.match(/[^ =;]+(?=\=)/g);
	if (keys){
		var i = keys.length;
		while (i--){
			document.cookie = keys[i]+'=0;expires=' + new Date(0).toUTCString();
		}
		setCurrentUser(null);
	}
}

function setClipboard(text, toast){
	if (typeof(ClipboardJS) == 'undefined'){
		loadScript('/res/lib/clipboard.js.gzip');
	}

	var board = new ClipboardJS('body', {
		text: function(){
			if (toast) showToast(toast);
			board.destroy();
			return text;
		}
	});
}

function getBrowserInfo(){
	if (browserinfo) return browserinfo;

	var ua = navigator.userAgent.toLowerCase();
	var msie = (ua.match(/firefox|chrome|safari|opera/g) || 'other')[0];
	
	if ((ua.match(/msie|trident/g) || [])[0]) msie = 'msie';

	var pc;
	var plat;
	var prefix;

	if ('ontouchstart' in window || ua.indexOf('touch') !== -1 || ua.indexOf('mobile') !== -1){
		if (ua.indexOf('ipad') >= 0){
			pc = 'pad';
		}
		else if (ua.indexOf('mobile') >= 0){
			pc = 'mobile';
		}
		else if (ua.indexOf('android') >= 0){
			pc = 'android';
		}
		else{
			pc = 'pc';
		}
	}
	else{
		pc = 'pc';
	}
	
	machinetype = pc.toUpperCase();

	switch (msie){
		case 'chrome':
		case 'safari':
		case "mobile":
			prefix = 'webkit';
			break;
		case 'msie':
			prefix = 'ms';
			break;
		case 'firefox':
			prefix = 'moz';
			break;
		case 'opera':
			prefix = 'opera';
			break;
		default:
			prefix = 'webkit';
			break
	}

	plat = ua.indexOf('android') > 0 ? 'android' : navigator.platform.toLowerCase();
	
	browserinfo = {
		version: (ua.match(/[\s\S]+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [])[1],
		plat: plat,
		type: msie,
		prefix: prefix,
		machinetype: machinetype
	};

	return browserinfo;
}

setTimeout(function(){
	getBrowserInfo();

	loadStyle('/res/css/widget.css');

	if (machinetype == 'MOBILE') loadStyle('/res/css/mobile.css');

	$(document).ready(function(){
		if (getBackgroundImage(document.body) == 'none') loadScript('/res/lib/background.js.gzip');
	});

	initfuncs.forEach(function(func){
		func();
	});
});