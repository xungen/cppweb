var lang = 'CN';
var EnSpaceBack = 1;
var EnSpaceFront = 2;

var langtextmap = new Map([
	['failed', {cn: '失败'}],
	['success', {cn: '成功'}],
	['process', {cn: '处理'}],
	['required', {cn: '不能为空'}],
	['too short', {cn: '长度不足'}],
	['format error', {cn: '格式错误'}],
	['record not found', {cn: '记录不存在'}],
	['permission denied', {cn: '没有访问权限'}],
	['operate frequently', {cn: '不要频繁操作'}],
]);

function translate(text, space) {
	var res = langtextmap.get(text);

	if (isObject(res)){
		res = (lang == 'CN' ? res.cn : res.en);
	}

	text = (strlen(res) > 0 ? res : text);

	if (lang == 'CN') return text;

	switch (space){
	case EnSpaceBack:
		return text + ' ';
	case EnSpaceFront:
		return ' ' + text;
	case EnSpaceBack | EnSpaceFront:
		return ' ' + text + ' ';
	}

	return text;
}

function setLanguage(language){
	if (strlen(language) > 0){
		lang = (language == 'CN' ? 'CN' : 'EN');
	}
}

function setLanguageText(entext, cntext) {
	if (langtextmap.has(entext)) return false;

	langtextmap.set(entext, {cn: cntext})

	return true;
}