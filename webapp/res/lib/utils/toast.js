var toasthidetimer = null;

setLanguageText('CANCEL', '取消');
setLanguageText('CONFIRM', '确认');
setLanguageText('MESSAGE', '信息');

function hideMsgBox(){
	$('#XG_MSGBOX_DIV_ID').hide();
	$('#XG_MODESCREEN_DIV_ID').fadeOut();
}

function hideToastBox(force){
	if (force){
		$('#XG_TOAST_DIV_ID').remove();
		$('#XG_TOAST_MODESCREEN_DIV_ID').remove();
	}
	else{
		$('#XG_TOAST_DIV_ID').hide();
		$('#XG_TOAST_MODESCREEN_DIV_ID').fadeOut();
	}
}

function showMsgBox(mode){
	$('#XG_MSGBOX_DIV_ID').show();
	if (mode == null || mode){
		$('#XG_MODESCREEN_DIV_ID').fadeIn();
	}
}

function showNoAccessToast(){
	showErrorToast(translate('permission denied'));
}

function setToastTitle(title){
	$('#XG_TOAST_TITLE_LABEL_ID').html(title);
}

function setMessageTitle(title){
	$('#XG_MSGBOX_TITLE_LABEL').html('&nbsp' + title);
}

function setToastErrorText(text, elem){
	var title = $('#XG_TOAST_TITLE_LABEL_ID').html();

	setToastTitle("<font color='#D00'>" + text + "</font>");

	if (elem && strlen(text) > 0){
		$.pack(elem).focus().bind('input propertychange',function(){
			setToastTitle(title);
		});
	}
}

function setMessageErrorText(text, elem){
	$('#XG_MSGBOX_ERRMSG_LABEL').html(text);

	if (elem && strlen(text) > 0){
		$.pack(elem).focus().bind('input propertychange',function(){
			$('#XG_MSGBOX_ERRMSG_LABEL').html('');
		});
	}
}

function getTextSize(text){
	var sz = strlen(text);

	if (machinetype == 'PC'){
		return sz > 0 ? sz : 1;
	}
	else{
		return sz > 0 ? sz : 1;
	}
}

function showToastMessage(msg, mode, timeout, width, closefunc){
	hideToastBox(true);

	var MsgBoxDiv = createCtrl('div', 'XG_TOAST_DIV_ID');
	
	if (strlen(width) == 0) width = 'auto';

	MsgBoxDiv.style.display = 'none';
	MsgBoxDiv.style.width = width;

	appendCtrl(MsgBoxDiv);

	if (mode == null || mode){
		var ModeScreenDiv = createCtrl('div', 'XG_TOAST_MODESCREEN_DIV_ID');
		
		appendCtrl(ModeScreenDiv);

		if (machinetype == 'MOBILE'){
			ModeScreenDiv.style.background = 'none';
		}

		ModeScreenDiv.style.display = 'none';
		ModeScreenDiv.style.top = getScrollTop() + 'px';

		$('#XG_TOAST_MODESCREEN_DIV_ID').fadeIn();
	}

	msg = "<div id='XG_TOAST_CONTENT_DIV_ID'>" + msg + "</div>";

	if (closefunc){
		$(MsgBoxDiv).html("<span id='XG_TOAST_TITLE_LABEL_ID'></span><span id='XG_TOAST_CLOSE_BUTTON_ID'></span></br>" + msg);

		$('#XG_TOAST_CLOSE_BUTTON_ID').click(function(){
			if (isFunction(closefunc)){
				var flag = closefunc();
				if (flag == null || flag) hideToastBox();
			}
			else{
				hideToastBox();
			}
		});
	}
	else{
		$(MsgBoxDiv).html(msg);
	}

	MsgBoxDiv.style.display = 'block';

   	centerWindow(MsgBoxDiv, 0, 0);

	if (toasthidetimer){
		clearTimeout(toasthidetimer);
		toasthidetimer = null;
	}

	if (timeout && timeout > 0){
		toasthidetimer = setTimeout(hideToastBox, timeout);
	}

	var msgbox = {};

	msgbox['dialog'] = MsgBoxDiv;
	msgbox['timeout'] = timeout;

	msgbox['title'] = function(msg){
		$('#XG_TOAST_TITLE_LABEL_ID').html(msg);
	};

	msgbox['center'] = function(){
		centerWindow(MsgBoxDiv, 0, 0);
	}

	return msgbox;
}

function showToastDialog(data, title, attach){
	var num = 0;
	var max = 4;
	var res = {};
	var msg = data;

	if (isString(data)){
		data = {};
	}
	else{
		if (data.style == null) data.style = [{}];

		while (data.style.length < data.title.length) data.style.push({});

		num = data.title.length;
		msg = "<table class='DialogTable'>"
				+ "<tr v-for='(val,key,idx) in model'><td>"
				+ "<input class='TextLabel' v-model='title[idx]' :size='titlesize' disabled/>"
				+ "<select v-if=\"isArray(style[idx].select)\" v-model='model[key]' :id=\"'vue_toast_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select\" :value='val' v-html='val'></option></select>"
				+ "<select v-else-if=\"strlen(style[idx].select)>0\" v-model='model[key]' :id=\"'vue_toast_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select.split('|')\" :value='val' v-html='val'></option></select>"
				+ "<textarea v-else-if=\"style[idx].type=='textarea'\" v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_toast_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' class='TextArea' style='resize:none'/>"
				+ "<input v-else v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_toast_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' autocomplete='off' class='TextField'/>"
				+ "</td><td v-if='style[idx].minlength' class='MustTagTd'>*</td></tr></table>";

		for (var i = 0; i < num; i++){
			var tmp = getTextSize(data.title[i]);
			if (tmp > max) max = tmp;
		}

		data['titlesize'] = max + 2;
	}

	if (isString(attach)){
		msg += "<div id='XG_TOAST_ATTACH_DIV_ID'>" + attach + '</div>';
	}
	else if (isFunction(attach)){
		msg += "<div id='XG_TOAST_ATTACH_DIV_ID' style='margin-top:10px;padding-top:8px;text-align:center;border-top:1px solid #CCC'><button class='TextButton' style='float:none'>确定</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='TextButton' style='float:none'>取消</button></div>";
	}

	function close(flag){
		if (flag == null) flag = 0;
		for (var i = 0; i < num; i++){
			var elem = getCtrl('vue_toast_dialog_' + i);
			dispatch(elem, 'input');
			if (flag){
				var len = strlen(elem.value);
				if (len > 0 && isFunction(data.style[i].check)){
					if (!data.style[i].check(elem.value)){
						setMessageErrorText(data.title[i] + translate('format error', EnSpaceFront), elem);
						return false;
					}
				}
				if (data.style[i].minlength && len < data.style[i].minlength){
					if (len > 0){
						setMessageErrorText(data.title[i] + translate('too short', EnSpaceFront), elem);
					}
					else{
						setMessageErrorText(data.title[i] + translate('required', EnSpaceFront), elem);
					}
					return false;
				}
			}
		}
		var res = true;
		if (isFunction(attach)){
			res = attach(flag);
			res = res == null || res;
		}
		if (res) hideToastBox(true);
		return res;
    }

    var dialog = showToastMessage(msg, true, null, null, close);

	if (title) dialog.title(title);

	getVue('XG_TOAST_CONTENT_DIV_ID', data);

	res['title'] = dialog.title
	res['msgbox'] = dialog;
	dialog.center();

    for (var i = 0; i < num; i++){
		var elem = getCtrl('vue_toast_dialog_' + i);
		if (data.style[i].type == 'textarea') $(elem).height(48).prev().height(48);
		if (data.style[i].setup) data.style[i].setup(elem);
		res[elem.name] = elem;
	}

	if (isFunction(attach)){
		$('#XG_TOAST_ATTACH_DIV_ID button:first').click(function(){
			close(1);
		});

		$('#XG_TOAST_ATTACH_DIV_ID button:last').click(function(){
			close(0);
		});
	}

    return res;
}

function showToast(msg, timeout, width){
	if (timeout == null){
		var len = strlen(msg);
		
		if (len < 10) timeout = 1500;
		else if (len < 20) timeout = 2000;
		else if (len < 30) timeout = 2500;
		else timeout = 3000;
	}

	return showToastMessage(msg, false, timeout, width);
}

function showErrorToast(msg, timeout, width){
	if (timeout == null){
		var len = strlen(msg);
		
		if (len < 10) timeout = 2000;
		else if (len < 20) timeout = 2500;
		else if (len < 30) timeout = 3000;
		else timeout = 3500;
	}
	
	return showToastMessage('<red>' + msg + '</red>', false, timeout, width);
}

function addMsgBoxOption(html){
	return $('#XG_MSGBOX_DIV_CONFIRM_ID').parent().append(html).children().last();
}

function showMessage(msg, title, width, mode, func, attach, data){
	hideMsgBox();
	hideToastBox();

	var MsgBoxDiv = createCtrl('div', 'XG_MSGBOX_DIV_ID');
	
	if (strlen(width) == 0) width = 'auto';

	MsgBoxDiv.style.display = 'none';

	appendCtrl(MsgBoxDiv);

	if (mode == null || mode){
		var ModeScreenDiv = createCtrl('div', 'XG_MODESCREEN_DIV_ID');
		
		appendCtrl(ModeScreenDiv);

		ModeScreenDiv.style.display = 'none';
		ModeScreenDiv.style.top = getScrollTop() + 'px';

		$('#XG_MODESCREEN_DIV_ID').fadeIn();
	}

	var str = "<table><tr><td id='XG_MSGBOX_DIV_TITLE_ID'></td></tr><tr><td><div id='XG_MSGBOX_TEXT_ID'></div></td></tr><tr id='XG_MSGBOX_OPTION_ROW_ID'><td><div style='margin-top:2px;padding-top:6px;text-align:center;border-top:1px solid #888'><button class='TextButton' id = 'XG_MSGBOX_DIV_CONFIRM_ID' onclick='hideMsgBox()'></button>";
	
	if (strlen(attach) > 0) str += attach;

	str += "</div></td></tr></table>";
	
	$(MsgBoxDiv).html(str);

	var MsgLabel = getCtrl('XG_MSGBOX_TEXT_ID');
	var MsgTitle = getCtrl('XG_MSGBOX_DIV_TITLE_ID');
	var ConfirmBtn = getCtrl('XG_MSGBOX_DIV_CONFIRM_ID');

	MsgLabel.style.width = width;

    function endrag(source, target){
        var moveable = false;
        var x0 = 0, y0 = 0, x1 = 0, y1 = 0;
    
        function moveFunc(elem){
            elem = getCtrl(elem);
        
            var result = {x : elem.offsetLeft, y : elem.offsetTop};
        
            elem = elem.offsetParent;
            
            while (elem){
                result.x += elem.offsetLeft;
                result.y += elem.offsetTop;
                elem = elem.offsetParent;
            }
            
            return result;	  
        }
        
        source = getCtrl(source);
        target = getCtrl(target);
        source.style.cursor = 'move';
        
        source.onmousedown = function(e){
            e = e || window.event;
            x0 = e.clientX ; 
            y0 = e.clientY ; 
            x1 = parseInt(moveFunc(target).x); 
            y1 = parseInt(moveFunc(target).y);   
            moveable = true;
        };
        
        source.onmousemove = function(e){
            e = e || window.event;
    
            if (moveable){
                target.style.top = (y1 + e.clientY - y0) + 'px';
                target.style.left = (x1 + e.clientX - x0) + 'px';
            }
        };
    
        source.onmouseup = function(e){
            if (moveable){
                moveable = false; 
            }
        };
    }

	endrag('XG_MSGBOX_DIV_TITLE_ID', 'XG_MSGBOX_DIV_ID');

	if (title == null) title = translate('MESSAGE');

	ConfirmBtn.innerText = translate('CONFIRM');

	$(MsgLabel).html(msg);
	$(MsgTitle).html("<label id='XG_MSGBOX_TITLE_LABEL'>" + title + "</label><label id='XG_MSGBOX_ERRMSG_LABEL'/><label class='ImageButton' id='XG_MSGBOX_CLOSE_ICON'></label>");
	
	var CloseIcon = getCtrl('XG_MSGBOX_CLOSE_ICON');
	var TitleLabel = getCtrl('XG_MSGBOX_TITLE_LABEL');
	
	CloseIcon.onclick = function(){
		if (func == null){
			hideMsgBox();
		}
		else{
			var res = func(0);
			
			if (res == null || res){
				hideMsgBox();
			}
		}
	}

	ConfirmBtn.onclick = function(){
		if (func == null){
			hideMsgBox();
		}
		else{
			var res = func(1);
			
			if (res == null || res){
				hideMsgBox();
			}
		}
	}
	
	if (data == null || isObject(data)){
		getVue('XG_MSGBOX_TEXT_ID', data);
	}

	MsgBoxDiv.style.display = 'block';

   	centerWindow(MsgBoxDiv, 0, 0);

	var msgbox = {};
	
	msgbox['dialog'] = MsgBoxDiv;
	msgbox['confirm'] = ConfirmBtn.onclick;
	
	msgbox['title'] = function(msg){
		$('#XG_MSGBOX_TITLE_LABEL').html(msg);
	};
	
	msgbox['text'] = function(msg){
		$('#XG_MSGBOX_TEXT_ID').html(msg);

		centerWindow(MsgBoxDiv, 0, 0);
	};
	
	msgbox['hideOption'] = function(){
		$('#XG_MSGBOX_OPTION_ROW_ID').hide();
	};
	
	msgbox['showOption'] = function(){
		$('#XG_MSGBOX_OPTION_ROW_ID').show();
	};

	msgbox['setErrorText'] = function(text, elem){
		setMessageErrorText(text, elem);
	};

	msgbox['center'] = function(){
		centerWindow(MsgBoxDiv, 0, 0);
	}

	return msgbox;
}

function showConfirmMessage(msg, title, func, width, mode, data){
	var msgbox = new showMessage(msg, title, width, mode, func, "<label>&nbsp;&nbsp;</label><button class='TextButton' id='XG_MSGBOX_DIV_CANCEL_ID' onclick='hideMsgBox()'>" + translate('CANCEL') + "</button>", data);
	var cancelbtn = getCtrl('XG_MSGBOX_DIV_CANCEL_ID');
	
	cancelbtn.onclick = function(){
		if (func == null){
			hideMsgBox();
		}
		else{
			var res = func(0);
			
			if (res == null || res) hideMsgBox();
		}
	}

	msgbox['cancel'] = cancelbtn.onclick;
	
	return msgbox;
}

function showDialog(data, title, func){
	if (isString(data)) return showMessage(data, title, null, true, func);

    if (data.style == null) data.style = [{}];

    while (data.style.length < data.title.length) data.style.push({});

	var max = 4;
	var res = {};
	var num = data.title.length;
    var msg = "<table class='DialogTable'>"
            + "<tr v-for='(val,key,idx) in model'><td>"
            + "<input class='TextLabel' v-model='title[idx]' :size='titlesize' disabled/>"
			+ "<select v-if=\"isArray(style[idx].select)\" v-model='model[key]' :id=\"'vue_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select\" :value='val' v-html='val'></option></select>"
            + "<select v-else-if=\"strlen(style[idx].select)>0\" v-model='model[key]' :id=\"'vue_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select.split('|')\" :value='val' v-html='val'></option></select>"
            + "<textarea v-else-if=\"style[idx].type=='textarea'\" v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' class='TextArea' style='resize:none'/>"
			+ "<input v-else v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' autocomplete='off' class='TextField'/>"
			+ "</td><td v-if='style[idx].minlength' class='MustTagTd'>*</td></tr></table>";

	for (var i = 0; i < num; i++){
		var tmp = getTextSize(data.title[i]);
		if (tmp > max) max = tmp;
	}

	data['titlesize'] = max + 2;

    res['msgbox'] = showMessage(msg, title, null, true, function(flag){
		for (var i = 0; i < num; i++){
			var elem = getCtrl('vue_dialog_' + i);
			dispatch(elem, 'input');
			if (flag){
				var len = strlen(elem.value);
				if (len > 0 && isFunction(data.style[i].check)){
					if (!data.style[i].check(elem.value)){
						setMessageErrorText(data.title[i] + translate('format error', EnSpaceFront), elem);
						return false;
					}
				}
				if (data.style[i].minlength && len < data.style[i].minlength){
					if (len > 0){
						setMessageErrorText(data.title[i] + translate('too short', EnSpaceFront), elem);
					}
					else{
						setMessageErrorText(data.title[i] + translate('required', EnSpaceFront), elem);
					}
					return false;
				}
			}
		}
        if (func) return func(flag);
    }, null, data);

    for (var i = 0; i < num; i++){
		var elem = getCtrl('vue_dialog_' + i);
		if (data.style[i].type == 'textarea') $(elem).height(48).prev().height(48);
		if (data.style[i].setup) data.style[i].setup(elem);
		res[elem.name] = elem;
	}

    return res;
}

function showConfirmDialog(data, title, func){
	if (isString(data)) return showConfirmMessage(data, title, func);

    if (data.style == null) data.style = [{}];

    while (data.style.length < data.title.length) data.style.push({});

	var max = 4;
	var res = {};
	var num = data.title.length;
    var msg = "<table class='DialogTable'>"
            + "<tr v-for='(val,key,idx) in model'><td>"
            + "<input class='TextLabel' v-model='title[idx]' :size='titlesize' disabled/>"
			+ "<select v-if=\"isArray(style[idx].select)\" v-model='model[key]' :id=\"'vue_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select\" :value='val' v-html='val'></option></select>"
            + "<select v-else-if=\"strlen(style[idx].select)>0\" v-model='model[key]' :id=\"'vue_dialog_'+idx\" :name='key' class='TextSelect'><option v-for=\"val in style[idx].select.split('|')\" :value='val' v-html='val'></option></select>"
            + "<textarea v-else-if=\"style[idx].type=='textarea'\" v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' class='TextArea' style='resize:none' spellcheck='false'/>"
			+ "<input v-else v-model='model[key]' @input='if(style[idx].filter)model[key]=style[idx].filter($event.currentTarget.value)' :id=\"'vue_dialog_'+idx\" :name='key' :size='style[idx].size' :type=\"style[idx].type||'text'\" :readonly='style[idx].readonly' :maxlength='style[idx].maxlength' :placeholder='style[idx].placeholder' autocomplete='off' spellcheck='false' class='TextField'/>"
			+ "</td><td v-if='style[idx].minlength' class='MustTagTd'>*</td></tr></table>";

	for (var i = 0; i < num; i++){
		var tmp = getTextSize(data.title[i]);
		if (tmp > max) max = tmp;
	}

	data['titlesize'] = max + 2;

    res['msgbox'] = showConfirmMessage(msg, title, function(flag){
		for (var i = 0; i < num; i++){
			var elem = getCtrl('vue_dialog_' + i);
			dispatch(elem, 'input');
			if (flag){
				var len = strlen(elem.value);
				if (len > 0 && isFunction(data.style[i].check)){
					if (!data.style[i].check(elem.value)){
						setMessageErrorText(data.title[i] + translate('format error', EnSpaceFront), elem);
						return false;
					}
				}
				if (data.style[i].minlength && len < data.style[i].minlength){
					if (len > 0){
						setMessageErrorText(data.title[i] + translate('too short', EnSpaceFront), elem);
					}
					else{
						setMessageErrorText(data.title[i] + translate('required', EnSpaceFront), elem);
					}
					return false;
				}
			}
		}
        if (func) return func(flag);
    }, null, true, data);

    for (var i = 0; i < num; i++){
		var elem = getCtrl('vue_dialog_' + i);
		if (data.style[i].type == 'textarea') $(elem).height(48).prev().height(48);
		if (data.style[i].setup) data.style[i].setup(elem);
		res[elem.name] = elem;
	}

    return res;
}