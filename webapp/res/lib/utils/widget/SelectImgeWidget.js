setLanguageText('picture size can not exceed', '图片大小不能超过');
setLanguageText('picture format[bmp/png/gif/jpg]', '图片仅限于bmp/png/gif/jpg格式');

function SelectImgeWidget(elem, title, height, size){
	var func = null;
	var self = this;
	var ImageView = null;
	var UploadButton = null;
	var id = 'SelectImgeWidget' + getSequence();

	str = "<div><input type='file' id='" + id + "FileUpload'/><table><tr><td><input class='FileUploadButton' id='" + id + "UploadButton' onfocus='this.blur()' readonly/></td><td><input id='" + id + "UploadPreviewImage' onfocus='this.blur()' readonly/></td></tr></table></div>";
	
	$.pack(elem).append(str);

	ImageView = $('#' + id + 'UploadPreviewImage');
	UploadButton = $('#' + id + 'UploadButton');
	setLabelText(id + 'UploadButton', title);
	ImageView.addClass('UploadPreviewImage');
	$('#' + id + 'FileUpload').hide();
	
	if (strlen(height) > 0){
		UploadButton.height(height).css('line-height', height);
		ImageView.height(height).css('width', height);
	}
	
	showUploadButton = function(){
		$('#' + id + 'FileUpload').val('');
	}
	
	UploadButton.click(function(){
		$('#' + id + 'FileUpload').click();
	});

	if (size == null){
		size = 8 * 1024 * 1024;
	}
	else{
		size *= 1024;
	}
				
	$('#' + id + 'FileUpload').change(function(){
		var sz = 0;
		var file = getCtrl(id + 'FileUpload').files[0];
		
		self.data = null;

		if (file){
			var ext = getFileExtname(file.name);
			
			if (ext.length <= 0 || 'bmp/png/gif/jpg/jpeg'.indexOf(ext) < 0){
				showToast(translate('picture format[bmp/png/gif/jpg]'));
			}
			else{
				if (file.size > size){
					showToast(translate('picture size can not exceed', EnSpaceBack) + size / 1024 + 'KB');
				}
				else{
					self.data = window.URL.createObjectURL(file);
					ImageView.css('background-image', 'url(' + self.data + ')');

					if (func) func(self);
				}
			}
		}
	});

	this.image = ImageView;
	this.button = UploadButton;
	this.callback = function(fun){
		func = fun;
	}
	this.getFileData = function(){
		return getCtrl(id + 'FileUpload').files[0];
	}
}