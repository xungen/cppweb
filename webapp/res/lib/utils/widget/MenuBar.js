function MenuBar(elem, text, icon, group, height, closable, contextmenu){
	if (closable == null) closable = true;
	if (group == null) group = [];


	if (height == null){
		height = 16;
	}
	else{
		try{
			height = parseInt(height);
		}
		catch(e){
			height = 16;
		}
	}

	let menubox = null;
	let children = [];
	let minimize = false;
	let toastleft = height + 14;

	height += 'px';

	function getLine(text, icon, menu){
		icon = icon ? "<td style='width:24px'><div style='margin:9px 5px 9px 3px;width:" + height + ";height:" + height+ ";background-size:100%;background-image:url(" + icon + ")'/></td>" : "";
		menu = menu && closable ? "<td class='MenuBarTextTd' style='text-aling:right'><div style='float:right;inline-block;margin-right:6px;font-family:宋体' class='transform'>&gt;</div></td>" : "<td></td>";
		return "<table style='width:100%;text-align:left'><tr>" + icon + "<td class='MenuBarTextTd' style='height:" + height + "'><div style='padding:9px 4px 7px 4px'>" + text + "</div></td>" + menu + "</tr></table>";
	}

	elem = $.pack(elem).append('<div></div>').children('div:last');

	if (closable){
		elem.append("<div style='cursor:default;padding:2px;user-select:none'>" + getLine(text, icon, true) + "</div><div style='display:none'></div>");
	}
	else{
		elem.append("<div style='cursor:default;padding:2px;user-select:none'>" + getLine(text, icon, true) + "</div><div></div>");
	}

	var tag = elem.find('.transform');
	var bar = elem.children('div:first');
	var list = elem.children('div:last');

	var self = this;
	var click = null;
	var toast = null;
	var color = '#FFF';
	var curitem = null;
	var barcolor = '#586068';
	var listcolor = '#343640';
	var hovercolor = '#00AAAA';
	var selectcolor = '#FFFFFF';

	tag.css('transform', 'rotate(90deg)');
	bar.css('border-left', '2px solid rgba(0, 0, 0, 0)');
	bar.css('background-color', barcolor).css('color', color);
	list.css('background-color', listcolor).css('color', color);

	group.push(this);

	if (contextmenu){
		let title = [];
		for (let i = 0; i < contextmenu.length; i++){
			title.push(contextmenu[i].title);
		}
		menubox = new ContextMenu(title, function(text, elem){
			for (let i = 0; i < contextmenu.length; i++){
				let item = contextmenu[i];
				if (text == item.title){
					if (item.click) item.click(elem);
					break;
				}
			}
		});
	}

	function open(){
		if (!minimize){
			list.slideDown(150);
			tag.css('transform', 'rotate(-90deg)');
			group.forEach(function(item){
				if (item != self) item.close();
			});
		}
	}

	function close(){
		if (closable){
			list.slideUp(150);
			tag.css('transform', 'rotate(90deg');
		}
	}

	function select(item, click){
		if (menubox){
			menubox.view.hide();
		}
		if (item){
			item = $.pack(item);
			if (curitem && item == curitem) return curitem;
			list.children('div').css('background-color', listcolor).css('color', color);
			curitem = item.css('background-color', selectcolor).css('color', hovercolor);
			if (click) click(item);
			group.forEach(function(bar){
				if (item != bar.select()) bar.unselect();
			});
		}
		if (color == hovercolor){
			bar.css('border-left', '2px solid rgba(0, 0, 0, 0)');
		}
		else{
			bar.css('border-left', '2px solid ' + color);
		}
		return curitem ? curitem : null;
	}

	bar.click(function(){
		if (!minimize){
			if (list.css('display') == 'none'){
				if (click) click(true);
				open();
			}
			else{
				if (click) click(false);
				close();
			}
		}
		if (children.length <= 0){
			group.forEach(function(item){
				if (bar == item.bar){
					if (color == hovercolor){
						bar.css('border-left', '2px solid rgba(0, 0, 0, 0)').css('color', hovercolor);
					}
					else{
						bar.css('border-left', '2px solid ' + color).css('color', hovercolor);
					}
				}
				else{
					item.unselect();
				}
			});
		}
	}).hover(function(){
		bar.css('text-shadow', '0px 0px 1px ' + hovercolor);
		if (minimize){
			if (click && children.length <= 0){
				click(false);
			}
			group.forEach(function(item){
				if (item == self){
					item.showToast();
				}
				else{
					item.hideToast();
				}
			});		
		}
	},
	function(){
		bar.css('text-shadow', '0px 0px 0px #000');
	});

	this.add = function(text, icon, click){
		list.append("<div style='cursor:default;padding-left:" + (toastleft - 4) + "px'>" + getLine(text, icon) + "</div>");
		var item = list.children('div:last').click(function(){
			select(item, click);
		}).hover(function(){
			item.css('color', hovercolor);
		}, function(){
			if (item != curitem) item.css('color', color);
		});
		item['select'] = function(updated){
			select(item, updated || updated == null ? click : null);
		}
		children.push({elem: item, text: text, icon: icon, click: click});
		if (menubox) menubox.bind(item);
		item['menubar'] = self;
		item['text'] = text;
		return item;
	}

	this.hideToast = function(){
		if (toast){
			toast.css('display', 'none');
		}
	}

	this.showToast = function(){
		let children = self.children();

		if (toast == null){
			toast = $.pack(createCtrl('div'));
			appendCtrl(toast);
		}

		toast.css('display', 'none');
		toast.css('z-index', '10000');
		toast.css('position', 'fixed');
		toast.css('background-color', hovercolor).css('color', selectcolor);
		toast.html("<div><div class='HiddenScrollbar' style='overflow-x:hidden;overflow-y:scroll;position:absolute;left:0px;top:0px;right:-20px;bottom:0px'><div></div></div></div>");
		
		toast.hover(function(){
		}, function(){
			self.hideToast();
		});

		let size = 0;
		let view = toast.children().children().children();

		if (children.length <= 0){
			children.push({text: text, icon: icon, click: function(){
				bar.click();
			}});
		}

		for (let i = 0; i < children.length; i++){
			let text = children[i].text;
			let icon = children[i].icon;
			let click = children[i].click;
			view.append("<div style='cursor:default;padding:2px'>" + getLine(text, icon) + "</div>");
			view.children('div:last').click(function(){
				self.hideToast();
				select(this, click);
			});

			if (size < getTextSize(text)){
				size = getTextSize(text);
			}
		}

		let unit = elem.height();
		var bottom = getClientHeight();
		let mintop = getTop(elem.parent());
		let viewheight = unit * children.length;

		view.height(viewheight);

		if (mintop + viewheight > bottom){
			viewheight = parseInt((bottom - mintop)  / unit) * unit;
		}

		let top = getTop(bar) - viewheight / 2 + unit / 2;
	
		if (top + viewheight > bottom){
			viewheight = parseInt((bottom - top)  / unit) * unit;
			top = bottom - viewheight;
		}

		let left = getLeft(bar) + toastleft;

		toast.height(viewheight).width(size * 8 + 40).css('top', top + 'px').css('left', left).css('display', 'inline-block');
	}

	this.tag = tag;
	this.bar = bar;
	this.list = list;
	this.text = text;
	this.open = open;
	this.close = close;
	this.select = select;

	this.unselect = function(){
		if (curitem){
			curitem.css('background-color', listcolor).css('color', color);
			curitem = null;
		}
		bar.css('color', color).css('border-left', '2px solid rgba(0, 0, 0, 0)');
	}

	this.click = function(func){
		if (func){
			click = func;
		}
		else{
			if (click) click();
		}
	}

	this.color = function(data){
		if (data){
			if (data.color) color = data.color;
			if (data.barcolor) barcolor = data.barcolor;
			if (data.listcolor) listcolor = data.listcolor;
			if (data.hovercolor) hovercolor = data.hovercolor;
			if (data.selectcolor) selectcolor = data.selectcolor;

			bar.css('background-color', barcolor).css('color', color);
			list.css('background-color', listcolor).css('color', color);
		}

		return {color: color, barcolor: barcolor, listcolor: listcolor, hovercolor: hovercolor, selectcolor: selectcolor};
	}

	this.clear = function(){
		list.children().remove();
		children = [];
	}

	this.minimize = function(){
		close();
		minimize = true;
		bar.find('.MenuBarTextTd').hide();
	}

	this.maximize = function(){
		minimize = false;
		bar.find('.MenuBarTextTd').show();
		if (curitem) open();
	}

	this.children = function(){
		let arr = [];
		list.find('.MenuBarTextTd').children().each(function(){
			let text = $(this).html();
			for (let i = 0; i < children.length; i++){
				if (text == children[i].text){
					arr.push(children[i]);
					break;
				}
			}
		});
		return children = arr;
	}
}