var recordlisthtm = ''

setLanguageText('loading...', '正在加载数据...');
setLanguageText('load data failed', '加载数据失败');
setLanguageText('total 0 records  at 0/0 page', '共0条 第0/0页');
setLanguageText('total {0} records  at {1}/{2} page', '共{0}条 第{1}/{2}页');

function getRecordListViewHTML() {
	if (strlen(recordlisthtm) > 0) return recordlisthtm;
	return recordlisthtm = getHttpResult('/app/workspace/pub/recordlist.htm');
}

function RecordListView(elem, path, vmdata, paramfunc, quietmode){
	var id = 'RecordListView' + getSequence();

	$.pack(elem).append("<div id='" + id + "'></div>");

	if (vmdata['list'] == null) vmdata['list'] = [];
	if (vmdata['count'] == null) vmdata['count'] = 0;
	if (vmdata['button'] == null) vmdata['button'] = [];
	if (vmdata['filter'] == null) vmdata['filter'] = {};
	if (vmdata['pagenum'] == null) vmdata['pagenum'] = 0;
	if (vmdata['pagecount'] == null) vmdata['pagecount'] = 0;
	if (vmdata['cellstyle'] == null) vmdata['cellstyle'] = '';
	if (vmdata['showcheckbox'] == null) vmdata['showcheckbox'] = false;

	$('#' + id).html(getRecordListViewHTML().replaceAll('CELLSTYLE', vmdata.cellstyle));

	var arr = [];
	var button = vmdata.button;

	for (var i = 0; i < button.length; i++){
		var access = button[i]['access'];
		if (strlen(access) > 0) arr.push(access);
	}

	if (arr.length > 0){
		var map = getAccessMap(arr);
		for (var i = 0; i < button.length; i++){
			if (button[i]['visable'] == null){
				var access = button[i]['access'];
				if (strlen(access) > 0){
					button[i]['visable'] = map[access] >= 0;
				}
			}
		}
	}

	if (vmdata['pagesize'] == null){
		var height = $(document).height();
		if (height < 500){
			vmdata['pagesize'] = 10;
		}
		else if (height < 800){
			vmdata['pagesize'] = 15;
		}
		else{
			vmdata['pagesize'] = 20;
		}
	}

	if (paramfunc == null){
		paramfunc = function(){
			return {};
		};
	}

	var view = getVue(id, vmdata);
	var nextpage = $('#' + id + ' .NextPage').children().css('margin-right', '3px');
	var lastpage = $('#' + id + ' .LastPage').children().css('margin-right', '5px');
	var firstpage = $('#' + id + ' .FirstPage').children().css('margin-right', '3px');
	var frontpage = $('#' + id + ' .FrontPage').children().css('margin-right', '3px');

	var pagenumtext = $('#' + id + ' .PageNumText');
	var pagecountlabel = $('#' + id + ' .PageCountLabel').children().css('margin-right', '4px');

	firstpage.click(function(){
		vmdata.pagenum = 0;
		loadRecord(true);
	});

	lastpage.click(function(){
		vmdata.pagenum = vmdata.pagecount - 1;
		loadRecord(true);
	});

	nextpage.click(function(){
		vmdata.pagenum += 1;
		loadRecord(true);
	});

	frontpage.click(function(){
		vmdata.pagenum -= 1;
		loadRecord(true);
	});

	function page(num, flag){
		if (num >= vmdata.pagecount) num = vmdata.pagecount - 1;
		if (num < 0) num = 0;

		pagenumtext.val(num + 1);
		vmdata.pagenum = num;
		loadRecord(flag);
	}

	pagenumtext.blur(function(){
		var val = pagenumtext.val();

		if (strlen(val) == 0){
			pagenumtext.val(vmdata.pagenum + 1);
		}
		else{
			var num = parseInt(val);

			if (num != vmdata.pagenum + 1) page(num - 1);
		}
	}).keydown(function(e){
		if (e.which == 13) pagenumtext.blur();
	});

	function setLabelText(text){
		pagecountlabel.attr('size', getTextSize(text)).val(text);
	}

	function getSelectList(){
		let res = [];
		$('#' + id).find('.RecordListCheckBox').each(function(){
			if (this.checked){
				res.push(vmdata.list[$(this).attr('index')]);
			}
		});
		return res;
	}

	function updatePageButtonStatus(){
		if (vmdata.pagecount == null || vmdata.pagecount < 0) return pagenumtext.parent().hide();

		pagenumtext.parent().show();

		firstpage.attr('disabled', 'disabled');
		frontpage.attr('disabled', 'disabled');
		nextpage.attr('disabled', 'disabled');
		lastpage.attr('disabled', 'disabled');

		if (vmdata.pagecount <= 0){
			setLabelText(translate('total 0 records  at 0/0 page'));
			pagenumtext.val('');
		}
		else{
			if (vmdata.pagenum >= vmdata.pagecount) vmdata.pagenum = vmdata.pagecount - 1;

			var num = vmdata.pagenum + 1;

			if (vmdata.pagenum > 0){
				frontpage.removeAttr('disabled');
				firstpage.removeAttr('disabled');
			}
			
			if (num < vmdata.pagecount){
				nextpage.removeAttr('disabled');
				lastpage.removeAttr('disabled');
			}

			setLabelText(translate('total {0} records  at {1}/{2} page').format(vmdata.count, num, vmdata.pagecount));
			pagenumtext.val(num);
		}

		if (vmdata.pagecount > 999){
			pagenumtext.attr('size', '5');
		}
		else if (vmdata.pagecount > 99) {
			pagenumtext.attr('size', '3');
		}
		else{
			pagenumtext.attr('size', '2');
		}
	}

	function loadRecord(flag){
		var param = paramfunc();

		param['pagenum'] = vmdata.pagenum;
		param['pagesize'] = vmdata.pagesize;

		if (flag) showToastMessage(translate('loading...'));

		getHttpResult(path, param, function(data){
			if (flag) hideToastBox();

			if (data.code == XG_TIMEOUT){
				if (flag) sessionTimeout();
			}
			else if (data.code == XG_AUTHFAIL){
				if (flag) showToast(translate('permission denied'));
			}
			else if (data.code < 0){
				if (flag) showToast(translate('load data failed'));
			}
			else{
				if (data.list == null) data.list = [];

				if (typeof(data.count) == 'undefined'){
					data.count = data.list.length;
					vmdata.pagecount = data.count > 0 ? 1 : 0;
				}
				else{
					vmdata.pagecount = Math.ceil(data.count / vmdata.pagesize);
				}

				if (data.list.length == 0){
					if (vmdata.pagecount > 0 && vmdata.pagenum > 0){
						vmdata.pagenum = 0;
						loadRecord(flag);
						return false;
					}

					if (flag) showToast(translate('record not found'));
				}

				if (vmdata.check) vmdata.check(data);

				vmdata.count = data.count;
				vmdata.list = data.list;

				updatePageButtonStatus();

				setTimeout(function(){
					var barcheck = $('#' + id).find('.RecordBarCheckBox');
					var listcheck = $('#' + id).find('.RecordListCheckBox').each(function(){
						barcheck[0].checked = false;
						this.checked = false;
					}).click(function(){
						var bar = barcheck[0];
						var list = getSelectList();
						if (list.length == vmdata.list.length){
							bar.indeterminate = false;
							bar.checked = true;
						}
						else if (list.length > 0){
							bar.indeterminate = true;
							bar.checked = true;
						}
						else{
							bar.indeterminate = false;
							bar.checked = false;
						}
					});

					barcheck.change(function(){
						var checked = this.checked;
						listcheck.each(function(){
							this.indeterminate = false;
							this.checked = checked;
						});
					});
				});
			}
		}, flag);
	}

	this.getSelectList = getSelectList;
	this.reload = loadRecord;
	this.view = $('#' + id);
	this.data = vmdata;
	this.page = page;
	this.vue = view;

	this.update = function(check){
		if (check || check == null){
			if (isFunction(check)) check(vmdata);
			if (vmdata.check) vmdata.check(vmdata);
		}
		view.$forceUpdate();
	}

	this.remove = function(item){
		var arr = vmdata.list;
		var len = vmdata.list.length;

		if (isObject(item)){
			for (var i = 0; i < len; i++){
				if (item == arr[i]){
					arr.splice(i, 1);
					view.$forceUpdate();
					return true;
				}
			}
		}
		else{
			if (item >= 0 && item < len){
				arr.splice(item, 1);
				view.$forceUpdate();
				return true;
			}
		}

		return false;
	}

	if (path){
		loadRecord(quietmode ? false : true);
	}
	else{
		updatePageButtonStatus();
	}
}