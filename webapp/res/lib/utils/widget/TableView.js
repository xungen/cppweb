var recordviewhtm = ''

function getRecordViewHTML() {
	if (strlen(recordviewhtm) > 0) return recordviewhtm;
	return recordviewhtm = getHttpResult('/app/tableview/pub/recordview.htm');
}

function TableView(elem, tabid){
	var id = getSequence();
	var msg = getRecordViewHTML();

	msg = msg.replaceAll('[$][{]recordoperhtml[}]', '').replaceAll("[$][{]tabid[}]", tabid);
	msg = msg.replaceAll('QueryRecordButton', 'QueryRecordButton' + id);
	msg = msg.replaceAll('QueryCondTable', 'QueryCondTable' + id);
	msg = msg.replaceAll('RecordListDiv', 'RecordListDiv' + id);
	msg = msg.replaceAll('RecordOperTd', 'RecordOperTd' + id);
	msg = msg.replaceAll('recordvmdata', 'recordvmdata' + id);

	$.pack(elem).html(msg);

	return this.vmdata = window['$recordvmdata' + id];
}