function SlideBar(elem, maxval, minval, height){
	var bar = getCtrl(elem);

	bar.innerHTML = "<div style='position:relative;margin:0px;display:inline-block;background:#6B6'>0</div>";

	if (minval == null) minval = 0;
	if (maxval == null) maxval = minval + 100;

	var func = null;
	var self = this;
	var drag = bar.childNodes[0];
	var score = maxval - minval;

	if (!height) height = 14;

	bar.style.padding = '0px';
	bar.style.userSelect = 'none';
	bar.style.background = '#BBB';
	bar.style.height = height + 'px';

	drag.style.top = '-2px';
	drag.style.textAlign = 'center';
	drag.style.userSelect = 'none';
	drag.style.cursor = 'default';
	drag.style.border = '1px dashed #888';
	drag.style.height = (height + 2) + 'px';
	drag.style.lineHeight = (height + 3) + 'px';
	drag.style.width = strlen(minval) <= 3 && strlen(maxval) <= 3 ? '22px' : '26px';
	
	var value = 0;
	var barwidth = bar.offsetWidth;
	var dragwidth = drag.offsetWidth;
	var slidewidth = barwidth - dragwidth + 1;

	this.getValue = function(){
		return value;
	}

	this.change = function(fun){
		func = fun;
	}

	this.setValue = function(val){
		var pos = parseInt(slidewidth * (val - minval) / score + 0.5);

		pos += getLeft(bar);

		$(drag).offset({left: pos}).text(val);

		if (value != val){
			value = val

			if (isFunction(func)) func(val);
		}
	}

	drag.onmousedown = function(ev){
		var ev = ev || window.event;
		var str = getLeft(bar);

		if (ev.button == 0){
			var x = ev.clientX;
			var ox = getLeft(drag);
			var doconmouseup = document.onmouseup;
			var docmousemove = document.onmousemove;

			function update(dx){
				var pos = ox + dx;

				if (pos < str) pos = str;
				if (pos > str + slidewidth) pos = str + slidewidth;

				self.setValue(minval + parseInt((pos - str) * score / slidewidth + 0.5));
			}

		    document.onmousemove = function(ev){
		        var ev = ev || window.event;
				update(ev.clientX - x);
			};

			document.onmouseup = function(){
		        document.onmousemove = docmousemove;
				document.onmouseup = doconmouseup;
				var ev = ev || window.event;
				update(ev.clientX - x);
		    };
		}
	}
};