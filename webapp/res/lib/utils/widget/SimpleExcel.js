
function SimpleExcel(xlsx, index){
	if (index == null) index = 0;
	let name = xlsx.SheetNames[index];
	let data = xlsx.Sheets[name];

	function getDateTimeString(time){
		let d = time - 1;
		let t = Math.round((d - Math.floor(d)) * 24 * 60 * 60);
		return moment(new Date(1900, 0, d, 0, 0, t)).format('YYYY-MM-DD HH:mm:ss');
	}

	for (let i = 0; i < 104; i++){
		let key = null;
		if (i < 26){
			key = String.fromCharCode(i + 65);
		}
		else if (i < 52){
			key = 'A' +  String.fromCharCode(i - 26 + 65);
		}
		else if (i < 78){
			key = 'B' +  String.fromCharCode(i - 52 + 65);
		}
		else{
			key = 'C' +  String.fromCharCode(i - 78 + 65);
		}
		this[key] = this[key.toLocaleLowerCase()] = function(index){
			let tmp = data[key + (index + 1)];
			if (tmp == null) return null;
			if (tmp.t == 'n'){
				if (tmp.w.indexOf(':') > 0) return getDateTimeString(tmp.v);
				if (tmp.w.indexOf('/') > 0 || tmp.w.indexOf('-') > 0) return getDateTimeString(tmp.v).substring(0, 10)
			}
			return getString(tmp.v);
		}
	}

	let rows = 0;
	let cols = 0;

	try{
		let arr = data['!ref'].split(':');
		if (arr[1].substring(1, 2) >= 'A'){
			rows = parseInt(arr[1].substring(2));
			if (arr[1].substring(0, 1) == 'C'){
				cols = arr[1].charCodeAt(1) - 'A'.charCodeAt(0) + 78 + 1;
			}
			else if (arr[1].substring(0, 1) == 'B'){
				cols = arr[1].charCodeAt(1) - 'A'.charCodeAt(0) + 52 + 1;
			}
			else{
				cols = arr[1].charCodeAt(1) - 'A'.charCodeAt(0) + 26 + 1;
			}
		}
		else{
			rows = parseInt(arr[1].substring(1));
			cols = arr[1].charCodeAt(0) - arr[0].charCodeAt(0) + 1;
		}
	}
	catch(e){
		showToast('解析文件失败');
	}

	this.xlsx = function(){
		return xlsx;
	}
	this.rows = function(){
		return rows;
	}
	this.cols = function(){
		return cols;
	}
	this.get = function(x, y){
		if (y < 26){
			return this[String.fromCharCode('A'.charCodeAt(0) + y)](x);
		}
		else if (y < 52){
			return this['A' + String.fromCharCode('A'.charCodeAt(0) + y - 26)](x);
		}
		else if (y < 78){
			return this['B' + String.fromCharCode('A'.charCodeAt(0) + y - 52)](x);
		}
		else{
			return this['C' + String.fromCharCode('A'.charCodeAt(0) + y - 78)](x);
		}
	}
	this.toArray = function(title){
		let arr = [];
		for (let i = 0; i < rows; i++){
			let item = {};
			for (let key in title){
				item[title[key]] = getString(this[key](i));
			}
			arr.push(item);
		}
		return arr;
	}
	this.toString = function(title){
		let arr = this.toArray(title);
		return JSON.stringify(arr);
	}
}

function loadSimpleExcel(file, callback){
	function load(){
		if (file){
			let reader = new FileReader();
			reader.onload = function(e){
				callback(new SimpleExcel(XLSX.read(e.target.result, {type: 'binary'})));
			};
			reader.readAsBinaryString(file);
		}
	}

	if (typeof(moment) == 'undefined'){
		loadScript('/res/lib/moment.js.gzip', true);
	}

	if (typeof(XLSX) == 'undefined'){
		loadScript('/res/lib/xlsx.js.gzip', true);
		setTimeout(load, 3000);
	}
	else{
		load();
	}
}