function AutoTextPicker(elem, words){
    this.count = 0,
    this.index = -1,
    this.onclick = null,
    this.onselect = null,
    this.selected = null,
    this.datalist = words;

    var cols = 1;
    var self = this;
    var rows = Math.ceil(this.datalist.length / cols);
    var html = "<table id='AutoTextPickerTable' width='100%'>";

    for (i = 0; i < rows; i++){
        html += "<tr class='AutoDataRow'>";
        for (j = 0; j < cols && this.count < this.datalist.length; j++){
            html += '<td>'+ this.datalist[this.count] + '</td>';
            this.count++;
        }
        html+= "</tr>";
    }
    html += '</table>';

    this.elem = elem = getCtrl(elem);
    this.div = createCtrl('div');
    this.div.innerHTML = html;
    elem.parentNode.appendChild(this.div);
    this.div.style.zIndex = 1000;
    this.div.style.overflow = 'auto';
    this.div.style.position = 'fixed';
    this.div.style.border = '1px solid #ABC';
    this.div.style.left = getLeft(elem) + 'px';
    this.div.style.width = elem.clientWidth + 'px';
    this.div.style.top = (elem.clientHeight + getTop(elem) + 2) + 'px';
    
    elem.onclick = function(){
        if(self.div.style.display == 'none'){
            self.show();
        }
        else{
            self.hide();
       }
    };
    
    elem.onkeydown = function(e){
        if (e.keyCode == 13){
            self.hide();
        }
        else if (e.keyCode == 38){
            self.selectItem(-1);
            e.preventDefault();
        }
        else if (e.keyCode == 40){
            self.selectItem(1);
            e.preventDefault();
        }
    };

    this.selectItem = function(step){
        this.index += step;
        
        if (this.index < 0) this.index = this.count - 1;
        if (this.index >= this.count) this.index = 0;
 
        var tds = this.div.getElementsByTagName('td');
 
        for (var i = 0, l = tds.length ; i < l; i++){
            if (this.index == i){
                this.setText(tds[i].innerHTML);
                break;
            }
        }
 
        this.show();
    }
    
    this.hide = function(){
        this.div.style.display = 'none';
    }
    
    this.show = function(){
        this.div.style.display = 'block';
        this.updateStatus();
    }

    this.destroy = function(){
        $(this.div).remove();
    }

    this.setText = function(text){
        this.hide();
        elem.value = text;
        if (onselect) onselect(text);
    }

    this.updateStatus = function(){
        var tds = this.div.getElementsByTagName('td');
 
        for (var i = 0, l = tds.length ; i < l; i++){
            $(tds[i]).attr('index', i);
 
            tds[i].onmousedown = function(){
                self.setText(this.innerHTML);
                self.index = parseInt($(this).attr('index'));
                self.elem.blur();
                
                if (self.onclick) self.onclick(this.innerHTML);
            }
            
            if (this.selected){
                if (this.selected(tds[i].innerHTML, elem.value)){
                    tds[i].style.background = '#BCD';
                    tds[i].style.color = '#800';
                    this.index = i;
                }
                else{
                    tds[i].style.background = '#FFF';
                    tds[i].style.color = '#234';
                }
            }
            else{
                if (tds[i].innerHTML == elem.value){
                    tds[i].style.background = '#BCD';
                    tds[i].style.color = '#800';
                    this.index = i;
                }
                else{
                    tds[i].style.background = '#FFF';
                    tds[i].style.color = '#234';
                }
            }
        }

        $(tds).css('margin', '0px').css('cursor', 'default').css('padding', $(elem).css('padding')).css('font-size', $(elem).css('font-size'));
    }

    this.updateStatus();
}
