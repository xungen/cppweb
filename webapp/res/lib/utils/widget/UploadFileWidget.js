setLanguageText('NAME:', '名称：');
setLanguageText('TYPE:', '类型：');
setLanguageText('SIZE:', '大小：');
setLanguageText('file size can not exceed', '文件大小不能超过');

function UploadFileWidget(elem, title, height, size){
	var self = this;
	var change = null;
	var callback = null;
	var FileView = null;
	var UploadButton = null;
	var id = 'UploadFileWidget' + getSequence();

	str = "<div><input type='file' id='" + id + "FileUpload'/><input class='FileUploadButton' id='" + id + "UploadButton' onfocus='this.blur()' readonly/><input class='TextField' id='" + id + "UploadPreviewFile' onfocus='this.blur()' readonly/></div>";
	
	$.pack(elem).append(str);
	
	str = "<div id='" + id + "UploadFileName'></div><div id='" + id + "UploadFileType'></div>"
	str += "<div id='" + id + "UploadFileSize'></div><progress id='" + id + "UploadProgress'></progress>";

   	appendCtrl(createCtrl('div', id + 'UploadInfoDiv'));

	$('#' + id + 'UploadInfoDiv').addClass('UploadInfoDiv').html(str);
	$('#' + id + 'UploadProgress').addClass('UploadProgress');
	FileView = $('#' + id + 'UploadPreviewFile');
	UploadButton = $('#' + id + 'UploadButton');
	setLabelText(id + 'UploadButton', title);
	$('#' + id + 'FileUpload').hide();

	if (strlen(height) > 0){
		FileView.height(height);
	}
	else{
		height = FileView.height();
	}

	UploadButton.height(height).css('line-height', height);

	var showUploadButton = function(){
		$('#' + id + 'UploadInfoDiv').hide();
		$('#' + id + 'FileUpload').val('');
	}
	
	FileView.click(function(){
		$('#' + id + 'FileUpload').click();
	});

	UploadButton.click(function(){
		$('#' + id + 'FileUpload').click();
	});

	if (size == null) size = 1024 * 1024;
				
	$('#' + id + 'FileUpload').change(function(){
		var sz = 0;
		var file = getCtrl(id + 'FileUpload').files[0];
		
		self.data = null;

		$(this).val('');

		if (file){
			if (file.size > size){
				showToast(translate('file size can not exceed', EnSpaceBack) + size / 1024 + 'KB');
			}
			else{
				if (change == null || change(file)){
					self.data = window.URL.createObjectURL(file);
					sz = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';

					if (strlen(type = file.type) == 0) type = 'text/plain';

					if (type.length > 24) type = type.substr(0, 22) + '...';

					getCtrl(id + 'UploadFileName').innerHTML = translate('NAME:', EnSpaceBack) + file.name;
					getCtrl(id + 'UploadFileType').innerHTML = translate('TYPE:', EnSpaceBack) + type;
					getCtrl(id + 'UploadFileSize').innerHTML = translate('SIZE:', EnSpaceBack) + sz;

					var width = FileView.parent().width();
					var widget =  getCtrl(id + 'UploadInfoDiv');
					
					if (width < 180) width = 180;
					if (width > 360) width = 360;

					widget.style.top = (getTop(UploadButton[0]) + UploadButton.height() + 2) + 'px';
					widget.style.left = getLeft(UploadButton[0]) + 'px';
					widget.style.width = width + 'px';
					widget.style.display = 'block';

					var fd = new FormData();
					var xhr = new XMLHttpRequest();

					fd.append(id + 'FileUpload', file);

					xhr.addEventListener('error', function(evt){
						if (callback) callback();
						showUploadButton();
					}, false);
					
					xhr.addEventListener('abort', function(evt){
						if (callback) callback();
						showUploadButton();
					}, false);

					xhr.upload.addEventListener('progress', function(evt){
						var percent = 100;
						var progress = getCtrl(id + 'UploadProgress');

						if (evt.lengthComputable){
							percent = Math.round(evt.loaded * 100 / evt.total);
						}
						progress.innerText = percent + '%';
						progress.value = percent / 100.0;
					}, false);

					xhr.addEventListener('load', function(evt){
						var data = eval('(' + evt.target.responseText + ')');
						
						if (data.code <= 0){
							if (callback) callback(data);
						}
						else{
							var reader = new FileReader();

							reader.readAsDataURL(file);
							FileView.val(file.name);
							if (callback) callback(data);
						}

						setTimeout(showUploadButton, 1000);
					}, false);

					xhr.open('POST', '/RecvFile');
					xhr.send(fd);
				}
			}
		}
	});

	this.label = FileView;
	this.button = UploadButton;
	this.change = function(func){
		change = func;
		return this;
	}
	this.callback = function(func){
		callback = func;
		return this;
	}
	this.setFilter = function(filter){
		if (filter && filter.charAt(0) !=  '.') filter = '.' + filter;
		$('#' + id + 'FileUpload').attr('accept', filter);
		return this;
	}
	this.getFileData = function(){
		return getCtrl(id + 'FileUpload').files[0];
	}
}