function QueryWidget(elem, desc, title, func, width, padding){
	elem = $.pack(elem);

	if (width == null) width = 48;
	if (padding == null) padding = 8;

	elem.html("<table style='width:100%'><tr>"
		+ "<td><input class='TextField' autocomplete='off' style='width:100%;padding:" + padding + "px;box-shadow:none;border-radius:0px;box-sizing:border-box' type='text' placeholder='" + desc + "'/></td>"
		+ "<td style='width:" + width + "px'><input class='TextButton' style='width:100%;padding:" + padding + "px;box-shadow:none;border-radius:0px;box-sizing:border-box' value='" + title + "' readonly/></td>"
		+ "</tr></table>");

	var list = elem.find('input');
	var text = list.first();

	this.button = list.last().css('margin', '0px').click(function(){
		func(text);
	});

	this.text = text.keydown(function(e){
		if (e.which == 13) func(text);
	});

	return text;
}