function UploadImageWidget(elem, title, height, size, local){
	var self = this;
	var callback = null;
	var ImageView = null;
	var UploadButton = null;
	var id = 'UploadImageWidget' + getSequence();
	var str = "<div><input type='file' id='" + id + "FileUpload'/><input class='FileUploadButton' id='" + id + "UploadButton' onfocus='this.blur()' readonly/><span><input id='" + id + "UploadPreviewImage' style='cursor:default' readonly/></span></div>";

	$.pack(elem).append(str);

	str = "<div id='" + id + "UploadFileName'></div><div id='" + id + "UploadFileType'></div>"
	str += "<div id='" + id + "UploadFileSize'></div><progress id='" + id + "UploadProgress'></progress>";

   	appendCtrl(createCtrl('div', id + 'UploadInfoDiv'));

	$('#' + id + 'UploadInfoDiv').addClass('UploadInfoDiv').html(str);
	$('#' + id + 'UploadProgress').addClass('UploadProgress');
	ImageView = $('#' + id + 'UploadPreviewImage');
	UploadButton = $('#' + id + 'UploadButton');
	setLabelText(id + 'UploadButton', title);
	ImageView.addClass('UploadPreviewImage');
	$('#' + id + 'FileUpload').hide();
	
	if (strlen(height) > 0){
		UploadButton.height(height).css('line-height', height);
		ImageView.height(height).css('width', height);
	}
	
	var showUploadButton = function(){
		$('#' + id + 'UploadInfoDiv').hide();
		$('#' + id + 'FileUpload').val('');
	}
	
	UploadButton.click(function(){
		$('#' + id + 'FileUpload').click();
	});

	if (size == null) size = 1024 * 1024;
		
	$('#' + id + 'FileUpload').change(function(){
		var sz = 0;
		var file = getCtrl(id + 'FileUpload').files[0];
		
		self.data = null;

		$(this).val('');

		if (file){
			var ext = getFileExtname(file.name);

			if (ext.length <= 0 || 'bmp/png/gif/ico/jpg/jpeg'.indexOf(ext) < 0){
				showToast(translate('picture format[bmp/png/gif/jpg/ico]'));
			}
			else{
				if (file.size > size){
					showToast(translate('picture size can not exceed', EnSpaceBack) + size / 1024 + 'KB');
				}
				else{
					self.data = window.URL.createObjectURL(file);
					sz = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
					
					getCtrl(id + 'UploadFileName').innerHTML = translate('NAME:', EnSpaceBack) + file.name;
					getCtrl(id + 'UploadFileType').innerHTML = translate('TYPE:', EnSpaceBack) + file.type;
					getCtrl(id + 'UploadFileSize').innerHTML = translate('SIZE:', EnSpaceBack) + sz;

					function updateView(){
						var reader = new FileReader();
							
						reader.onload = function(e){
							var img = new Image();
	
							img.src = this.result;

							img.onload = function(){
								var canvas = document.createElement('canvas');
								var ctx = canvas.getContext('2d');
								var cx = 32;
								var cy = 32;

								if (height && height.indexOf('px') > 0){
									cx = cy = height.replace('px', '');

									if (cx < 32) cx = cy = 32;
								}

								canvas.width = cx;
								canvas.height = cy;
								ctx.drawImage(img, 0, 0, cx, cy);

								ImageView.css('background-image', 'url(' + canvas.toDataURL('image/png') + ')');
							}
						}

						reader.readAsDataURL(file);
					}

					if (local){
						updateView();

						if (callback) callback({code: file.size, url: file.name});

						return true;
					}

					var widget =  getCtrl(id + 'UploadInfoDiv');
					
					widget.style.top = (getTop(UploadButton[0]) + UploadButton.height() + 2) + 'px';
					widget.style.left = getLeft(UploadButton[0]) + 'px';
					widget.style.display = 'block';

					var fd = new FormData();
					var xhr = new XMLHttpRequest();

					fd.append(id + 'FileUpload', file);

					xhr.addEventListener('error', function(evt){
						if (callback) callback();
						showUploadButton();
					}, false);
					
					xhr.addEventListener('abort', function(evt){
						if (callback) callback();
						showUploadButton();
					}, false);

					xhr.upload.addEventListener('progress', function(evt){
						var percent = 100;
						var progress = getCtrl(id + 'UploadProgress');

						if (evt.lengthComputable){
							percent = Math.round(evt.loaded * 100 / evt.total);
						}
						progress.innerText = percent + '%';
						progress.value = percent / 100.0;
					}, false);

					xhr.addEventListener('load', function(evt){
						var data = eval('(' + evt.target.responseText + ')');
						
						if (data.code > 0) updateView();

						if (callback) callback(data);

						setTimeout(showUploadButton, 1000);
					}, false);

					xhr.open('POST', '/RecvFile');
					xhr.send(fd);
				}
			}
		}
	});

	$('#' + id + 'FileUpload').attr('accept', 'image/*');

	this.image = ImageView;
	this.button = UploadButton;
	this.callback = function(func){
		callback = func;
		return this;
	}
	this.setFilter = function(filter){
		if (filter && filter.charAt(0) !=  '.') filter = '.' + filter;
		$('#' + id + 'FileUpload').attr('accept', filter);
		return this;
	}
	this.getFileData = function(){
		return getCtrl(id + 'FileUpload').files[0];
	}
}