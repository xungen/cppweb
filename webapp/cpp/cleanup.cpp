#include <stdx/all.h>

vector<string> pathlist = {
	"$CPPWEB_INSTALL_HOME/webapp/dat"
};

class MainApplication : public Application
{
public:
	bool main()
	{
		LogThread::Instance()->init("log");

		LogTrace(eINF, "enter cleanup loop ...");

		for (auto& path : pathlist) path = stdx::translate(path);

		while (true)
		{
			CATCH_EXCEPTION({
				time_t now = time(NULL);

				for (const string& path : pathlist)
				{
					vector<string> vec;

					stdx::GetFolderContent(vec, path, eFILE);

					for (const string& name : vec)
					{
						string filepath = path + "/" + name;
						time_t filetime = path::mtime(filepath);

						if (now > filetime + 60 * 60)
						{
							LogTrace(eINF, "file[%s][%s] has expired", filepath.c_str(), DateTime(filetime).toString().c_str());

							path::remove(filepath);
						}
					}
				}
			});

			sleep(60);
		}

		return true;
	}
};

START_APP(MainApplication)
