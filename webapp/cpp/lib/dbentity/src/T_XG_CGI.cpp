#include "T_XG_CGI.h"


void CT_XG_CGI::clear()
{
	this->path.clear();
	this->maxsz.clear();
	this->maxcnt.clear();
	this->remark.clear();
	this->enabled.clear();
	this->hostmaxcnt.clear();
	this->statetime.clear();
}
int CT_XG_CGI::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_CGI(" + string(GetColumnString()) + ") VALUES(";
	sql += this->path.toValueString(conn->getSystemName());
	vec.push_back(&this->path);
	sql += ",";
	sql += this->maxsz.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->maxcnt.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->remark.toValueString(conn->getSystemName());
	vec.push_back(&this->remark);
	sql += ",";
	sql += this->enabled.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->hostmaxcnt.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_CGI::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->path = row->getString(0);
	this->path.setNullFlag(row->isNull());
	this->maxsz = row->getLong(1);
	this->maxsz.setNullFlag(row->isNull());
	this->maxcnt = row->getLong(2);
	this->maxcnt.setNullFlag(row->isNull());
	this->remark = row->getString(3);
	this->remark.setNullFlag(row->isNull());
	this->enabled = row->getLong(4);
	this->enabled.setNullFlag(row->isNull());
	this->hostmaxcnt = row->getLong(5);
	this->hostmaxcnt.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(6);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_CGI::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_CGI";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_CGI::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->path);
	return find(getPKCondition(), vec);
}
string CT_XG_CGI::getPKCondition()
{
	string condition;
	condition = "PATH=";
	condition += this->path.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_CGI::update(bool updatenull)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_CGI SET ";
	if (updatenull || !this->maxsz.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MAXSZ=";
		sql += this->maxsz.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->maxcnt.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MAXCNT=";
		sql += this->maxcnt.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (updatenull || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->hostmaxcnt.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "HOSTMAXCNT=";
		sql += this->hostmaxcnt.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->path);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_CGI::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_CGI";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_CGI::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->path);
	return remove(getPKCondition(), vec);
}
string CT_XG_CGI::getValue(const string& key)
{
	if (key == "PATH") return this->path.toString();
	if (key == "MAXSZ") return this->maxsz.toString();
	if (key == "MAXCNT") return this->maxcnt.toString();
	if (key == "REMARK") return this->remark.toString();
	if (key == "ENABLED") return this->enabled.toString();
	if (key == "HOSTMAXCNT") return this->hostmaxcnt.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_CGI::setValue(const string& key, const string& val)
{
	if (key == "PATH")
	{
		this->path = val;
		return true;
	}
	if (key == "MAXSZ")
	{
		this->maxsz = val;
		return true;
	}
	if (key == "MAXCNT")
	{
		this->maxcnt = val;
		return true;
	}
	if (key == "REMARK")
	{
		this->remark = val;
		return true;
	}
	if (key == "ENABLED")
	{
		this->enabled = val;
		return true;
	}
	if (key == "HOSTMAXCNT")
	{
		this->hostmaxcnt = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_CGI::update(bool updatenull, const string& condition, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_CGI SET ";
	if (updatenull || !this->maxsz.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MAXSZ=";
		sql += this->maxsz.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->maxcnt.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MAXCNT=";
		sql += this->maxcnt.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (updatenull || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->hostmaxcnt.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "HOSTMAXCNT=";
		sql += this->hostmaxcnt.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
