#include "T_XG_TIMER.h"


void CT_XG_TIMER::clear()
{
	this->path.clear();
	this->status.clear();
	this->timeval.clear();
	this->statetime.clear();
}
int CT_XG_TIMER::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_TIMER(" + string(GetColumnString()) + ") VALUES(";
	sql += this->path.toValueString(conn->getSystemName());
	vec.push_back(&this->path);
	sql += ",";
	sql += this->status.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->timeval.toValueString(conn->getSystemName());
	vec.push_back(&this->timeval);
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_TIMER::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->path = row->getString(0);
	this->path.setNullFlag(row->isNull());
	this->status = row->getLong(1);
	this->status.setNullFlag(row->isNull());
	this->timeval = row->getString(2);
	this->timeval.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(3);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_TIMER::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_TIMER";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_TIMER::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->path);
	return find(getPKCondition(), vec);
}
string CT_XG_TIMER::getPKCondition()
{
	string condition;
	condition = "PATH=";
	condition += this->path.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_TIMER::update(bool updatenull)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_TIMER SET ";
	if (updatenull || !this->status.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATUS=";
		sql += this->status.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->timeval.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TIMEVAL=";
		sql += this->timeval.toValueString(conn->getSystemName());
		v.push_back(&this->timeval);
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->path);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_TIMER::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_TIMER";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_TIMER::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->path);
	return remove(getPKCondition(), vec);
}
string CT_XG_TIMER::getValue(const string& key)
{
	if (key == "PATH") return this->path.toString();
	if (key == "STATUS") return this->status.toString();
	if (key == "TIMEVAL") return this->timeval.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_TIMER::setValue(const string& key, const string& val)
{
	if (key == "PATH")
	{
		this->path = val;
		return true;
	}
	if (key == "STATUS")
	{
		this->status = val;
		return true;
	}
	if (key == "TIMEVAL")
	{
		this->timeval = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_TIMER::update(bool updatenull, const string& condition, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_TIMER SET ";
	if (updatenull || !this->status.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATUS=";
		sql += this->status.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->timeval.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TIMEVAL=";
		sql += this->timeval.toValueString(conn->getSystemName());
		v.push_back(&this->timeval);
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
