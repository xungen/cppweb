#ifndef XG_ENTITY_CT_XG_PARAM_H
#define XG_ENTITY_CT_XG_PARAM_H
///////////////////////////////////////////////////////////////////////
#include <dbx/DBConnect.h>


class CT_XG_PARAM : public DBEntity
{
public:
	bool next();
	void clear();
	int insert();
	int remove();
	sp<QueryResult> find();
	string getPKCondition();
	string getValue(const string& key);
	int update(bool updatenull = false);
	bool setValue(const string& key, const string& val);
	int remove(const string& condition, const vector<DBData*>& vec = {});
	sp<QueryResult> find(const string& condition, const vector<DBData*>& vec = {});
	int update(bool updatenull, const string& condition, const vector<DBData*>& vec = {});

public:
	DBString	id;
	DBString	name;
	DBString	param;
	DBString	filter;
	DBBlob		remark;
	DBDateTime	statetime;

	static const char* GetTableName()
	{
		return "T_XG_PARAM";
	}

	static const char* GetColumnString()
	{
		return "ID,NAME,PARAM,FILTER,REMARK,STATETIME";
	}
};

///////////////////////////////////////////////////////////////////////
#endif
