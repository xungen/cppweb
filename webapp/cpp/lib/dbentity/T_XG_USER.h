#ifndef XG_ENTITY_CT_XG_USER_H
#define XG_ENTITY_CT_XG_USER_H
///////////////////////////////////////////////////////////////////////
#include <dbx/DBConnect.h>


class CT_XG_USER : public DBEntity
{
public:
	bool next();
	void clear();
	int insert();
	int remove();
	sp<QueryResult> find();
	string getPKCondition();
	string getValue(const string& key);
	int update(bool updatenull = false);
	bool setValue(const string& key, const string& val);
	int remove(const string& condition, const vector<DBData*>& vec = {});
	sp<QueryResult> find(const string& condition, const vector<DBData*>& vec = {});
	int update(bool updatenull, const string& condition, const vector<DBData*>& vec = {});

public:
	DBString	user;
	DBString	dbid;
	DBString	icon;
	DBString	name;
	DBString	password;
	DBString	menulist;
	DBString	grouplist;
	DBString	language;
	DBInteger	level;
	DBInteger	enabled;
	DBString	certno;
	DBString	mail;
	DBString	phone;
	DBString	address;
	DBString	remark;
	DBDateTime	statetime;

	static const char* GetTableName()
	{
		return "T_XG_USER";
	}

	static const char* GetColumnString()
	{
		return "USER,DBID,ICON,NAME,PASSWORD,MENULIST,GROUPLIST,LANGUAGE,LEVEL,ENABLED,CERTNO,MAIL,PHONE,ADDRESS,REMARK,STATETIME";
	}
};

///////////////////////////////////////////////////////////////////////
#endif
