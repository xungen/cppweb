#ifndef XG_ENTITY_CT_XG_MENU_H
#define XG_ENTITY_CT_XG_MENU_H
///////////////////////////////////////////////////////////////////////
#include <dbx/DBConnect.h>


class CT_XG_MENU : public DBEntity
{
public:
	bool next();
	void clear();
	int insert();
	int remove();
	sp<QueryResult> find();
	string getPKCondition();
	string getValue(const string& key);
	int update(bool updatenull = false);
	bool setValue(const string& key, const string& val);
	int remove(const string& condition, const vector<DBData*>& vec = {});
	sp<QueryResult> find(const string& condition, const vector<DBData*>& vec = {});
	int update(bool updatenull, const string& condition, const vector<DBData*>& vec = {});

public:
	DBString	id;
	DBString	folder;
	DBString	title;
	DBString	icon;
	DBString	url;
	DBInteger	enabled;
	DBString	remark;
	DBInteger	position;
	DBDateTime	statetime;

	static const char* GetTableName()
	{
		return "T_XG_MENU";
	}

	static const char* GetColumnString()
	{
		return "ID,FOLDER,TITLE,ICON,URL,ENABLED,REMARK,POSITION,STATETIME";
	}
};

///////////////////////////////////////////////////////////////////////
#endif
