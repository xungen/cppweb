#ifndef XG_WEBX_METRIC_CPP
#define XG_WEBX_METRIC_CPP
///////////////////////////////////////////////////////////
#include "../route.h"
#include "../metric.h"

void webx::MetricSet(const string& name, int value)
{
	typedef void (*MetricSetFunc)(const char*, int);
	static MetricSetFunc func = (MetricSetFunc)Process::GetObject("HTTP_METRIC_SET_FUNC");

	if (func) func(name.c_str(), value);
}
void webx::MetricAdd(const string& name, int value)
{
	typedef void (*MetricAddFunc)(const char*, int);
	static MetricAddFunc func = (MetricAddFunc)Process::GetObject("HTTP_METRIC_ADD_FUNC");

	if (func) func(name.c_str(), value);
}
string webx::GetMetricSQLitePath(const string& appname, const string& name, int date)
{
	string filename = stdx::replace(name, "/", ".");

	filename = stdx::replace(filename, "\\", ".");

	if (IsFileName(appname) && IsFileName(filename))
	{
		return stdx::format("%sdat/monitor/%d/%s/%s.db", HttpServer::Instance()->getPath().c_str(), date, appname.c_str(), filename.c_str());
	}
	else
	{
		return stdx::EmptyString();
	}
}
string webx::GetMetricSQLitePath(const string& appname, const string& name, const DateTime& dt)
{
	return webx::GetMetricSQLitePath(appname, name, dt.year * 10000 + dt.month * 100 + dt.day);
}
///////////////////////////////////////////////////////////
#endif
