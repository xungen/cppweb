#ifndef XG_WEBX_METRIC_H
#define XG_WEBX_METRIC_H
///////////////////////////////////////////////////////////
#include "route.h"

namespace webx
{
	const int METRIC_ADD = 0;
	const int METRIC_SET = 1;
	const int METRIC_AVG = 2;
	
	void MetricSet(const string& name, int value);
	void MetricAdd(const string& name, int value = 1);
	string GetMetricSQLitePath(const string& appname, const string& name, int date);
	string GetMetricSQLitePath(const string& appname, const string& name, const DateTime& dt);
};

///////////////////////////////////////////////////////////
#endif
