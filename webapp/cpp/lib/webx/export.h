#include "route.h"
#include "metric.h"

EXTERN_DLL_FUNC void SetLogFlag(int flag)
{
	LogThread::Instance()->setLogFlag(flag);
}
EXTERN_DLL_FUNC int Loop(const char* path)
{
	const char* cmd[] = {"webserver"};

	if (path == NULL) return XG_PARAMERR;

	Process::Instance(ARR_LEN(cmd), (char**)(cmd));

	if (HttpServer::Instance()->init(path)) HttpServer::Instance()->loop();

	return XG_SYSERR;
}
EXTERN_DLL_FUNC void WriteLog(const char* tag, const char* msg)
{
	if (tag && msg)
	{
		int level = eINF;
		char flag = toupper(tag[0]);
		
		switch(flag)
		{
		case 'D':
			level = eDBG;
			break;
		case 'T':
			level = eTIP;
			break;
		case 'E':
			level = eERR;
			break;
		case 'I':
			if (toupper(tag[1]) == 'M') level = eIMP;
			break;
		}

		LogTrace(level, "%s", msg);
	}
}

EXTERN_DLL_FUNC int GetId()
{
	return HttpServer::Instance()->getId();
}

EXTERN_DLL_FUNC int GetPort()
{
	return HttpServer::Instance()->getPort();
}
EXTERN_DLL_FUNC const char* GetHost()
{
	return HttpServer::Instance()->getHost().c_str();
}
EXTERN_DLL_FUNC const char* GetSequence()
{
	thread_local string val;

	val = HttpServer::Instance()->getSequence();

	return val.c_str();
}
EXTERN_DLL_FUNC const char* GetMimeType(const char* key)
{
	thread_local string val;

	val = HttpServer::Instance()->getMimeType(key);

	return val.c_str();
}
EXTERN_DLL_FUNC const char* GetParameter(const char* key)
{
	thread_local string val;

	val = webx::GetParameter(key);

	return val.c_str();
}
EXTERN_DLL_FUNC const char* GetRouteHost(const char* path)
{
	thread_local string val;
	HostItem item = webx::GetRouteHost(path);

	if (item.host.empty()) return NULL;

	val = item.toString();

	return val.c_str();
}
EXTERN_DLL_FUNC void MetricAdd(const char* name, int value)
{
	if (name && value >= 0) webx::MetricAdd(name, value);
}
EXTERN_DLL_FUNC void MetricSet(const char* name, int value)
{
	if (name && value >= 0) webx::MetricSet(name, value);
}
EXTERN_DLL_FUNC const char* GetConfig(const char* name, const char* key)
{
	thread_local string val;

	if (name == NULL) name = "";
	if (key == NULL) key = "";

	try
	{
		val = webx::GetConfig(name, key);
	}
	catch (Exception e)
	{
		LogTrace(eERR, "query confile[%s][%s] failed[%s]", name, key, e.getErrorString());
	}

	return val.c_str();
}
EXTERN_DLL_FUNC void SetCgiDefaultGroup(const char* path, const char* grouplist)
{
	if (path && grouplist) webx::SetCgiDefaultGroup(path, grouplist);
}
EXTERN_DLL_FUNC int DisableSession(const char* sid)
{
	try
	{
		sp<Session> session = webx::GetSession(sid);

		if (session)
		{
			session->disable();

			return XG_OK;
		}

		return XG_TIMEOUT;
	}
	catch (Exception e)
	{
	}

	return  XG_SYSERR;
}
EXTERN_DLL_FUNC int CreateSession(const char* sid, int timeout)
{
	try
	{
		sp<Session> session = webx::GetSession(sid, timeout);

		if (session) return XG_OK;
	}
	catch (Exception e)
	{
	}

	return XG_SYSERR;
}
EXTERN_DLL_FUNC const char* GetSession(const char* sid, const char* key)
{
	thread_local string val;

	try
	{
		sp<Session> session = webx::GetSession(sid);

		if (session)
		{
			val = session->get(key);
		}
		else
		{
			val.clear();
		}
	}
	catch (Exception e)
	{
		val.clear();
	}

	return val.c_str();
}
EXTERN_DLL_FUNC int SetSession(const char* sid, const char* key, const char* val)
{
	if (val == NULL) val = "";

	try
	{
		sp<Session> session = webx::GetSession(sid);

		if (!session) return XG_TIMEOUT;

		if (session->set(key, val)) return XG_OK;
	}
	catch (Exception e)
	{
	}

	return XG_SYSERR;
}
EXTERN_DLL_FUNC void SetCgiAccess(const char* path, const char* access)
{
	if (strcasecmp(access, "public") == 0)
	{
		HttpServer::Instance()->setCgiAccess(path, CGI_PUBLIC);
	}
	else if (strcasecmp(access, "protect") == 0)
	{
		HttpServer::Instance()->setCgiAccess(path, CGI_PROTECT);
	}
	else if (strcasecmp(access, "private") == 0)
	{
		HttpServer::Instance()->setCgiAccess(path, CGI_PRIVATE);
	}
	else
	{
		HttpServer::Instance()->setCgiAccess(path, CGI_DISABLE);
	}
}
EXTERN_DLL_FUNC void SetCgiExtdata(const char* path, const char* extdata)
{
	HttpServer::Instance()->setCgiExtdata(path, extdata ? extdata : "");
}
EXTERN_DLL_FUNC int CheckAccess(const char* path, const char* param, const char* grouplist)
{
	typedef int (*CheckAccessFunc)(const char*, const char*, const char*);
	static CheckAccessFunc func = (CheckAccessFunc)Process::GetObject("HTTP_CHECK_ACCESS_FUNC");

	if (func == NULL) return XG_SYSERR;

	return func(path, param, grouplist);
}
EXTERN_DLL_FUNC int CheckLimit(const char* key, int maxtimes, int timeout)
{
	if (key == NULL || maxtimes <= 0 || timeout <= 0) return XG_PARAMERR;

	string name = stdx::format("%s:limit", key);

	if (RedisConnect::CanUse())
	{
		sp<RedisConnect> redis = RedisConnect::Instance();

		if (!redis) return XG_SYSERR;

		if (redis->incr(name, 1, timeout) > maxtimes) return XG_DAYLIMIT;
	}
	else
	{
		int times = 0;
		sp<Session> sess = HttpServer::Instance()->getSession(name, timeout);

		if (!sess) return XG_SYSERR;

		sess->getInt("times", times);
		sess->setInt("times", ++times);

		if (times > maxtimes) return XG_DAYLIMIT;
	}

	return XG_OK;
}
EXTERN_DLL_FUNC void SetCgiDoc(const char* path, const char* reqdoc, const char* rspdoc, const char* remark)
{
	HttpServer::Instance()->setCgiDoc(path, reqdoc, rspdoc, remark);
}
EXTERN_DLL_FUNC int GetLastRemoteStatus()
{
	return webx::GetLastRemoteStatus();
}
EXTERN_DLL_FUNC const char* GetRemoteResult(const char* path, const char* data, const char* head)
{
	string reqData;
	string reqHead;
	thread_local SmartBuffer result;

	if (data) reqData = data;
	if (head) reqHead = head;

	result = webx::GetRemoteResult(NULL, path, reqData, reqHead);

	return result.str();
}