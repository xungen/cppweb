#ifndef XG_WEBX_STD_H
#define XG_WEBX_STD_H
///////////////////////////////////////////////////////////
#include "sess.h"

#ifndef XG_HTTP_GZIP_COMPRESS_LEVEL
#define XG_HTTP_GZIP_COMPRESS_LEVEL -1
#endif

namespace webx
{
	class Application;

	struct FileParamItem
	{
		int len;
		string key;
		string contype;
		string filename;
		string filepath;
		const char* data;
	};

	class ProcessBase : public HttpProcessBase
	{
		friend class Application;

	protected:
		HttpServer* app;
		JsonElement json;
		sp<LoginToken> token;
		function<int()> func;
		HttpRequest* request;
		HttpResponse* response;

	public:
		virtual int process();
		virtual int forward(ProcessBase* cgi);
		virtual int simpleResponse(int code = XG_OK);
		virtual string checkLogin(const string& sid = "");
		virtual void checkSession(const string& sid = "");
		virtual void parse(JsonReflect& data, bool decode = false);
		virtual void checkSystemRight(const string& grouplist = "");
		virtual int doWork(HttpRequest* request, HttpResponse* response);
		virtual int compressResponse(int level = XG_HTTP_GZIP_COMPRESS_LEVEL);

		HttpRequest* getRequest() const
		{
			return request;
		}
		HttpResponse* getResponse() const
		{
			return response;
		}
		ProcessBase() : app(HttpServer::Instance()), request(NULL), response(NULL)
		{
		}
	};

	struct WebAppData
	{
		string path;
		function<sp<ProcessBase>()> create;

		WebAppData()
		{
		}
		WebAppData(const string& path, function<sp<ProcessBase>()> create)
		{
			this->path = path;
			this->create = create;
		}
	};

	string GetCgiPathList();
	void ReloadSystemConfig();
	map<string, WebAppData>& GetAppMap();
	sp<ProcessBase> GetWebApp(const string& key);
	int GetAccess(const initializer_list<const char*>& list, int index);
	const char* GetString(const initializer_list<const char*>& list, int index);

	bool IsMailString(const string& str);
	string GetScriptString(const string& str);
	string GetFileIcon(const string& filename);
	int PrintRecordview(StringCreator& out, const string& tabid);
	bool IsFileName(const string& str, int minlen = 1, int maxlen = 64);
	bool IsFilePath(const string& str, int minlen = 1, int maxlen = 256);
	bool IsAlnumString(const string& str, int minlen = 1, int maxlen = 64);
	bool IsSeqnoString(const string& str, int minlen = 1, int maxlen = 32);
	void CheckFileName(const string& str, int minlen = 1, int maxlen = 64);
	void CheckFilePath(const string& str, int minlen = 1, int maxlen = 256);
	void CheckAlnumString(const string& str, int minlen = 1, int maxlen = 64);
	void CheckSeqnoString(const string& str, int minlen = 1, int maxlen = 32);
	string GetLimitString(DBConnect* dbconn, int pagesize = 1, int page = 0);

	int SaveParamItem(FileParamItem& item, const string& path);
	int GetFileParamList(vector<FileParamItem>& vec, SmartBuffer& buffer, HttpServer* app, HttpRequest* request, HttpResponse* response);

	int PackJson(sp<QueryResult> rs, JsonElement& json);
	int PackJson(sp<QueryResult> rs, const string& name, JsonElement& json);

	int PackJson(sp<QueryResult> rs, JsonElement& json, function<string(int, const string&)> func);
	int PackJson(sp<QueryResult> rs, const string& name, JsonElement& json, function<string(int, const string&)> func);

	template<class T> void RegistWebApp(const string& url)
	{
		string key = CgiMapData::GetKey(url);

		webx::GetAppMap()[key] = webx::WebAppData(key, [](){return newsp<T>();});
	}
};

extern void SetHttpDestroyFunc(DESTROY_HTTP_CGI_FUNC func);

#define param_string(name) string name = request->getParameter(#name)
#define param_int(name) int name = stdx::atoi(request->getParameter(#name).c_str())
#define param_float(name) double name = stdx::atof(request->getParameter(#name).c_str())

#define head_param_string(name) string name = request->getHeadValue(#name)
#define head_param_int(name) int name = stdx::atoi(request->getHeadValue(#name).c_str())
#define head_param_float(name) double name = stdx::atof(request->getHeadValue(#name).c_str())

#define param_name_string(name) 			\
string name = request->getParameter(#name);	\
if (!webx::IsFileName(name)) throw Exception(XG_PARAMERR, "invalid parameter[" + string(#name) + "]")

#define param_path_string(name) 			\
string name = request->getParameter(#name);	\
if (!webx::IsFilePath(name)) throw Exception(XG_PARAMERR, "invalid parameter[" + string(#name) + "]")

#define param_seqno_string(name) 			\
string name = request->getParameter(#name);	\
if (!webx::IsSeqnoString(name)) throw Exception(XG_PARAMERR, "invalid parameter[" + string(#name) + "]")

class HttpCgiSetup
{
public:
	static string& GetCgiPath();

	HttpCgiSetup()
	{
		GetCgiPath() = HttpServer::GetWebAppPath("${filename}", DllFile::GetLastPath().c_str());
		HttpServer::Instance()->setCgiAccess(GetCgiPath(), CGI_PRIVATE);
	}
	HttpCgiSetup(int access)
	{
		GetCgiPath() = HttpServer::GetWebAppPath("${filename}", DllFile::GetLastPath().c_str());
		HttpServer::Instance()->setCgiAccess(GetCgiPath(), access);
	}
	HttpCgiSetup(const char* path)
	{
		GetCgiPath() = HttpServer::GetWebAppPath(path, DllFile::GetLastPath().c_str());
		HttpServer::Instance()->setCgiAccess(GetCgiPath(), CGI_PRIVATE);
	}
	HttpCgiSetup(int access, const char* path)
	{
		GetCgiPath() = HttpServer::GetWebAppPath(path, DllFile::GetLastPath().c_str());
		HttpServer::Instance()->setCgiAccess(GetCgiPath(), access);
	}
	HttpCgiSetup(const char* path, int access)
	{
		GetCgiPath() = HttpServer::GetWebAppPath(path, DllFile::GetLastPath().c_str());
		HttpServer::Instance()->setCgiAccess(GetCgiPath(), access);
	}
	HttpCgiSetup(const char* path, const char* extdata)
	{
		GetCgiPath() = HttpServer::GetWebAppPath(path, DllFile::GetLastPath().c_str());
		HttpServer::Instance()->setCgiExtdata(GetCgiPath(), extdata ? extdata : "");
		HttpServer::Instance()->setCgiAccess(GetCgiPath(), CGI_PRIVATE);
	}
	HttpCgiSetup(const char* path, const char* extdata, int access)
	{
		GetCgiPath() = HttpServer::GetWebAppPath(path, DllFile::GetLastPath().c_str());
		HttpServer::Instance()->setCgiExtdata(GetCgiPath(), extdata ? extdata : "");
		HttpServer::Instance()->setCgiAccess(GetCgiPath(), access);
	}
	HttpCgiSetup(const char* path, int access, const char* extdata)
	{
		GetCgiPath() = HttpServer::GetWebAppPath(path, DllFile::GetLastPath().c_str());
		HttpServer::Instance()->setCgiExtdata(GetCgiPath(), extdata ? extdata : "");
		HttpServer::Instance()->setCgiAccess(GetCgiPath(), access);
	}
	HttpCgiSetup(int access, const char* path, const char* extdata)
	{
		GetCgiPath() = HttpServer::GetWebAppPath(path, DllFile::GetLastPath().c_str());
		HttpServer::Instance()->setCgiExtdata(GetCgiPath(), extdata ? extdata : "");
		HttpServer::Instance()->setCgiAccess(GetCgiPath(), access);
	}

	const string& getPath() const
	{
		return GetCgiPath();
	}
};

template<class TYPE> class HttpAppSetup
{
	string key;

public:
	HttpAppSetup()
	{
		init("${classname}");
	}
	HttpAppSetup(int access)
	{
		init("${classname}", access);
	}
	HttpAppSetup(const char* path)
	{
		init(path);
	}
	HttpAppSetup(int access, const char* path)
	{
		init(path, access);
	}
	HttpAppSetup(const char* path, int access)
	{
		init(path, access);
	}
	HttpAppSetup(const char* path, const char* extdata)
	{
		init(path, CGI_PRIVATE, extdata);
	}
	HttpAppSetup(const char* path, const char* extdata, int access)
	{
		init(path, access, extdata);
	}
	HttpAppSetup(const char* path, int access, const char* extdata)
	{
		init(path, access, extdata);
	}
	HttpAppSetup(int access, const char* path, const char* extdata)
	{
		init(path, access, extdata);
	}
	HttpAppSetup(const char* path, const char* extdata, const char* filename)
	{
		init(path, CGI_PRIVATE, extdata, filename);
	}
	HttpAppSetup(const char* path, int access, const char* extdata, const char* filename)
	{
		init(path, access, extdata, filename);
	}
	void init(const char* path, int access = CGI_PRIVATE, const char* extdata = NULL, const char* filename = Object::GetClassName<TYPE>())
	{
		key = stdx::tolower(HttpServer::GetWebAppPath(path, filename));
		webx::RegistWebApp<TYPE>(key);
		HttpServer::Instance()->setCgiAccess(key, access);
		HttpServer::Instance()->setCgiExtdata(key, extdata ? extdata : "");
	}

public:
	const string& getPath() const
	{
		return key;
	}
};

template<class REQUEST, class RESPONSE> class HttpCgidocSetup
{
public:
	HttpCgidocSetup(const char* path, const char* remark)
	{
		HttpServer::Instance()->setCgiDoc(path ? path : "", REQUEST().toDocString(), RESPONSE().toDocString(), remark ? remark : "");
	}
	HttpCgidocSetup(const char* path, const char* remark, const char* extdata)
	{
		HttpServer::Instance()->setCgiExtdata(path ? path : "", extdata ? extdata : "");
		HttpServer::Instance()->setCgiDoc(path ? path : "", REQUEST().toDocString(), RESPONSE().toDocString(), remark ? remark : "");
	}
};

#define HTTP_WEBCGI(...) static auto _cgipathsetup_ = newsp<HttpCgiSetup>(__VA_ARGS__);
#define HTTP_WEBAPP(TYPE, ...) static auto _cgipathsetup_ = newsp<HttpAppSetup<TYPE>>(__VA_ARGS__);
#define HTTP_DAILYTASK(TYPE, PATH, TIME) HTTP_WEBAPP(TYPE,PATH,CGI_PROTECT,("dailytask="+stdx::str(TIME)).c_str());
#define HTTP_TIMERTASK(TYPE, PATH, DELAY) HTTP_WEBAPP(TYPE,PATH,CGI_PROTECT,("timertask="+stdx::str(DELAY)).c_str());

#define WEBAPP(FUNC, REQUEST, RESPONSE, ...) 										\
HTTP_WEBAPI(webx::GetAccess({__VA_ARGS__}, 0), webx::GetString({__VA_ARGS__}, 1), {	\
	REQUEST req;																	\
	RESPONSE rsp;																	\
	parse(req);																		\
	::FUNC(*this, req, rsp);														\
	out << rsp.toString();															\
	return XG_OK;																	\
})																					\
HTTP_APIDOC(REQUEST, RESPONSE, webx::GetString({__VA_ARGS__}, 2), webx::GetString({__VA_ARGS__}, 3))

#define HTTP_WEBAPI(ACCESS, PATH, FUNC)													\
namespace{class HttpFunc:public webx::ProcessBase{public:HttpFunc(){func=[&]()FUNC;}};	\
static auto _cgipathsetup_ = newsp<HttpAppSetup<HttpFunc>>((char*)PATH,ACCESS,"",__FILE__);}

#define HTTP_APIDOC(REQUEST, RESPONSE, ...) static auto _cgidocsetup_ = newsp<HttpCgidocSetup<REQUEST,RESPONSE>>(_cgipathsetup_->getPath().c_str(),__VA_ARGS__);

#define HTTP_PLUGIN_INIT(func)										\
HTTP_WEBCGI(CGI_PRIVATE, "@null")									\
DEFINE_HTTP_CGI_EXPORT_FUNC(HttpProcessBase)						\
EXTERN_DLL_FUNC int HttpPluginInit(){func;return XG_OK;}

#define DEFINE_HTTP_CGI_EXPORT_FUNC(TYPE)							\
EXTERN_DLL_FUNC const char* GetHttpCgiPath()						\
{																	\
	return HttpCgiSetup::GetCgiPath().c_str();						\
}																	\
EXTERN_DLL_FUNC const char* GetHttpCgiPathList()					\
{																	\
	static string path;												\
	if (path.empty()) path = webx::GetCgiPathList();				\
	return path.c_str();											\
}																	\
EXTERN_DLL_FUNC HttpProcessBase* CreateHttpProcessObject()			\
{																	\
	return new TYPE();												\
}																	\
EXTERN_DLL_FUNC void DestroyHttpProcessObject(HttpProcessBase* obj)	\
{																	\
	delete obj;														\
}
///////////////////////////////////////////////////////////
#endif
