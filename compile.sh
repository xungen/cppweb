#!/bin/bash

name=`uname`
setup=cppshell/setup

if [ "$name" == "Linux" ]
then
	setup=cppshell/setup
else
	setup=cppshell/setup.exe

	if [ `pwd | wc -w` -gt 1 ]
	then
		echo 'current path has space character'
		exit -1
	fi
fi

echo 'initialize configure'
echo '---------------------------------------------'

if [ -d C:/Windows/System32 ]
then
	mkdir -p product/win/lib
	mkdir -p product/win/dll
	mkdir -p product/win/bin

	gccpath=`pwd`/product/win/gcc/bin

	if [ -d $gccpath ]
	then
		if [ `which g++ 2> /dev/null | wc -l` -eq 0 ]
		then
			export PATH=$PATH:$gccpath
		fi
	fi

	if [ -f product/win/lib/openssl/lib/ssl.lib ]
	then
		echo '1.check openssl success'
	else
		echo 'please install openssl first'
		exit -1
	fi
else
	mkdir -p product/lib
	mkdir -p product/dll
	mkdir -p product/bin

	if [ `which openssl 2> /dev/null | wc -l` -eq 0 ]
	then
		echo 'please install openssl first'
		exit -1
	else
		echo '1.check openssl success'
	fi
fi

if [ -f cppshell/setup.cpp ]
then
	rm -rf $setup
	g++ -std=c++11 cppshell/setup.cpp -o $setup

	if [ -f $setup ]
	then
		$setup
	else
		echo "compiler don't support c++11"
		exit -1
	fi

	echo '2.check g++ compiler success'
else
	echo '2.skip g++ compiler check'
fi

if [ `which javac 2> /dev/null | wc -l` -gt 0 ]
then
	echo '3.check java compiler success'
else
	echo '3.skip java module initialize'
fi

echo '4.create product directory success'

export CPPWEB_INSTALL_HOME=`pwd`

source .webprofile

echo '---------------------------------------------'
echo '>>> initialize build-essential success'
echo ''

echo '*** wait few minutes for compiling ***'
echo ''

make $1 $2 $3 $4